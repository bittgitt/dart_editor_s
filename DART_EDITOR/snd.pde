// INTERFACE CONTROL BUTTON
ControlP5 control,control2,hints;
Button exit, save, load, sendFirstPage, sendSecondPage, sendOnPageOne, sendOnPageTwo, edit, sound, New, Monitor_IN_button, Monitor_OUT_button ;
Knob del_;
Chart myChart;
Textarea hints2;
//Button scale1;
Matrix scale1, Matrix_button;
 Numberbox color_, filter_channel, filter_value;

 //  if (elementData.get(theButton.getId()).User_byte_12 == 0)
//**************************** 
//*****    the control p5 init
//**************************** 

void init() {
  control = new ControlP5(this);
   control2 = new ControlP5(this);
    hints = new ControlP5(this);
    hints.setAutoDraw(false);
    
  cp5 = new ControlP5(this);
    cp5.setAutoDraw(false);
    
  ellipseMode(CENTER);
  f = createFont("typeO.TTF", Betw2*0.4); 
      textFont(f, Betw2*0.4);
 //textFont(f, Betw2);
   cp5.setFont(f, (int)(Betw2*0.6));
 //  cp5.setColor(Color.red);
 
  
  control.setFont(f, int(Betw2*0.7));
  //control.setFont(f, int(Betw2));
  control2.setFont(f, int(Betw2*0.5));
 
  // control.setColorBackground(color(90, 90, 90) );
}
// FUNCTION TO ACTIVATE PANEL BUTTON
void mousePressed() {
  if (exit.isPressed() && i_sender >= 60)    {delay(500); exit(); //\myBus.close(); 
}

  if (edit.isPressed() && i_sender >= 60) {if (edit.isOn() == true ) {edit_activate();  // initMidi(); // saw.play(); 
} else {edit_deactivate();// saw.stop(); 
}   }

 if (sound.isPressed()) {if (sound.isOn() == true ) {soundstatus = true; //edit_activate(); // saw.play(); 
} else { soundstatus = false; //edit_deactivate();// saw.stop(); 
}   }

if (save.isPressed() && i_sender >= 60) { delay(500); save.setOff(); selectOutput("Save Editor Settings:", "fileToSave"); save.setOff(); }
if (load.isPressed()) {post_load_setup= 1; delay(500);   load.setOff(); selectInput("Load Editor Settings:", "fileToLoad"); load.setOff(); elementData.get(idElement).labeler();}










  ////// SELETTORE PAGE /////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  if (sendFirstPage.isPressed() && i_sender >= 60) {      // selettore page - anche se si chiama send
      page_selected = false;
      sendSecondPage.setOff(); 
      
      
    
      
      for (int i=0; i<60; i++) {
      elementData.get(i).plug_page();
      elementData.get(i).labeler();}
      
      set_scale_view();    
      set_matrix_view();
      
       elementData.get(idElement).labeler();
     
   }
   
     ////// SELETTORE PAGE
    if (sendSecondPage.isPressed() && i_sender >= 60)  {   // selettore page - anche se si chiama send
       page_selected = true;
       sendFirstPage.setOff();  
       
      
       
       for (int i=0; i<60; i++) {
       elementData.get(i).plug_page();
       elementData.get(i).labeler();}
       
       set_scale_view();
       set_matrix_view();
       
       elementData.get(idElement).labeler();
       
      
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    
    
    
    
    
    
    
    
    
    
    
  //*************
  //send selected to first page   // invia tutto
  //**************
 if (sendOnPageOne.isPressed() && i_sender >= 60) {   // send ALL
   i_sender = 0;
   general_send =1;
   }
  
  
  

  if (sendOnPageTwo.isPressed() && i_sender >= 60 ) {                 // send THIS

    myBus.sendMessage(241, 0, 0);
    delay(260);
    for (int i=0; i<60; i++) {
      
         if (elementData.get(i).getStateDisplay())  // è L'ELEMENTO SELEZIONATO?
     { 
     sender_single(i);
      if (elementData.get(i).toggleMode == 26) {general_send =1;}
     
     }
      
      
      if (elementData.get(i).toggleMode == 26 && general_send == 0) { // devo inviare anche l'ITEM general, se non è mai stato inviato
      general_send =1;
      sender_single(i);
      } 
      
      
   
      
    } 
    myBus.sendMessage(241, 0, 0);
    
  }
  
  
  
}
// TO SET THE BUTTON POSITION
void setUIButtonsPosition () {
  
 // rect(gridCols[2],  gridRow[2], gridCols[15], gridRow[14]  ,7);
  // hintbox();
   
  
   
  exit = control.addButton("EXIT")
    .setId(1000)
    .setValue(0)
    .setPosition(gridCols[1], gridRow[1])
    .setSize(int(Betw2*4), int(rowBetw*1.8))
    .setView(new SisButton())
    ;
  ;
  sendFirstPage = control.addButton("PAGE 1")
    .setId(1001)
    .setValue(0)
    .setPosition(gridCols[3]+Betw2*0.5, gridRow[1])
    .setSize(int(Betw2*4), int(rowBetw*1.8))
   // .setSize(int(Betw2*6),int(rowBetw*1.8))
    .setView(new SisButton_page())
   // .isSwitch()
  
   .setSwitch(true) 
    .setOn()
    ;
  ;
  
  // sendFirstPage.isSwitch();
   
  sendSecondPage = control.addButton("PAGE 2")
    .setId(1002)
    .setValue(0)
    .setPosition(gridCols[5]+Betw2, gridRow[1])
    .setSize(int(Betw2*4), int(rowBetw*1.8))
   // .setSize(int(Betw2*6), int(rowBetw*1.8))
    .setView(new SisButton_page())
  .setSwitch(true) 
    ;
  ;
 // sendSecondPage.isSwitch();
  Monitor_IN_button = control2.addButton("IN")
    .setId(2017)
    .setValue(0)
    .setPosition(gridCols[19]+Betw2*0.3, gridRow[18]+rowBetw*1.5)
    .setSize(int(Betw2*1.7), int(rowBetw*1.4))
   // .setSize(int(Betw2*6), int(rowBetw*1.8))
    .setView(new MIDI_IN_OUT())
  .setSwitch(true) 
   .setOn()
    ;
  ;
  
   Monitor_OUT_button = control2.addButton("OUT")
    .setId(2018)
    .setValue(0)
   
    .setPosition(gridCols[20]+Betw2*0.3, gridRow[18]+rowBetw*1.5)
    .setSize(int(Betw2*1.7), int(rowBetw*1.4))
   // .setSize(int(Betw2*6), int(rowBetw*1.8))
    .setView(new MIDI_IN_OUT())
  .setSwitch(true) 
   .setOn()
    ;
  ;
    
  


  save = control.addButton("SAVE")
    .setId(1004)
    .setValue(0)
    .setPosition(gridCols[10], gridRow[1])
    .setSize(int(Betw2*4), int(rowBetw*1.8))
    .setView(new SisButton())
     .setSwitch(false) 
    ;
  ;
  
  {
  edit = control.addButton("DRAW")
    .setId(1006)
    .setValue(0)
    .setPosition(gridCols[12]+Betw2/2
    , gridRow[1])
    
    .setSize(int(Betw2*4), int(rowBetw*1.8))
    .setView(new SisButton_toggle())
    .setSwitch(true) 
    ;
  ;
  }
  
  if (Draw_settings.getInt("value") ==0) 
  edit.setPosition(-gridCols[15]+Betw2/2
    , gridRow[1]);
  
  
 // "logo.gif"
    sound = control.addButton("SOUND")
    // .setImage(logo) 
    .setId(1008)
    
    .setValue(0)
    .setPosition(gridCols[14]+Betw2
    , gridRow[1])
    .setSize(int(Betw2*4), int(rowBetw*1.8))
    .setView(new SisButton_toggle())
    .setSwitch(true) 
    .setOn()
    ;
  ;
  
/////////////////////////////////////////////////////////////////////////////////////////////
  scale1= control2.addMatrix("")
     .setPosition(gridCols[20]+Betw2, gridRow[16]+rowBetw)
     .setSize((int) gridCols[3] //-(Betw2/8)
     ,(int) gridCols[3]/11)
     .setGrid(12, 1)
     .setGap(1, 2)
     //.setInterval(200)
     .setMode(ControlP5.MULTIPLES)
     .setColorBackground(color(120))
     .setBackground(color(40))
     .setId(1005)
     //.setMoveable(true) 
    //
  // .onClick(componi)
   //.onPress(componi)
   //onEnter
   .addCallback(componi)
     .stop()
     ;


   Matrix_button = control2.addMatrix("")
     .setPosition( (int) gridCols[17]+Betw2, (int)gridRow[6]+rowBetw)
     .setSize((int) gridCols[7]-(Betw2*2)
     ,(int) gridCols[7]-(Betw2*2))
     .setGrid(8, 8)
     .setGap(4, 4)
     //.setInterval(200)
     .setMode(ControlP5.MULTIPLES)
     .setColorBackground(color(120))
     .setBackground(color(100))
     .setId(2022)
   // .addCallback(Matrix2tables)
   // .onChange(Matrix2tables)
       .onClick(Matrix2tables)
       .onRelease(Matrix2tables)
     .stop()
     ;
  
     
     ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  
  
  load = control.addButton("LOAD")
    .setId(1005)
    .setValue(0)
    .setPosition(gridCols[8]-Betw2*0.5, gridRow[1])
    .setSize(int(Betw2*4), int(rowBetw*1.8))
    .setView(new SisButton())
      .setSwitch(false) 
    ;
   /*   New = control.addButton("NEW")
    //.setId(1005)
    .setValue(0)
    .setPosition(gridCols[19]-Betw2*0.5, gridRow[15])
    .setSize(int(Betw2*5), int(rowBetw*1.8))
    .setView(new SisButton())
    ;
  ; */
    sendOnPageOne = control.addButton("Send ALL")
    .setId(1003)
    .setValue(0)
    .setPosition(gridCols[17], gridRow[1])
    .setSize(int(Betw2*6.5), int(rowBetw*1.8))
    .setView(new SisButton())
    .setSwitch(false) 
    ;
  ;
  sendOnPageTwo = control.addButton("Send THIS")
    .setId(1007)
    .setValue(0)
    .setPosition(gridCols[21]-Betw2 *0.5
    , gridRow[1])
    .setSize(int(Betw2*6.5), int(rowBetw*1.8))
    .setView(new SisButton())
      .setSwitch(false) 
    ;
  ;
  
    //strokeWeight(0);
   //  noStroke();
    
 filter_channel = control2.addNumberbox("Filter Channel", (int) (gridCols[19]+Betw2), (int) (gridRow[19]+rowBetw*1.5
 ), (int)(Betw2*3), (int)(rowBetw*1.5)) //   
      .plugTo( this, "channel_filter" )
      .setId(2015)
      .setRange(0, 16)
  
       .setColorCaptionLabel(color(160))
   
     .setColorBackground(color(80)) 
   
      .setLabel("Channel f.")
      

     .setVisible(true);
    ;
    
    makeEditable (filter_channel);
    
    
    
     filter_value = control2.addNumberbox("Filter Value", (int) (gridCols[19]+Betw2), (int) (gridRow[21]+rowBetw*1.5
     ), (int)(Betw2*3), (int)(rowBetw*1.5)) //   
      .plugTo( this, "value_filter" )
      .setId(2016)
      .setRange(-1, 127)
       .setColorBackground(color(80)) 
       .setColorCaptionLabel(color(160))
       .setValue(-1)
      .setLabel("Value f.")
     // .setValueLabel("zero")
      // .setValue(memoryPosition) 
     .setVisible(true);
    ;
    // makeEditable( filter_channel );
       makeEditable( filter_value );
       
      
       
         myChart = cp5.addChart("")
               .setId(2019)
               
               .setPosition(gridCols[17],  gridRow[18]+rowBetw)
             //   rect(gridCols[17],  gridRow[18]+rowBetw, gridCols[7], gridRow[6],7); //red out
               .setSize( int(gridCols[7]), int(gridRow[6]))
               .setRange(-10, 137)
               .setView(Chart.AREA) // use Chart.LINE, Chart.PIE, Chart.AREA, Chart.BAR_CENTERED
              // .setStrokeWeight(7)
             // .setColor(ControlP5.BLUE) 
               .setColorBackground(color(0,0,0)) 
          //    .setColorForeground(color(90,90,90)) 
          //  .setColorCaptionLabel(color(90,90,90)) 
          //     .setColorValue(color(90,90,90)) 
          //     .setColorActive(color(90,90,90)) 
          //    .setColorValueLabel(color(90,90,90))  
          //     .setColorLabel(color(90,90,90)) 
              // .setColorCaptionLabel(57)
               ;
 
  myChart.addDataSet("incoming");
  myChart.setData("incoming", new float[100]);
  myChart.setColors("incoming", color(30,30,30),color(50,50,50));

}


/*
CallbackListener toFront_mon = new CallbackListener() {
  public void controlEvent(CallbackEvent theEvent) {
    theEvent.getController().bringToFront();
    ((ScrollableList)theEvent.getController()).open();
  }
};

CallbackListener close_mon = new CallbackListener() {
  public void controlEvent(CallbackEvent theEvent) {
    ((ScrollableList)theEvent.getController()).close();
  }
};
*/


void edit_activate() {
edit_mode = true;
//saw.play();
    // Button t = (Button)cp5.get("3");
   // t.setView(new Marrone());
    // t.hide();
 //  t.setPosition(500, 500);
   
   for (int i=0; i<60; i++) {
   //  if (i != idElement && idElement< elementData.size()) 
    {
      elementData.get(i).labell.setVisible(false);
       elementData.get(i).label_hint.setVisible(false);
      elementData.get(i).mp.setVisible(false);
        
  //      elementData.get(i).labell.hide();
   //    elementData.get(i).label_hint.hide();
    //    elementData.get(i).mp.hide();
     
       // elementData.get(i).led.setVisible(false);
   //     if (elementData.get(i).toggleMode == 26) {      // general mode 
    //      Button t = (Button)cp5.get(Integer.toString(i+1));
     //      t.hide();}
     
    }
  }
}

void edit_deactivate() {
edit_mode = false;

   for (int i=0; i<elementData.size(); i++) {
        elementData.get(i).labell.setVisible(true);
        elementData.get(i).label_hint.setVisible(true);
        elementData.get(i).mp.setVisible(true);   
  }
}





//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  void makeEditable( Numberbox n ) {
    // allows the user to click a numberbox and type in a number which is confirmed with RETURN
    final NumberboxInput nin = new NumberboxInput( n ); // custom input handler for the numberbox
    // control the active-status of the input handler when releasing the mouse button inside 
    // the numberbox. deactivate input handler when mouse leaves.
    n.onClick(new CallbackListener() {
      public void controlEvent(CallbackEvent theEvent) {
        nin.setActive( true );
      }
    }
    )
 
    .onLeave(new CallbackListener() {
      public void controlEvent(CallbackEvent theEvent) {
        nin.setActive( false ); 
       // nin.submit();
      }
    }
    );
  }
 
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////









/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


CallbackListener componi = new CallbackListener() { // ogni volta che modifico il matrix-box vado ad aggiornare i valori caricati su note(value) e su maxvalue
  public void controlEvent(CallbackEvent theEvent) {
 scala1 = "";     scala2 = "";
 for (byte ii = 0; ii < 7; ii++) 
 {
 if ( scale1.get(ii,0) == true) // {scala1 = scala1.substring(1); } else scala1 = scala1.substring(0);// 
 scala1 ="1"+scala1; else scala1= "0"+scala1;
 }
  for (byte ii = 7; ii < 12; ii++) 
 {
 if ( scale1.get(ii,0) == true) // {scala1 = scala1.substring(1); } else scala1 = scala1.substring(0);// 
 scala2 ="1"+scala2; else scala2= "0"+scala2;
 }
 scal1 = (byte)unbinary(scala1);
 scal2 = (byte)unbinary(scala2);
if(page_selected == false) 
{ elementData.get(idElement).note[0] = scal1;
 elementData.get(idElement).setExcursionControllMax = scal2;}
 else
 { elementData.get(idElement).note[1] = scal1;
 elementData.get(idElement).setExcursionControllMax_2nd = scal2;}
 elementData.get(idElement).nT.setValue(scal1); 
 elementData.get(idElement).max.setValue(scal2); 
  }
};

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////


         void set_scale_view() // caricare setup da numberbox al pianofortino
                               // e scegliere se visualizzre o no il pianofortino
         {  
           if (elementData.get(idElement).midiTypeOpt.getValue() == 0 && elementData.get(idElement).dmx.getValue() >1)  // se type = note // e siamo in pot mode
         
         if (elementData.get(idElement).dMode.getValue() == 21 || elementData.get(idElement).dMode.getValue() == 22  )  // se mode = spinner 1 o 2
        
         {scale1.show(); scale1.bringToFront();      // numberbox a 12 caselline - visibile
        
          show_piano =1;                             // immagine pianofortino - visibile
         
          elementData.get(idElement).nT.hide();      // togli il numberbox delle note
          
           String temp_scale1;
           char bit_sc1;
           boolean bit_scale1;
           byte numm;
           byte numm2;
           if (page_selected== false)                        /// vado a caricare lo status della scala dai numberbox alle caselle del pianofortino
           { numm =(byte)elementData.get(idElement).note[0];
            numm2 =(byte)elementData.get(idElement).setExcursionControllMax;}
            else
            { numm =(byte)elementData.get(idElement).note[1];
             numm2 =(byte)elementData.get(idElement).setExcursionControllMax_2nd;}
             
             
       {  //------------------------------------------------
           // temp_scale1 = binary(elementData.get(idElement).note);
           temp_scale1 = binary(numm);
           
          for (byte i =0; i<8; i++)
          {  
            bit_sc1 =  temp_scale1.charAt(7-i);
           if ( bit_sc1 == '0') bit_scale1 = false; else bit_scale1 = true;
            scale1.set(i, 0,  bit_scale1) ;
          }
         
          temp_scale1 = binary(numm2);
              for (byte i =0; i<5; i++)
          {  
            bit_sc1 =  temp_scale1.charAt(7-i);
           if ( bit_sc1 == '0') bit_scale1 = false; else bit_scale1 = true;
            scale1.set(i+7, 0,  bit_scale1) ;}
      } //------------------------------------------------
         
         
         // scale1.set(2, 0,  true) ;
        }
        
        if (elementData.get(idElement).midiTypeOpt.getValue() != 0 || elementData.get(idElement).dmx.getValue() <2)  
        if (elementData.get(idElement).dMode.getValue() == 21 || elementData.get(idElement).dMode.getValue() == 22 )
        {scale1.hide(); show_piano = 0; // nascondi scale e pianofortino
        
         elementData.get(idElement).nT.show(); // mostra numberbox
      }
        }
        
        
 /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////  
       
        
        
