void midiMessage(MidiMessage message//,long timestamp, String Busname
) { 
  
  
  int midiSignal = constrain ((int)(message.getStatus()), 144, 240);
 
  int indexTypeMidi = constrain(((midiSignal-144)/16), 0, 5);
 
  int value_show = (int)(message.getMessage()[1] & 0xFF);
  
  
  
  
 
  if (channel_filter == 0 ) {     // visualizza tutti i canali midi 1-16
  if (value_show == value_filter || value_filter == -1)  // visualizza solo la value scelta - oppure tutto, se value_filter è -1
{  
  
  if (Monitor_IN_button.isOn())
  { 
    optCC= typeMidiList[(indexTypeMidi)];
    buffer_value = Note; buffer_intensity = Intensity; buffer_raw1 = raw1; // ho sempre una copia del messaggio vecchio
  Channel = midiSignal- typeMidiArray[indexTypeMidi]+1;
  Note = value_show;
  Intensity = (int)(message.getMessage()[2] & 0xFF);
  current_db2 = Intensity;
  raw1 = (int)(message.getStatus() & 0xFF); 
  write_mon = true;  

// if (full_screen == false   )  // modalità FULL disattivata
 //{
 //  midiMonitor();
  refresh_monitor();
  
  

  
  
  
  if (raw1 < 160) {  // se è una nota
 
   wave.setFrequency( frequenza2(value_show) );
   if (value_show > soglia_wave2 ) wave2.setFrequency( frequenza2(value_show-detune) ); else  wave2.setFrequency( frequenza2(value_show+detune) );
   
    wave.setAmplitude( float(int(boolean((int)(message.getMessage()[2] & 0xFF)))) /2   ); // se note on si sente , se note off va a zero
  //  wave2.setAmplitude( int(boolean((int)(message.getMessage()[2] & 0xFF))));
    wave2.setAmplitude(calc_freq_f2(value_show)  );
  //  MIDI_IN_LED = true;
  //  feedback_counter = 0;
}
    else //  if (raw1 < 176)
  { 

     wave.setFrequency( frequenza2((int)(message.getMessage()[2] & 0xFF)) );
 if ((int)(message.getMessage()[2] & 0xFF) > 76 )
  wave2.setFrequency( frequenza2((int)(message.getMessage()[2] & 0xFF)-detune) );
  else 
  wave2.setFrequency( frequenza2((int)(message.getMessage()[2] & 0xFF)+detune) );
  
    wave.setAmplitude( 0.5 );

     wave2.setAmplitude(calc_freq_f2((int)(message.getMessage()[2] & 0xFF))  );

}


}
 MIDI_IN_LED = true;
feedback_counter = 0; 

   // monitor_flux6 [0] = raw1; monitor_flux6 [1] = Note ; monitor_flux6 [2] = Intensity;
}
} 

 
 
 
 else  // visualizza solo il canale Midi scelto. // se channel filter è impostato su qualcosa
 { int cosa = midiSignal- typeMidiArray[indexTypeMidi]+1; 
 if ( channel_filter == cosa) { 
   
   if (value_show == value_filter || value_filter == -1) {
   //  buffer_Channel = byte(Channel); 
  if (Monitor_IN_button.isOn())
  {   
    optCC= typeMidiList[(indexTypeMidi)];
    buffer_value = Note; buffer_intensity = Intensity; buffer_raw1 = raw1;
   write_mon = true;
     Channel = cosa;     
   
   Note = value_show;

  //if (indexTypeMidi == 3 || indexTypeMidi == 4) {} else 
  Intensity = (int)(message.getMessage()[2] & 0xFF);
  current_db2 = Intensity;
    raw1 = (int)(message.getStatus() & 0xFF);
   
    
      if (raw1 < 160) {
          wave.setFrequency( frequenza2(value_show) );
          if (value_show > soglia_wave2)  wave2.setFrequency( frequenza2(value_show-detune) );
          else wave2.setFrequency( frequenza2(value_show+detune) );
          
    wave.setAmplitude(float(int(boolean(Intensity)) /2));
  //   wave2.setAmplitude(int(boolean(Intensity)) );
      wave2.setAmplitude(calc_freq_f2( (value_show))  );
      
   // MIDI_IN_LED = true;
   // feedback_counter = 0; 
        //saw.freq(frequenza2(Note)); 
      // saw.amp(int(boolean(Intensity)));
    } 
    else  //  if (raw1 < 176)
  { // saw.freq(frequenza2(Intensity)); saw.amp(0.5); 
          wave.setFrequency( frequenza2((int)(message.getMessage()[2] & 0xFF)) );
          if ( (int)(message.getMessage()[2] & 0xFF) > soglia_wave2 ) 
          wave2.setFrequency( frequenza2((int)(message.getMessage()[2] & 0xFF)-detune) );
          else
          wave2.setFrequency( frequenza2((int)(message.getMessage()[2] & 0xFF)+detune) );
          
          
    wave.setAmplitude(0.5 );
    
   //  wave2.setAmplitude(0.5 );
     wave2.setAmplitude(calc_freq_f2( ((int)(message.getMessage()[2] & 0xFF)))  );
      
 

}

}

 MIDI_IN_LED = true; 
  feedback_counter = 0; 


   // monitor_flux6 [0] = raw1; monitor_flux6 [1] = Note ; monitor_flux6 [2] = Intensity;
}
  
  
 }
}

for (byte i=0; i<60; i++)   // colora di verde l'ITEM corrispondente
{
 elementData.get(i).feedback_ = false ;
  
  
if (elementData.get(i).note[int(page_selected)] == value_show  )                     // se corrisponde la value - all'interno della pagina corrente


if ( elementData.get(i).midiChannel ==  midiSignal- typeMidiArray[indexTypeMidi]+1 && page_selected == false && elementData.get(i).toggleMode > 0 && elementData.get(i).toggleMode < 25 || 
elementData.get(i).midiChannel_2nd ==  midiSignal- typeMidiArray[indexTypeMidi]+1 && page_selected == true && elementData.get(i).toggleMode_2nd > 0 && elementData.get(i).toggleMode_2nd < 25
) 
                                                                                     // se corrisponde il canale midi e l'item NON è in modalità blind input (0)

{ 
  

if (elementData.get(i).indexMidiType == indexTypeMidi)                               // deve corrispondere anche il type (note/cc/etc)
{
elementData.get(i).feedback_ = true ;                                                // segnale VERDE sull'item

 if (Intensity >0) { cp5.get(Integer.toString(i+1)).setValue(Intensity) ;}


                                                                                     // tutta quests sezione riguarda la rotazione del pentagono (polygon)  
                                                                                     // quando viene ricevuto un 0 o 127 dall'esterno
{
if (Intensity == 0 ) 
elementData.get(i).encoder_rotate = elementData.get(i).encoder_rotate -3 ;
else if (
Intensity == 127) 
elementData.get(i).encoder_rotate = elementData.get(i).encoder_rotate +3 ;
if (elementData.get(i).encoder_rotate > 360*4 || elementData.get(i).encoder_rotate < -(360*4) ) elementData.get(i).encoder_rotate = 0;
}

/////                                                                          // questa sezione contiene la procedura OFFGROUP controllata dall'esterno
                                                                               // se ricevo un segnale dall'esterno 
                                                                               // e questo corrisponde ad un item in un gruppo, 
                                                                               // devo fare reagire tutto il gruppo.
{
  
      for (int del=0; del<60; del++)  // vado a scansionare tutti gli item - devo vedere se altri items sono sullo stesso gruppo
{ //----------- 
 
  {   
    
    if (elementData.get(del).toggleMode >2 && elementData.get(del).toggleMode < 11 ) // l'item scansionato è  toggle groups? 
    { 
      // a questo punto se il gruppo dell'elemento scansionato è uguale al gruppo dell'elemento di cui ho ricevuto il messaggio midi (elementData.get(i))...
      if (elementData.get(del).toggleMode == elementData.get(i).toggleMode)   
      
       // vado a spegnere il pulsante
      {
 elementData.get(del).togglestate = false; // il pulsante che prima era acceso (grigio) adesso è pronto per essere premuto e riacceso.
 elementData.get(del).setStateButton(false); // 
      }
      // però, se la value dell'item scansionato e quella dell'item di cui ricevo il midi sono uguali le cose vanno diversamente :
      // il pulsante dovrà rimanere acceso se l'intensity è >0
       if (elementData.get(del).note == elementData.get(i).note && Intensity >0 )
      
       { // accendi il pulsante e lascialo acceso
          elementData.get(del).togglestate = true;   // 
          elementData.get(del).setStateButton(true); //
       }
      
      
  }
  
  
  }
}//------------
  
}//////


}



 
feedback_counter = 0;
}  
  
}


}
