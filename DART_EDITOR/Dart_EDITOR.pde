///////////////////////////
//                       //
// DART_EDITOR           //
// Massimiliano Marchese //
// Piero Pappalardo      //
// www.dartmobo.com      //
//                       //
///////////////////////////

String version = "/////      DART_EDITOR  V1.80    ";
String custom_intro_string;

//   Import the library to create an interface   //

import controlP5.*;
//to work with file
import java.util.*;
import java.util.Date;
// Import class for pop up midi port setup
import javax.swing.*; 
//Import the MidiMessage classes http://java.sun.com/j2se/1.5.0/docs/api/javax/sound/midi/MidiMessage.html
import themidibus.*; //Import the library
import javax.sound.midi.MidiMessage; 
import javax.sound.midi.SysexMessage;
import javax.sound.midi.ShortMessage;

import ddf.minim.*;
import ddf.minim.ugens.*;

Minim       minim;
AudioOutput out;
Oscil       wave;
Oscil       wave2;

float frequenza2 (float nota)
{
return pow(2,((nota-69)/12))*440;
}

int keyboard_out (int f)
{
if  ( f > 118) {
      return  f-94;
    } else
      if ( f >= 24 ) {
        return  f+8;
      } else {
        return  f;
      }
}



//***********************************
//        button state to open the ui data settings
//**********************************
//float prova_bool = 0.8;

int idElement=0;
int infoGraph;
float ex_dimensionX;
float ex_dimensionY;
int setValueActiveElement= 0;
float larghezza = width;
float lunghezza = height;
int colore_ = 100;
int channel_filter = 0;
int value_filter = -1;
long old_timestamp;

int i_sender  = 61;
int init_initmidi;
boolean MIDI_IN_LED;
boolean MIDI_OUT_LED;
float current_db2;
boolean monitor_color;

int general_send; // inviare il general send anche in SEND THIS ma solo se il general send è stato modificato o mai inviato.

 int test_external_ctrl;

int intro;


boolean view_ciruit_bool; // viene regolata dalla void view_circuit per visualizzare solo quando serve il circuito dartmobo

boolean sawplay = true;
boolean soundstatus = false;
boolean full_screen;
boolean full_screen_toggle = true;
int full_screen_scaler;

int color_R;
int color_G;
int color_B;
int stacco_colore;


byte soglia_wave2 = 64;
byte detune =36;
float calc_freq_f2 (int in)
{
int   calc_freq2a;
float calc_freq_f;

  calc_freq2a = abs(abs(in-64)-64);
  calc_freq_f = map(calc_freq2a,0,64,0.5,0);
  //  println(calc_freq_f);
return  calc_freq_f;
}


int page_count; // avoid doubles - only one modifier can be set to page at a time;
int touch1_count;
int touch2_count;
int spin1_count;
int spin2_count;
int pads_count;
int mouse_count;
int distance_count;
int general_count;

int manda_pagine =0;
byte feedback_counter;

//byte buffer_status;
int buffer_raw1;
//int buffer_Channel;
int buffer_value;
int buffer_intensity;
byte scal1;
byte scal2;

byte post_load_setup;

byte input_remap[] = {
 
6,4,8,3,
7,1,5,2,
};



String scala1 ="_";
String scala2 ="_";





String Dart_device;


boolean write_mon = false;
boolean alert_message = false;
boolean page_selected = false;




String keyCode_ = ".";
String Key_ = ".";

int change;
int change2;
boolean change_do =true;

int mousewheel;
// boolean old_loader = false;
boolean hide_;
boolean add_element;
boolean edit_mode = true;
boolean control_key = false;
// boolean immagine_circuito = false;
int [] monitor_flux1 = {000,000,000};
int [] monitor_flux2 = {000,000,000};
int [] monitor_flux3 = {000,000,000};
int [] monitor_flux4 = {1,1,1};
int [] monitor_flux5 = {1,1,1};
int [] monitor_flux6 = {1,1,1};

boolean monitor_write = true;
boolean monitor_write2 = false;
// MidiMessage old_message;

int old_value0;
int old_value1;
int old_value2;
int raw1;
int raw2;


byte show_piano ;
color background;

//**********************************
//**********************************


public void settings() {
 // if (editor_settings.getInt(5,1) == 1)
 // fullScreen();
//  else  if (editor_settings.getInt(5,1) == 0)
 size(1024, 600);
// size(displayWidth, displayHeight-100);
  // frame.setResizable(true);
}



void setup() {
  
  
 // println(boolean(64/80));
 
  
   editor_settings = loadTable("Editor_settings.csv", "header");
   Draw_settings = editor_settings.getRow(0);
 //  println(Draw_settings.getInt("Parameter"));
 // println(Draw_settings.getInt("value"));
  //  println("lllllll");
  color_R = editor_settings.getInt(1,1);
  color_G = editor_settings.getInt(2,1);
  color_B = editor_settings.getInt(3,1);
  if ( (color_R+color_B+color_G)/3 < 50 ) stacco_colore = 50-(color_R+color_B+color_G)/3;
  else if ( (color_R+color_B+color_G)/3 > 210 ) stacco_colore = -140;//
  background = color(color_R,color_G,color_B);
    color_R = editor_settings.getInt(1,1)+stacco_colore;
  color_G = editor_settings.getInt(2,1)+stacco_colore;
  color_B = editor_settings.getInt(3,1)+stacco_colore;
  custom_intro_string = editor_settings.getString(4,1);
 // print("color 1 -  ");
 // println(editor_settings.getInt(3,1));
 settingScreen();    // settaggi spaziatura
 

  background(background);

 init(); // cp5 library
  

  // THE BUTTON POSITION AND POLYMORFISM in ElementPosition class//
  setUIButtonsPosition(); // panel b - pulsanti exit, page1,page2,load,save,draw,sound
  initTableOfElementData(); // panel b
  setupElement (); // element position
   hintbox();
 
  circuit_position = loadImage("pcb_memorypositions_transp.gif"); // cursor1
 // circuit_position = loadImage("cursor1.png"); 
  piano =  loadImage("piano_keys.gif");
  cursor = loadImage("bitmap_dart_3.png"); 
  
  square1 = loadImage("mask (1).png"); 
  square2 = loadImage("mask (2).png"); 
  square3 = loadImage("mask (3).png"); 
  square4 = loadImage("mask (4).png"); 
  square5 = loadImage("mask (5).png"); 
  
  circle1 = loadImage("mask (6).png"); 
  circle2 = loadImage("mask (7).png"); 
  circle3 = loadImage("mask (8).png"); 
  circle4 = loadImage("mask (9).png"); 
  circle5 = loadImage("mask (10).png"); 
  
  mask1 = loadImage("mask (11).png"); 
  mask2 = loadImage("mask (12).png"); 
  mask3 = loadImage("mask (13).png"); 
  mask4 = loadImage("mask (14).png"); 
  mask5 = loadImage("mask (15).png"); 

  for (int i=0; i<elementData.size(); i++) {
      elementData.get(i).setDisplay(false);
      }
 
loadTableSettings("Default_preset.csv");


   page_selected = false ;
  
      for (int i=0; i<60; i++) {
    
       elementData.get(i).labeler();
      }
        for (int i=0; i<60; i++) {
      elementData.get(i).plug_page();
 
   if (elementData.get(i).shape != 1)  del_ = (Knob)cp5.get(Integer.toString(i+1));
  // del_ = cp5.get(Integer.toString(i+1));

      }
      
      

 frameRate(30);
 minim = new Minim(this);
 out = minim.getLineOut();
 wave = new Oscil( 440, 0.0f, Waves.SAW );
 wave2 = new Oscil( 440, 0.0f, Waves.SAW );
// wave.setAmplitude( 0.0 );
 wave.patch( out );
  wave2.patch( out );
 
   idElement = 0;
  //  elementData.get(idElement).selected = true;
    elementData.get(idElement).labeler();
        elementData.get(idElement).dMode.show();
        elementData.get(idElement).dMode.setVisible(true);
         elementData.get(idElement).selected = true;
       elementData.get(idElement).setStateButton(true); 
    //  elementData.get(0).setStateButton(false);
 
  spegni_numberbox_indesiderati();
// idElement =1; // inizio col primo item selezionato
  
   

}
void draw() {
   init_initmidi();  // non viene eseguita in loop, era un tentativo di fare in modo di ristabilire le connessioni MIDI in caso di distacco del cavo usb
  
 
  
  if (feedback_counter < 255) feedback_counter++; // temporizzatore del buzzer sound, quando si clicca su un item o si riceve un feedback.
  feedback_off();
  
  change();

 interfaceDisp(); // cancella il background - disegna il settings box
  hints.draw();
  
 cp5.draw(); // tutti gli items

 
  myChart.push("incoming",current_db2);
  
 if (full_screen == false   )  // modalità FULL disattivata
 {
  midiMonitor();
   //if( full_screen_toggle == true)
  if(  full_screen_toggle == true)  // modalità FULL disattivata - disegna tutti i selettori
  {
  hints2.show();
  Monitor_IN_button.show();
  Monitor_OUT_button.show();
  filter_channel.show();
 filter_value.show();
 sendOnPageOne.show();
 sendOnPageTwo.show();
 sendFirstPage.show();
 sendSecondPage.show();
 load.show();
 edit.show();
 save.show();
 exit.show();
 sound.show();
 elementData.get(idElement).labeler();
 }

 full_screen_toggle = false;
 //elementData.get(idElement).labeler();
 }
if (full_screen == true && full_screen_toggle !=  full_screen //  fa funzionare l'editor come un controller su schermo, no midimonitor no settingsbox
) 
 {
   full_screen_toggle = true;
 hints2.hide();
 Monitor_IN_button.hide();
 Monitor_OUT_button.hide();
 filter_channel.hide();
 filter_value.hide();
  sendOnPageOne.hide();
 sendOnPageTwo.hide();
 sendFirstPage.hide();
 sendSecondPage.hide();
  load.hide();
 edit.hide();
 save.hide();
 exit.hide();
 sound.hide();
 elementData.get(idElement).labeler();
 }

   
  if (i_sender >= 60) 
  {
  elementListener(); // to trigger the button element of the dart
  explanationText();
   //  allarme_page_();
  //THE MIDI MONITOR in MIDIClass//
   // in midi class
  // delay(300);
  
 keyPressed_();
 
 image_circuit();

post_load_setup_();
 sawplay ();
}
 
else {
  
if (i_sender == 0) {   myBus.sendMessage(241, 0, 0); delay(50); 
hints2.setText("SENDING the complete preset to the DART device");
}
sender();

if (i_sender == 60)  myBus.sendMessage(241, 0, 0);

}

show_piano ();

// delay(100);


// if (test_external_ctrl < 127) test_external_ctrl++; else { test_external_ctrl = 0; delay(5);} // test per controllo esterno delle Knobs
// cp5.get("15").setValue(test_external_ctrl) ;

}
/*void controllo_doppioni () {
    for (int i=0; i<64; i++) {
    if (elementData.get(i).toggleMode == 17) 
    {
    elementData.get(idElement).hintarea.setText("page switch already activated on modifier n."+i); 
  }
  }
}*/




void post_load_setup_()
{
  if (post_load_setup == 1) { 
  
  }
  
  post_load_setup =0;
  
  
}
//___________________________________________________________________________________________________________________________________________

void keyPressed() {
  if (keyCode == 27) key = 0;
}

void keyPressed_ () {
 // if (keyCode == 27) key = 0;
  
  if (keyPressed) {
  //  if (keyCode == 27) key = 0;
    
    keycode2string();
    Key_ = str(key);
    
     
    //if ( control_key== true) 
    if (edit_mode == true)
    {if (key =='f' || key =='F'){
      full_screen=false;
      
   // if (full_screen == true) 
    { myChart.show();
          for (int i=0; i<60; i++) {
 // elementData.get(i).loadTableRow(table.getRow(i));
{
  Knob t = (Knob)cp5.get(Integer.toString(i+1)); 
// if (elementData.get(i).hide == 1) t.hide(); else t.show();
    t.setPosition(
    int(elementData.get(i).posX),
    int(elementData.get(i).posY)
    );
    
   // elementData.get(i).myKnobA.setPosition(elementData.get(i).posX,elementData.get(i).posY);
 //   t.setLabel(elementData.get(i).label);
    t.setWidth(elementData.get(i).radiusW);
    t.setHeight(elementData.get(i).radiusH);
  //  t.setSize(elementData.get(i).radiusW,3);
   // t.setSize(int(elementData.get(i).radiusW),3);
  //  t.hide();
 }

   
  //  delay(10);
  
  
  }}
  
 /* else { myChart.hide();
    for (int i=0; i<60; i++) {
 // elementData.get(i).loadTableRow(table.getRow(i));
{
  Knob t = (Knob)cp5.get(Integer.toString(i+1)); 
// if (elementData.get(i).hide == 1) t.hide(); else t.show();
    t.setPosition(
    int(elementData.get(i).posX*1.33+gridCols[1]*1.2),
    int(elementData.get(i).posY*1.33-gridRow[8])
    );
    
 
    t.setWidth(int(elementData.get(i).radiusW*1.33));
    t.setHeight(int(elementData.get(i).radiusH*1.33));

 }

  }
  
  }*/
  
  
    //  settingScreen();
    
    }
    
   
    if (key =='g' || key =='G'){
      full_screen=true;
    //  settingScreen();
    myChart.hide();
    for (int i=0; i<60; i++) {
 // elementData.get(i).loadTableRow(table.getRow(i));
{
  Knob t = (Knob)cp5.get(Integer.toString(i+1)); 
// if (elementData.get(i).hide == 1) t.hide(); else t.show();
    t.setPosition(
    int(elementData.get(i).posX*1.33+gridCols[1]*1.2),
    int(elementData.get(i).posY*1.33-gridRow[8])
    );
    
 
    t.setWidth(int(elementData.get(i).radiusW*1.33));
    t.setHeight(int(elementData.get(i).radiusH*1.33));

 }

  }
    
    }}
    
  if (edit_mode == false) { 
    
   /* if (key =='t') {
    elementData.get(idElement).feedback_ = true;
    }
    */
    
   
    
    
       if (key == '+' || key =='9')    // aggiungi un item
    if ( control_key== true) {
     add_element = true;
   for (int ix=0; ix< 60 ; ix++) {   
   //  elementData.size
     Knob t = (Knob)cp5.get(Integer.toString(ix+1));
  //int mouse_X = (mouseX/35)*35;
 // int mouse_Y = (mouseY/35)*35;
 // mouse_X = mouse_X*50;
  hide_ = t.isVisible(); 
   if (hide_ == false && add_element==true) {t.show(); add_element= false; key = 0; elementData.get(ix).hide = 0; // elementData.get(ix).memoryPosition = 0;
 }
   }
   
    }
    
    if (key == '0'  && control_key== true   // cancella tutti gli item
    ) 
    {
     // delay(100);
  for (int del=0; del<60; del++)
   // if (  elementData.get(del-1).toggleMode != 26) 
 //  if (  elementData.get(del).dMode.getValue() != 26) 
   if (elementData.get(del).toggleMode != 26) {
   del_ = (Knob)cp5.get(Integer.toString(del+1));
   elementData.get(del).hide = 1;
    elementData.get(del).toggleMode = 0;
    elementData.get(del).toggleMode_2nd = 0;  
    del_.hide();   
   // del_.setVisible(false);
   
  }
  key = 0;
}
  if (key == '-' || key =='8')   /// cancella questo item
  if ( control_key== true //&& elementData.get(idElement).toggleMode != 26
  )
  if (elementData.get(idElement).toggleMode != 26) {
    Knob t = (Knob)cp5.get(Integer.toString(idElement+1));
  t.hide();
  elementData.get(idElement).hide = 1;
      elementData.get(idElement).toggleMode = 0;
    elementData.get(idElement).toggleMode_2nd = 0;

    }
  }
    
    if ( key == ENTER)
  //{ delay(100); elementData.get(idElement).labell.setFocus(false);      elementData.get(idElement).labell.setText(elementData.get(idElement).label);}
   {   Knob t = (Knob)cp5.get(Integer.toString(idElement+1));
    t.setLabel(elementData.get(idElement).label);
  //  labell.setText(elementData.get(idElement).label);
 // delay(10);
 elementData.get(idElement).label_hint.setFocus(false); // delay(10); // 

// elementData.get(idElement).hintarea.setText(elementData.get(idElement).hint_message);
 //  delay(100); 
   elementData.get(idElement).labell.setFocus(false);      
   //elementData.get(idElement).labell.setText(elementData.get(idElement).label);
}





    if (keyCode == CONTROL) {control_key = true; }
  }
}






//____________________________________________________________________________________________________________________________________________

void keyReleased(){
// Key_ = str(key);


keyCode = 0; key=0; control_key=false; keyCode_=""; Key_ =""; elementData.get(idElement).feedback_ = false;}


//____________________________________________________________________________________________________________________________________________


void mouseWheel(MouseEvent event) {
 
   // float e = event.getCount();
  if (event.getCount() == 1) //{mousewheel = constrain(mousewheel++,0,10); } else { mousewheel = constrain(mousewheel--,0,10);}
  mousewheel++; else mousewheel--;
  
 if (edit_mode == false) {
   
   
   
  if (keyCode == SHIFT) { 
  mousewheel = constrain(mousewheel,1,5);
 // println(e);
Knob t = (Knob)cp5.get(Integer.toString(idElement+1));
  t.setSize(30*mousewheel, 30*mousewheel);
    elementData.get(idElement).dimension = mousewheel; 
     elementData.get(idElement).radiusW = 30*mousewheel; 
     elementData.get(idElement).radiusH = 30*mousewheel; 
 //  t.setPosition(mouseX, mouseY);
 }
   
   
   
   
   if (key =='w' || key =='W') {   
   mousewheel = constrain(mousewheel,1,5);
   Knob t = (Knob)cp5.get(Integer.toString(idElement+1));
   t.setWidth(30*mousewheel);
   elementData.get(idElement).radiusW = 30*mousewheel;   
 }
 
    
    
    if (key =='h' || key =='H') { 
    mousewheel = constrain(mousewheel,1,5);
    Knob t = (Knob)cp5.get(Integer.toString(idElement+1));
    t.setSize((elementData.get(idElement).radiusW), 30*mousewheel);
    t.setHeight(30*mousewheel);
    elementData.get(idElement).radiusH = 30*mousewheel; 
    }
    
     if (key =='m' || key =='M') { 
    mousewheel = constrain(mousewheel,1,16);
    //Knob t = (Knob)cp5.get(Integer.toString(idElement+1));
    //t.setSize((elementData.get(idElement).radiusW), 30*mousewheel);
    //t.setHeight(30*mousewheel);
    elementData.get(idElement).User_byte_10 = mousewheel; 
    }
    
         if (key =='n' || key =='N') { 
    mousewheel = constrain(mousewheel,1,12);
    //Knob t = (Knob)cp5.get(Integer.toString(idElement+1));
    //t.setSize((elementData.get(idElement).radiusW), 30*mousewheel);
    //t.setHeight(30*mousewheel);
    elementData.get(idElement).User_byte_11 = mousewheel; 
    }
    
          if (key =='b' || key =='B') { 
    mousewheel = constrain(mousewheel,0,1);
    //Knob t = (Knob)cp5.get(Integer.toString(idElement+1));
    //t.setSize((elementData.get(idElement).radiusW), 30*mousewheel);
    //t.setHeight(30*mousewheel);
    elementData.get(idElement).User_byte_12 = mousewheel; 
    }
 

    if (keyCode == CONTROL) {
    mousewheel = constrain(mousewheel,1,12);
    changeshape(idElement);



 
 }
  }
 
}



//////////////////////////////////////////////////////////////////////////////////////////////////////////////////



void hint_message_send (int quale, int dove) {
 // elementData.get(idElement).hintarea.setText(elementData.get(idElement).hint_message);
//  String [] hint_message_righe = {"0","0","0","0","0","0","0","0"};
String [] hint_message_righe  = {".",".",".",".",".",".",".",".","."};
  String messaggio_entrata = elementData.get(quale).hint_message;
  String Messaggio_elaborato;
  int riga = 0;
   for (int ix=0; ix< messaggio_entrata.length() ; ix++) 
 {
  
   
 if ( messaggio_entrata.charAt(ix) == '_') 
 {
   if (hint_message_righe[riga].length() == 0 || hint_message_righe[riga].length() == '_') hint_message_righe[riga] = "_";
 riga++;
 }
  else hint_message_righe [constrain(riga,0,8)] = hint_message_righe[riga]+messaggio_entrata.charAt(ix);
  
 }
 
 Messaggio_elaborato = 
 (
  hint_message_righe[0].substring(1)+"\n"
 +hint_message_righe[1].substring(1)+"\n"
 +hint_message_righe[2].substring(1)+"\n"
 +hint_message_righe[3].substring(1)+"\n"
 +hint_message_righe[4].substring(1)+"\n"
 +hint_message_righe[5].substring(1)+"\n"
 +hint_message_righe[6].substring(1)+"\n"
 +hint_message_righe[7].substring(1)+"\n"
 +hint_message_righe[8].substring(1)+"\n"

 );
 //Messaggio_elaborato = (hint_message_righe[0]+" \n"+hint_message_righe[1]);
  hints2.setText("Hint box:  "+ Messaggio_elaborato);
 }

void change()  {
if (change2 != change) {
  change2 = change;
  change_do = true;

} else   change_do = false;


}




void keycode2string() {
switch (keyCode) {
case CONTROL:
keyCode_ = "CONTROL ";
break;
case UP:
keyCode_ = "UP ";
break;
case DOWN:
keyCode_ = "DOWN ";
break;
case RIGHT:
keyCode_ = "RIGHT ";
break;
case LEFT:
keyCode_ = "LEFT ";
break;
case ALT:
keyCode_ = "ALT ";
break;
case SHIFT:
keyCode_ = "SHIFT ";
break;

}





}


void feedback_off ()
{
if (feedback_counter == 4) {
for (byte i = 0;i < 60; i++)
{
//if (elementData.get(i).feedback_ == true)
elementData.get(i).feedback_ = false;
}

// saw.amp(0.0);
 wave.setAmplitude( 0.0 );
// 
 wave2.setAmplitude( 0.0 );
 
MIDI_IN_LED = false;
MIDI_OUT_LED = false;

}

}

void sawplay ()
{
  if ( soundstatus == true && sawplay == false) { // saw.stop(); 
  wave.setAmplitude( 0.0 );
  //delay(2);
  wave2.setAmplitude( 0.0 );
sawplay = true;}
  if ( soundstatus == false && sawplay == true) {// saw.play(); 
   wave.setAmplitude( 0.4 );
  // delay(2);
    wave2.setAmplitude( 0.4 );
sawplay = false;}
}

void show_piano ()
{
if (show_piano == 1) image(piano, gridCols[20]+(Betw2*0.95)
,gridRow[16]+rowBetw , 
gridCols[3] +(Betw2/12)
,  gridCols[1]); 
   // .setPosition(gridCols[20]+Betw2, gridRow[16]+rowBetw)
   //  .setSize((int) gridCols[3],(int) gridCols[3]/12)
}

void init_initmidi()
{
  if (init_initmidi ==0 ){
 //     fill(0, 255, 0);
 // textSize(Betw2/1.7);
 //  text("grgrgrgr ", gridCols[2]+(Betw2*0.4), (int) gridRow[1]+rowBetw*2.4); delay (400);
  
    initMidi();
  myBus = new MidiBus(this, inD, outD);   
  myBus.sendTimestamps(false);
  init_initmidi =1;}
}

void image_circuit() {
// if (infoGraph == 2000 || infoGraph == 2014) 
   view_circuit();
    if (view_ciruit_bool == true) // hide_all ();
    
{image(circuit_position, (int) gridCols[1],(int)gridRow[8] //+rowBetw
, (int) gridCols[15], (int) gridRow[16]+rowBetw); 
delay (10); delay(100);
//(int) gridCols[15]+Betw2/2,(int) gridRow[5]

}

}

void view_circuit ()
{
  view_ciruit_bool = false;
  
   if (page_selected == false  ) 
   {
     
   //  2000 memoryposition
    if (infoGraph==2000)    // memoryposition
    {
    view_ciruit_bool = true;
    }
    
     if (infoGraph==2014) { // led
   if ( elementData.get(idElement).toggleMode < 11 ) { view_ciruit_bool = true;} // buttons
   if ( elementData.get(idElement).toggleMode == 17 ) { view_ciruit_bool = true;} // page
     }
     
     if (infoGraph==2006) // selettore dmx
     {
     if ( elementData.get(idElement).toggleMode == 17 ) { view_ciruit_bool = true;} // page
     }
     
     if (infoGraph==2004) // selettore maxvalue
     {
     if ( elementData.get(idElement).toggleMode == 23 || elementData.get(idElement).toggleMode == 23) // SE MODE è TOUCH
   { view_ciruit_bool = true;} // page
     }
     
   } 
   else  // page_selected = true // seconda pagina
   {
       if (infoGraph==2000)    // memoryposition
    {
    view_ciruit_bool = true;
    }
     
      if (infoGraph==2014) {
   if ( elementData.get(idElement).toggleMode_2nd < 11 ) { view_ciruit_bool = true;} // buttons
    if ( elementData.get(idElement).toggleMode_2nd == 17 ) { view_ciruit_bool = true;} // page
      }
      
        if (infoGraph==2006) // selettore dmx
     {
     if ( elementData.get(idElement).toggleMode_2nd == 17 ) { view_ciruit_bool = true;} // page
     }
     
       if (infoGraph==2004) // selettore maxvalue
     {
     if ( elementData.get(idElement).toggleMode_2nd == 23 || elementData.get(idElement).toggleMode_2nd == 23) // SE MODE è TOUCH
   { view_ciruit_bool = true;} // page
     }
      
   }
  
}
