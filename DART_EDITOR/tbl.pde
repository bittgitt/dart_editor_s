String [] button_labels =      {"Modifier",       "Data byte1",  "Min",           "Max",          "MIDI CHANNEL",   "DMX CHANNEL ",  "MIDI TYPE",   "MODE", "KEY",             "HOT KEY",   "",   "",       "LED"}; // normal_labels
String [] pot_labels =         {"Modifier",       "Data byte1",  "Min",           "Max",          "MIDI CHANNEL",   "DMX CHANNEL ",  "MIDI TYPE",   "MODE", "KEY up",          "KEY down",  "",   "",       "LED"};
String [] page_labels =        {"Page switch",    "Data byte1",  "Min",           "Max",          "MIDI CHANNEL",   "Led 1",         "MIDI TYPE",   "MODE", "",                "",          "",   "",       "LED 2"};
String [] spin_labels =        {"SPINNER",        "Data byte1",  "SPEED ",        "",             "MIDI CHANNEL",   "SPIN MODE",     "MIDI TYPE",   "MODE", "TOUCH-STOP",      "",          "",   "",       "LED"};
String [] encoder_labels =     {"SPINNER",        "Data byte1",  "SPEED ",        "",             "MIDI CHANNEL",   "SPIN MODE",     "MIDI TYPE",   "MODE", "TOUCH-STOP",      "",          "",   "",       "LED"};
String [] touch_labels =       {"TOUCH SENSOR",   "Data byte1",  "sens/decay",    "LED OUT",      "MIDI CHANNEL",   "RESET VALUE ",  "MIDI TYPE",   "MODE", "RESET MODE",      "",          "",   "",       "touch mode"};
String [] mouse_labels =       {"MOUSE EMULATOR", "MouseWheel",  "X circuit pos", "Y circuit pos","",               "mouse speed",   "",            "MODE", "",                "",          "",   "",       "INVERT"};
String [] PADS_labels =        {"PADS",           "Data byte1",  "",              "",             "MIDI CHANNEL",   "",              "MIDI TYPE",   "MODE", "",                "",          "",   "",       "LED"};
String [] distance_labels =    {"DISTANCE SENSOR","Data byte1",  "MIN",           "MAX",          "MIDI CHANNEL",   "DMX CHANNEL",   "MIDI TYPE",   "MODE", "POT/BUTTON/scale","",          "",   "",       "LED"};
String [] GENERAL_labels =     {"GENERAL SETUP",  "1= NoMobo",   "1= ExtraPlex",  "0= PADS",      "",               "spinners",      "",            "MODE", "LED EFX",         "",          "",   "",       "touch decay"};
String [] SEQ_bt_labels =      {"SEQUENCE",       "Data byte1",  "A",             "B",            "MIDI CHANNEL",   "C",             "MIDI TYPE",   "MODE", "",                "",          "",   "",       "D"};
String [] matrix_pads_labels = {"SEQUENCE",       "Data byte1",  "B",             "C",            "MIDI CHANNEL",   "E",             "F",           "MODE", "G",               "H",         "I",  "L",      "M"};
String [] user_labels =        {"USER",           "Data byte1",  "A",             "B",            "MIDI CHANNEL",   "C",             "MIDI TYPE",   "MODE", "",                "",          "",   "",       "D"};
String [] reset_labels =       {"RESET",          "Data byte1",  "target",    "reset value",  "MIDI CHANNEL",   "C",             "",            "MODE", "",                "",          "",   "",       "D"};
String [] shifter_labels =     {"SHIFTER",        "",            "",              "",             "",               "",              "",            "MODE", "",                "",          "",   "target", ""};

int [] typeMidiArray={ 144, 160, 176, 192, 208, 224};
String [] typeMidiList= {
"Note", // 0
"PolyAT", // 1
"CC", // 2
"PC",  // 3
"AT", // 4
"PB" // 5
// ,"NOTE-ON-OFF"
};


String []  toggleList ={
  "Blind Input",      // 0
  "Button",           // 1
  "Toggle",           // 2
  "Toggle Group 1",   // 3
  "Toggle Group 2", 
  "Toggle Group 3", 
  "Toggle Group 4",   // 6
  "Radio Group 1",    // 7
  "Radio Group 2", 
  "Radio Group 3",
  "Radio Group 4",    // 10
"POT",                // 11
"Hypercurve 1",       // 12
"Hypercurve 2",       // 13
"Center-curve",       // 14
"Center-curve2",      // 15
"SEQ",           // 16
"page switch",   // 17
"distance sens.",// 18
"encoder",       // 19
"PIEZO-PADS",          // 20
"SPINNER 1",     // 21
"SPINNER 2 ",    // 22
"touch 1",       // 23
"touch 2",       // 24
"MOUSE",         // 25
"GENERAL",       // 26
"VELO-PADS",     // 27
"SPRITE",        // 28
"enc.RESET",     // 29
"SHIFTER",       // 30
"USER 1",        // 31
"USER 2",        // 32
"USER 3",        // 33
"USER 4"         // 34 
}; // 33
/*
String []  toggleList ={
  "Pot", // 0
  "Button", 
  "Toggle", 
  "Toggle Group 1", 
  "Toggle Group 2", 
  "Toggle Group 3", 
  "Toggle Group 4", 
  "Toggle Group 5", 
  "Toggle Group 6", 
  "Toggle Group 7",
  "Toggle Group 8",  //10
"Hypercurve 1",
"Hypercurve 2",
"Center-curve",
"Center-curve2", //14
"user 2",
"user 3", //16
"page switch",  //17
"distance sens.", //18
"BLIND INPUT",  //19
"PADS",  //20
"SPINNER1 SET.",  //21
"SPINNER2 SET.", // 22
"touch 1", // 23
"touch 2", // 24
"MOUSE/ARROWS",  //25
"GENERAL" }; // 26*/


String []  toggleList_spinner ={"0", "1"};
String []  toggleList_spinner_TOUCHSTOP ={"0_OFF", "1_SHIFT", "2_STOP"};
String []  keylist_touch ={"0", "1", "2"};
String []  keylist_general ={"0 no efx", "1 pots", "2 Spinners","3 buttons"};


//"DMX channel. 32 channels are available by default"
String []  DMX_Strings ={
"DMX channel. 32 channels are available by default",  // normal modifier pot-button
"0 = 63-65 endless mode. \n 1 = 0-127 endless mode. \n 2 = POT emulation mode \n 3 = RAMP - like pot-emulation, when the maximum value is reached, the progression continues starting from zero.\n-----\nSCALE_play_MODE: is activated with SPIN_MODE > 2 and MIDI-TYPE set to NOTE \nyou can define which notes of an octave will be part of the scale (a piano octave will appear). \nif no note is selected the scale-learn mode will be active. \nin scale_learn the machine will record a new scale every time the touchsensor is activated. \nnotes will be recorded from midi-in (DIN or USB) and from controller keys itself", // spinner  - 21 22
"LED OUTPUT for PAGE 1 \n - \n the LED output functionality for the page switch must be activated from the main tab on the Dart_sketch", // page switch - niente - 17
"DMX channel. 32 channels are available by default", // distance sensor  // normale scelta dmx - 18
"RESET VALUE  \n 64 is mid position \n this value will be used for Touch-Reset function and Virtual-Touch-Reset function \n The Spinner value returns to the Reset Position if it is NOT used (touched or turned)", // touch sensor - reset value
// -23 24
"MOUSE speed selector: \n 0 = mouse emulation disabled \n 16 = half speed \n 32 = normal speed \n 64 = double speed", //-25
"0 = NO Spinners . \n 1 = Top spinner ACTIVE. \n 2 = Side + Top spinner ACTIVE \n 3 = multiple encoders active",
"third fixed value of the sequence, if C = or the sequence will stop at the value B to start again from A",
""};

String []  MIN_Strings ={
"MINIMUM Value \n sets the lower limit of the second DATA BYTE of the midi message.",  // normal modifier pot-button
"SPEED \n Sets the speed multiplier of the Spinner (POT MODE) \n 8 = NORMAL speed \n It is recommended  to lower the speed, for mousewheel emulation \n Range: -32 - +32 \n Negative values will invert encoding direction. \n in endless mode it sets the speed of the light efx", // spinner  - 21 22
"", // page switch - niente - 17
"MINIMUM Value \n sets the lower limit of the second DATA BYTE of the midi message.", // distance sensor  // normale scelta dmx - 18
"Touch sensitivity value \n Decay-time value, if virtual-touch function is active.", // touch sensor - reset value // -23 24
"joystick X axis - citcuit position", //-mouse -25
"0 = NO Extra Plexer. \n1 = Extra Plexer ACTIVE - the secondary touch sensor will be disabled. \nUP to 8 Extra modifiers can be connected to the DartMobo",
"seq - amount of increment between one step of the sequence and the next \nif B > 0 the value of A will be fixed and will alternate in sequence with that of B",
""};

String []  MAX_Strings ={
"MAXIMUM Value \n sets the upper limit of the second DATA BYTE of the midi message. \n  In a common MIDI NOTE message this value defines the VELOCITY of the note-ON message.",  // normal modifier pot-button
"INVERT spinner message direction \n 0 = NORMAL. \n 1 = INVERTED.", // spinner  - 21 22
"", // page switch - niente - 17
"MAXIMUM Value \n sets the upper limit of the second DATA BYTE of the midi message. \n  In a common MIDI NOTE message this value defines the VELOCITY of the note-ON message.", // distance sensor  // normale scelta dmx - 18
"Touch sensitivity setup: Upper limit multiplier", // touch sensor - reset value
// -23 24
"joystick Y axis - citcuit position", //-25
"0 = PADS ACTIVE - the last analog input (A5) will be scanned at a higher frequency. Just set a modifier to PADS to access the Pads settings. \n 1 = NO PADS - the last analog input (A5) is free free for common modifiers (pot, buttons, sensors)",
"seq - second fixed value of the sequence ",
""};

String []  VALUE_Strings ={
"VALUE of the MIDI message. \n it's the FIRST DATA BYTE of the midi message. \n In a common MIDI NOTE message this value defines the PITCH of the note.",  // normal modifier pot-button
"VALUE of the MIDI message. \n it's the FIRST DATA BYTE of the midi message. \n In a common MIDI NOTE message this value defines the PITCH of the note.", // spinner  - 21 22
"VALUE of the MIDI message. \n it's the FIRST DATA BYTE of the midi message. \n In a common MIDI NOTE message this value defines the PITCH of the note.", // page switch - niente - 17
"VALUE of the MIDI message. \n it's the FIRST DATA BYTE of the midi message. \n In a common MIDI NOTE message this value defines the PITCH of the note.", // distance sensor  // normale scelta dmx - 18
"VALUE of the MIDI message. \n it's the FIRST DATA BYTE of the midi message. \n In a common MIDI NOTE message this value defines the PITCH of the note.", // touch sensor - reset value
// -23 24
"MOUSEWHEEL \n Set this selector to the MemoryPosition value of the encoder you want to emulate mousewheel movements, \n use SPEED settings of the encoder to setup it.", //-25
"0 = shifters and multiplexers are enabled \n 1 = shifters and multiplexers are disabled, the Arduino pins 4,5,6,10,11,12 can be used as output pins for LEDs.",
"seq - value ",
""};

String []  KEY_Strings ={
"KEYS: QWERTY keyboard emulation.\n"+
      "list of ASCII keys.\n"+
      "MIDI data will not be output in QWERTY key mode, choose NULL to have midi output. \n"+
      "- \n" + "In POT mode, two Keyboard signals can be controllerd by a potentiometer:.\n",
      
      
      // normal modifier pot-button
"TOUCH-STOP \n 0 = NO touch-stop \n 1 = touch SHIFT \n 2 = touch-STOP \n. \n The touch-stop function will stop the flow of midi data produced by the rotation of the Spinner. This is useful to have more precision and control over a parameter, avoiding the INERTIA of the spinner to influence a parameter. \n if SHIFT (1) is selected, the touch-stop function will just translate spinning data to the upper MIDI Channel if touched", // spinner  - 21 22
"", // page switch - niente - 17
"POT / BUTTON emulation \n 0 (null) = Potentiometer mode : the distance sensor produces a continuous MIDI output flow, like it was a potentiometer. \n 1 = BUTTON emulation : the sensor emulates an ON-OFF switch, distance will regulate the second data byte (velocity) of the MIDI signal. \n 2 = Scale play mode (set type to NOTE for scale-play!) ", // distance sensor  // normale scelta dmx - 18
"TOUCH RESET \n 0 (null) = no touch-reset \n 1 = touch reset ACTIVE \n_\nThe touch-reset function will turn back the Spinner value to a specified RESET-VALUE when it is not touched or turned (virtual touch)", // touch sensor - reset value
// -23 24
"", //-25
"LED lighting animations can be enabled here", // -26 general
"",
""

};   

String []  LED_Strings ={
"LED OUTPUT \n This value defines wich digital output will be controlled by the current modifier. \n 0 = no LED output. \n - \n If the item is set in POT mode, the value 1 will cause the potentiometer to control the lighting effects on the controller panel. \n The value 0 deactivates the effects. "
      ,  // normal modifier pot-button
"LED OUTPUT \n This value defines wich digital output will be controlled by the current modifier. \n choose a value that is outside the 74HC595 chain (higher than 31) to have NO LED output, and avoid conflicts. ", // spinner  - 21 22
"LED OUTPUT for page 2 \n - \n the LED output functionality for the page switch must be activated from the main tab on the Dart_sketch", // page switch - niente - 17
"LED OUTPUT \n This value defines wich digital output will be controlled by the current modifier. \n choose a value that is outside the 74HC595 chain (higher than 31) to have NO LED output, and avoid conflicts.  ", // distance sensor  // normale scelta dmx - 18
"TOUCH MODE: \n 0 = Capacitive touch \n 1 = Virtual Touch \n 2 = Sensor monitoring MODE", // touch sensor - reset value
// -23 24
"invert mouse direction: \n 0 = normal \n 1 = y axis inverted \n 2 = x axis inverted \n 3 = x and y axis inverted" , //-25
"touch decay time - \nyou can assign a delay to the OFF message of the touch sensors, \nin this way you eliminate unwanted repetitions of on-off signals",
"fourth fixed value of the sequence, if D = or the sequence will stop at the value C to start again from A",
""};

// "LED OUTPUT \n This value defines wich digital output will be controlled by the current modifier. \n choose a value that is outside the 74HC595 chain (higher than 31) to have NO LED output, and avoid conflicts. ");
  


// !°£$ %/à)
//  =(^,'.-0
//  12345678
//  9çò;ì:_"
//  ABCDEFGH
//  IJKLMNOP
//  QRSTUVWX

//  YZèù+&?\
//  abcdefgh
//  ijklmnop
//  qrstuvwx
//  yzé§*|



  String [] keyboardList = {"null", 
  "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8",  "f9", "f10",
  "f11",  "f12", "right arrow", "left arrow", "down arrow", "up arrow",   "left ctrl", "left shift",    "left alt", "tab", 
  "return", "esc", "delete",   "space",  "!", "°", "£", "$", "%", "/",
  "à", ")", "=", "(",   "^", ",", "'", ".", "-", "0", 
  "1", "2", "3", "4", "5", "6",    "7", "8",  "9", "ç",
  "ò", ";", "ì", ":", "_", "''",  "A", "B", "C",  "D",
  "E", "F", "G", "H",   "I", "J", "K", "L", "M", "N",
  "O", "P",    "Q", "R", "S", "T", "U", "V", "W", "X",  
  "Y", "Z", "è", "ù", "+",   "&", "?", "\\",   "a", "b", 
  "c", "d", "e", "f", "g", "h", "i", "j",    "k", "l",
  "m", "n", "o", "p",   "q", "r", "s", "t", "u", "v", 
  "w",   "x",  "y", "z", "é", "§", "*", "|",   "mouse left", "mouse right",
  "mouse right","mouse center","**","***"
  };

  String [] modifiersList = {"Null", "ctrl", "shift", "alt", "ctrl+shift", "ctrl+alt", "ctrl+alt+shift"
  };
  
  
  int [] memPosLUT = {
  // first multi
  6, 8, 4, 2,
  7, 1, 5, 3, 
  // second multi
  14,16,12,10,
  15,9,13,11,
  //terzo multi
  22,24,20,18,
  23,17,21,19,
  //quarto multi
  30,32,28,26,
  31,25,29,27,
  //quinto multi
  38,40,36,34,
  39,33,37,35,
  //sesto multi
  46,48,44,42,
  47,41,45,43,
  // 7
  54,56,52,50,
  55,49,53,51,
  // 8
  62,64,60,58,
  63,57,61,59
  
};

int [] hint_strings_map = {
1, //0
1,
1,
1,
1,
1,
1,
1,
1,
1,
1, //10
1,
1,
1,
1,
1,
8, // 16 seq 
3, //17
4, // 18
2, // 19
3, // 20
2, // 21
2, // 22
5, // 23
5, // 24
6, // 25
7, // general
1,  // 27
1,  // 28
1,  // 29
9,   // 30
9,   // 31
9,   // 32
9,   // 33
1,   //  34
};
