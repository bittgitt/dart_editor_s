
  void plug_page ()    // ATTENZIONE : ved anche CallbackListener doppioni - per regolare gli item che devono avere MODDE uguale in entrambe le pagine!!!
  // stacca e riattacca i numberbox alle variabili corrette a seconda della pagina attuale 
  // se necessario cambia le etichette nei selettori a tendina
 
 {
  if (page_selected == false){ // sulla prima pagina  - nota : quando carico un preset , vengo portato subito sulla prima pagina
  
    if ( toggleMode != 17  // 17 = page - tutto pluggato
      && toggleMode != 26  // general - tutto pluggato
      && toggleMode != 28  // 28 = Sprite mode - tutto unpluggato  
    ) 
    
    {  // PAGE (17) e general (26) devono essere uguali
      
        nT.unplugFrom (this); //  else note = note_2nd;
        nT.plugTo( this, "note" );
        nT.setValue(note[0]);
    
        min.unplugFrom (this);
        min.plugTo( this, "setExcursionControllMin" );
        min.setValue(setExcursionControllMin);
        
        max.unplugFrom (this);
        max.plugTo( this, "setExcursionControllMax" );
        max.setValue(setExcursionControllMax);
        
       led.unplugFrom (this);
       led.plugTo( this, "led" );
       led.setValue(led_);
      
        midiC.unplugFrom (this);
        midiC.plugTo( this, "midiChannel" );
        midiC.setValue(midiChannel);
        
        dmx.unplugFrom (this);
        dmx.plugTo( this, "addressDMX" );
        dmx.setValue(addressDMX);
      
        key_numeric.unplugFrom (this);
        dKeys.unplugFrom (this);
        dKeys.plugTo( this, "keyBoard" );
        dKeys.setValue(keyBoard);
      
           if (toggleMode < 21) 
                                       dKeys.setLabel(keyboardList[keyBoard]); 
           else  if (toggleMode != 26 ){if (keyBoard < keylist_touch.length ) dKeys.setLabel(keylist_touch[keyBoard]);}
           else                        dKeys.setLabel(keylist_general[keyBoard]);
       
   
        dModif.unplugFrom (this);
        dModif.plugTo( this, "modifiers" );
        dModif.setValue(modifiers);
        if (toggleMode == 11) dModif.setLabel(keyboardList[modifiers]);    
        else  { // println(modifiers); 
        if (modifiers < 7) dModif.setLabel(modifiersList[modifiers]); }
       
        midiTypeOpt.unplugFrom (this);
        midiTypeOpt.plugTo( this, "indexMidiType" );
        midiTypeOpt.setLabel(typeMidiList[indexMidiType]);
        midiTypeOpt.setValue(indexMidiType);
        
  }   
  
          if ( toggleMode < 17 // il selettore mode viene ripluggato solo per modalità in cui può essere diverso da pagina a pagina
       //    || toggleMode == 0 
            || toggleMode > 27
          )    // se siamo in modalità BLIND INPUT 
       { dMode.unplugFrom (this);
       dMode.plugTo( this, "toggleMode" );
       dMode.setValue(toggleMode);
       }
          
          if ( toggleMode == 26)  // nel caso del general 
                                  //mode plugga i numberbox sulle varabili di entrambe le pagine
        {plugga_page();}
             //  labeler();
        
        if ( toggleMode > 28 && toggleMode < 34)   // user
        {
        dKeys.unplugFrom (this);        
        key_numeric.plugTo( this, "keyBoard" );
        key_numeric.setValue(keyBoard);
        }
        
  }

   if (page_selected == true) { // passiamo alla seconda pagina
     
  if ( toggleMode != 17  // page - tutto pluggato
  &&  toggleMode != 26 //  26 = general - tutto pluggato
     && toggleMode != 28  // Sprite mode - tutto unpluggato
      )
      
  {  
    
       nT.unplugFrom (this);  // else note_2nd = note;
       nT.plugTo( this, "note_2nd" );
       nT.setValue(note[1]);
   
       min.unplugFrom (this);
       min.plugTo( this, "setExcursionControllMin_2nd" );
       min.setValue(setExcursionControllMin_2nd);
       
       led.unplugFrom (this);
       led.plugTo( this, "led_2nd" );
       led.setValue(led_2nd);
        
       max.unplugFrom (this);
       max.plugTo( this, "setExcursionControllMax_2nd" );
       max.setValue(setExcursionControllMax_2nd);
        
       midiC.unplugFrom (this);
       midiC.plugTo( this, "midiChannel_2nd" );
       midiC.setValue(midiChannel_2nd);
        
       dmx.unplugFrom (this);
       dmx.plugTo( this, "addressDMX_2nd" );
       dmx.setValue(addressDMX_2nd);
        
         key_numeric.unplugFrom (this);
       dKeys.unplugFrom (this);
       dKeys.plugTo( this, "keyBoard_2nd" );
       dKeys.setValue(keyBoard_2nd);
       
         if (toggleMode_2nd < 21) dKeys.setLabel(keyboardList[keyBoard_2nd]); 
         else  if (toggleMode != 26 ) {if (keyBoard_2nd <  keylist_touch.length ) dKeys.setLabel(keylist_touch[keyBoard_2nd]);}
         else dKeys.setLabel(keylist_general[keyBoard_2nd]);
      
        
         dModif.unplugFrom (this);
         dModif.plugTo( this, "modifiers_2nd" );
         dModif.setValue(modifiers_2nd);
         if (toggleMode_2nd == 11) dModif.setLabel(keyboardList[modifiers_2nd]); 
         else {if (modifiers < 7) dModif.setLabel(modifiersList[modifiers_2nd]); }
       
        
        
       midiTypeOpt.unplugFrom (this);
       midiTypeOpt.plugTo( this, "indexMidiType_2nd" );       
       midiTypeOpt.setLabel(typeMidiList[indexMidiType_2nd]);
       midiTypeOpt.setValue(indexMidiType_2nd);
  }    
             if ( toggleMode < 17 
          //   || toggleMode == 0   // item in modalità BLIND INPUT
          || toggleMode > 27
          )
             { dMode.unplugFrom (this);
       dMode.plugTo( this, "toggleMode_2nd" );
       dMode.setValue(toggleMode_2nd);}        
       // labeler();
       
       if ( toggleMode > 28 && toggleMode < 34)   // user
        {
        dKeys.unplugFrom (this);        
        key_numeric.plugTo( this, "keyBoard_2nd" );
        key_numeric.setValue(keyBoard_2nd);
        }
  }
  }
  
  //---------------------------------------------------------------------------------------------------------------------
  
  
  
  
  
  void plugga_page() {  //                 linka un SELETTORE sulla prima e seconda pagina - adesso modificherà due variabili
midiC.plugTo( this, "midiChannel" ); midiC.plugTo( this, "midiChannel_2nd" ); 
nT.plugTo( this, "note" ); nT.plugTo( this, "note_2nd" );
min.plugTo( this, "setExcursionControllMin" ); min.plugTo( this, "setExcursionControllMin_2nd" );
led.plugTo( this, "led_" ); led.plugTo( this, "led_2nd" );
max.plugTo( this, "setExcursionControllMax" ); max.plugTo( this, "setExcursionControllMax_2nd" );
midiC.plugTo( this, "midiChannel" ); midiC.plugTo( this, "midiChannel_2nd" );
dmx.plugTo( this, "addressDMX" ); dmx.plugTo( this, "addressDMX_2nd" );
midiTypeOpt.plugTo(this, "indexMidiType" ); midiTypeOpt.plugTo(this, "indexMidiType_2nd" );
dKeys.plugTo( this, "keyBoard" ); dKeys.plugTo( this, "keyBoard_2nd" );
  }
  
  
  
  //----------------------------------------------------------------------------------------------------------------
  
  void splugga() {  //                                                               linka un SELETTORE sulla prima e seconda pagina - adesso modificherà due variabili
midiC.unplugFrom (this);
nT.unplugFrom (this);
min.unplugFrom (this); 
max.unplugFrom (this);

led.unplugFrom (this);
midiC.unplugFrom (this);
dmx.unplugFrom (this);
dKeys.unplugFrom (this);
//scale1.unplugFrom (this);
  }
  
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  
