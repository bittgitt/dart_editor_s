//THE CLASS OBJ of the ITEM 

ArrayList <myUI> elementData = new ArrayList <myUI>();
ControlP5 cp5;

class myUI {
  //**************************  Data
  
  
  int led_;
  int led_2nd;
  int memoryPosition = 1;
  int mem;
  boolean selected = false;
  boolean noteControll;
  
  int note[] = {0,0};
  int setExcursionControllMin;
  int setExcursionControllMin_2nd;
  int setExcursionControllMax; 
  int setExcursionControllMax_2nd;  
  int midiChannel;
  int midiChannel_2nd;
  int addressDMX;
  int addressDMX_2nd;
  int toggleMode = 0;
  int toggleMode_2nd = 0;
  int indexMidiType; //nota : optCC= typeMidiList[(indexTypeMidi)];
  int indexMidiType_2nd;
  int keyBoard;
  int keyBoard_2nd;
  int modifiers;
  int modifiers_2nd;
  
  int User_byte_1;
  int User_byte_2;
  int User_byte_3; 
  int User_byte_4;
  int User_byte_5;
  int User_byte_6;
  int User_byte_7; 
  int User_byte_8;
  int User_byte_9;
  int User_byte_10;
  int User_byte_11;
  int User_byte_12;
  
  int User_byte_14;
  
  
  boolean noteCC;
 
  int hide;
 
  int shape;
  int dimension =1;
  String label;
  String hint_message = ".";
  String temp;
  int posX;
  int posY;
  int radiusW;
  int radiusH;
  // dimension of the numberbox
  int boxX=60;
  int boxY=24;
  
  float encoder_rotate = 64;
  
  boolean feedback_;

  /////////// instances of default class
  ControlP5 sett;
  //****************************** the button data

  boolean stateButton;
  boolean stateButton_2nd;
  boolean togglestate;
  boolean togglestate_2nd;

  //******************************  
  //  for menus
  //****************************** 
  Numberbox mp, min, nT, diC, max, midiC, dmx, led, key_numeric;
  ScrollableList dMode, midiTypeOpt,  dModif, dKeys;
  Knob myKnobA;
 // ListBox dKeys;
  Textfield labell, label_hint;

      
 
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
void settingsBox() {
     
 
  
label_hint =  sett.addTextfield("label_hint")
             .setPosition((int) (gridCols[20]+Betw2*0.8),(int)gridRow[12]+rowBetw)
             .setSize( Betw2*6, (int)(rowBetw*1.5)) 
             .plugTo(this, "label_hint" )
             .setFont(createFont("typeO.TTF", (Betw2/1.7)))
             .setAutoClear(false)
             .setId(2012)
             .setColorBackground(color((color_R)*0.8,(color_G)*0.8,(color_B)*0.8)) 
             .setColorForeground(color(color_R*0.8,color_G*0.8,color_B*0.8))  
             .setColorValueLabel(color(color_R*0.6,color_G*0.6,color_B*0.6)) 
             .setColorCaptionLabel(color(color_R*1.6,color_G*1.6,color_B*1.6))
             .setLabel("hint")
             .setColor(color(250))
              .onChange(general_open) 
           .onChange(set_colore)
           .onClick(set_colore2)
           .onEnter(set_colore3)
           .onLeave(set_colore)
             .setVisible(false)
                .onEnter(shutdown)
           .onLeave(turnon)
             ;
       
labell =  sett.addTextfield("label")
           .setPosition( (int) (gridCols[20]+Betw2*0.8), (int)gridRow[14]+rowBetw   )  // gridRow[10]+rowBetw
           .setSize( Betw2*6, (int)(rowBetw*1.5)) 
           .plugTo(this, "label" )
           .setFont(createFont("typeO.TTF", (Betw2/1.7)))   
           .setAutoClear(false )
           .setId(2013)
           .setColorBackground(color(color_R*0.8,color_G*0.8,color_B*0.8)) 
           .setColorForeground(color(color_R*0.8,color_G*0.8,color_B*0.8))  
           .setColorValueLabel(color(color_R*0.6,color_G*0.6,color_B*0.6)) 
           .setColorCaptionLabel(color(color_R*1.6,color_G*1.6,color_B*1.6))
           .setLabel("name")
           .setColor(color(250))
           .setVisible(false)
           .onChange(set_colore)
           .onClick(set_colore2)
           .onEnter(set_colore3)
           .onLeave(set_colore)
              .onEnter(shutdown)
           .onLeave(turnon)
           ;
    
midiTypeOpt = sett.addScrollableList("midiTypeOpt")
           .plugTo(this, "indexMidiType" )
           .setPosition( (int)gridCols[20]+Betw2*0.8, gridRow[6]+rowBetw)
           .setSize(Betw2*6, Betw2*6)
           .setBarHeight(21)
           .setItemHeight(30)
           .setOpen(false)
           .onEnter(toFront)
           .onLeave(close)
           .setType(ControlP5.DROPDOWN)
           .setLabel(typeMidiList[indexMidiType])
           .setId(2001)
           .addItems(typeMidiList)
           // .addCallback(open_scale) 
           .onChange(open_scale)
           .setColorBackground(color(color_R*0.8,color_G*0.8,color_B*0.8)) 
           .setColorForeground(color(color_R*0.7,color_G*0.7,color_B*0.7))  
           .setColorValueLabel(color(color_R*1.6,color_G*1.6,color_B*1.6)) 
           .setColorCaptionLabel(color(250))    
            .onEnter(shutdown)
           .onLeave(turnon)
       //       .onChange(set_colore)
       //    .onClick(set_colore2)
       //    .onEnter(set_colore3)
       //    .onLeave(set_colore)
           
           ;
      
min = sett.addNumberbox("setExcursionControllMin",  (int) gridCols[17]+Betw2, (int)gridRow[6]+rowBetw, (int)(Betw2*3), (int)(rowBetw*1.5))
           .plugTo( this, "setExcursionControllMin" )
           .setId(2003)
           .setLabel("Minimum")     
           .setColorCaptionLabel(color(color_R*1.6,color_G*1.6,color_B*1.6))   
           .setRange(-127, 127)
           .onLeave(closenumberbox)   
           .setColorBackground(color(color_R*0.8,color_G*0.8,color_B*0.8)) 
           .setColorForeground(color(color_R*0.8,color_G*0.8,color_B*0.8))  
           .setColorValueLabel(color(250)) 
           .setColorCaptionLabel(color(color_R*1.6,color_G*1.6,color_B*1.6))
           .onChange(set_colore)
           .onClick(set_colore2)
           .onEnter(set_colore3)
           ;
      
max = sett.addNumberbox("setExcursionControllMax", (int) gridCols[17]+Betw2, (int)gridRow[8]+rowBetw, (int)(Betw2*3), (int)(rowBetw*1.5))
           .plugTo( this, "setExcursionControllMax" )
           .setId(2004)
           .setValue(127)
           .setLabel("Maximum")
           .setColorBackground(color(color_R*0.8,color_G*0.8,color_B*0.8)) 
           .setColorForeground(color(color_R*0.8,color_G*0.8,color_B*0.8))  
           .setColorValueLabel(color(250)) 
           .setColorCaptionLabel(color(color_R*1.6,color_G*1.6,color_B*1.6))
           .setRange(0, 127)
           .onLeave(closenumberbox)
           .onChange(general_open) 
           .onChange(set_colore)
           .onClick(set_colore2)
           .onEnter(set_colore3)
           ;  
      
      
      
nT = sett.addNumberbox("note",  (int) gridCols[17]+Betw2 , (int)gridRow[4]+rowBetw, (int)(Betw2*3), (int)(rowBetw*1.5))
          .plugTo( this, "note" )
          .setId(2002)
          .setRange(0, 127)
          .onLeave(closenumberbox)
          .onChange(general_open) 
          .setLabel("Value")
          .setColorBackground(color(color_R*0.8,color_G*0.8,color_B*0.8)) 
          .setColorForeground(color(color_R*0.8,color_G*0.8,color_B*0.8))  
          .setColorValueLabel(color(250)) 
          .setColorCaptionLabel(color(color_R*1.6,color_G*1.6,color_B*1.6))  
           .onChange(general_open) 
           .onChange(set_colore)
           .onClick(set_colore2)
           .onEnter(set_colore3)
          ;
          if (toggleMode == 17)   // page
          nT.plugTo( this, "note_2nd" );
       

midiC = sett.addNumberbox("midiChannel", (int) gridCols[17]+Betw2, (int)gridRow[10]+rowBetw,(int)(Betw2*3), (int)(rowBetw*1.5))
          .plugTo( this, "midiChannel" )
          .setId(2005)
          .setLabel("Midi Channel")
          .setColorBackground(color(color_R*0.8,color_G*0.8,color_B*0.8)) 
          .setColorForeground(color(color_R*0.8,color_G*0.8,color_B*0.8))  
          .setColorValueLabel(color(250)) 
          .setColorCaptionLabel(color(color_R*1.6,color_G*1.6,color_B*1.6))
          .setRange(1, 16)
          .onLeave(closenumberbox)
           .onChange(general_open) 
           .onChange(set_colore)
           .onClick(set_colore2)
           .onEnter(set_colore3)
          ;
      
      
dmx = sett.addNumberbox("addressDMX", (int) gridCols[17]+Betw2, (int)gridRow[12]+rowBetw, (int)(Betw2*3), (int)(rowBetw*1.5))
          .plugTo( this, "addressDMX" )
          .setId(2006)
          .setLabel("DMX Channel")
          .setColorBackground(color(color_R*0.8,color_G*0.8,color_B*0.8)) 
          .setColorForeground(color(color_R*0.8,color_G*0.8,color_B*0.8))  
          .setColorValueLabel(color(250)) 
          .setColorCaptionLabel(color(color_R*1.6,color_G*1.6,color_B*1.6))  
          .setRange(0, 127)
         ///  .addCallback(open_scale) 
          .onLeave(closenumberbox)
          .onChange(general_open) 
           .onChange(general_open) 
           .onChange(set_colore)
           .onChange(open_scale)
           .onClick(set_colore2)
           .onEnter(set_colore3)
          ;
      
      
      //////////////////////////////////////////////////////////////////////////////////////////////////////////////
      
    key_numeric = sett.addNumberbox("key_num", (int)(gridCols[20]+Betw2*0.8), (int)gridRow[8]+rowBetw, (int)(Betw2*3), (int)(rowBetw*1.5))
          .plugTo( this, "keyBoard" )
          .setId(2023)
          .setLabel("")
          .setColorBackground(color(color_R*0.8,color_G*0.8,color_B*0.8)) 
          .setColorForeground(color(color_R*0.7,color_G*0.7,color_B*0.7))  
          .setColorValueLabel(color(250)) 
          .setColorCaptionLabel(color(color_R*1.6,color_G*1.6,color_B*1.6))  
          .setRange(0, 127)
          .addCallback(open_scale) 
          .onLeave(closenumberbox)
          .onChange(general_open) 
          ;
          
        ///
        
        
    
dMode =  sett.addScrollableList("toggleMode")     
           .setPosition((int)gridCols[20]+Betw2*0.8, gridRow[4]+rowBetw)
           .setSize(Betw2*6, Betw2*8)
           .plugTo( this, "toggleMode" )
           .onEnter(toFront)
           .onLeave(close)
           .setId(2007)
           .setBarHeight(21)
           .setItemHeight(30)
           .addItems(toggleList)
           .setOpen(false)
           .setType(ControlP5.DROPDOWN)
           .setLabelVisible(true) 
           .onChange(doppioni) 
           .setColorBackground(color(color_R*0.8,color_G*0.8,color_B*0.8)) 
           .setColorForeground(color(color_R*0.7,color_G*0.7,color_B*0.7))  
           .setColorValueLabel(color(color_R*1.6,color_G*1.6,color_B*1.6)) 
           .setColorCaptionLabel(color(250))
            .onEnter(shutdown)
           .onLeave(turnon)
           .setLabel(toggleList[toggleMode]);
          
           ;
     

  
  
dKeys = sett.addScrollableList("keyBoard")
        .setPosition( (int)gridCols[20]+Betw2*0.8, gridRow[8]+rowBetw)
        .setSize(Betw2*6, Betw2*8)
        .plugTo( this, "keyBoard" )
        .onEnter(toFrontBox)
        .onLeave(closeBox)
        .setId(2010)  
        .setBarHeight(21)
        .setItemHeight(35)
        .addItems(keyboardList)
        .setType(ControlP5.DROPDOWN)
        .setOpen(false)
        .onChange(general_open) 
        .setColorBackground(color(color_R*0.8,color_G*0.8,color_B*0.8)) 
        .setColorForeground(color(color_R*0.7,color_G*0.7,color_B*0.7))  
        .setColorValueLabel(color(color_R*1.6,color_G*1.6,color_B*1.6)) 
        .setColorCaptionLabel(color(250))
        .setLabel(keyboardList[keyBoard])   // la scrittina sotto // non gli elementi scrollabili
           .onEnter(shutdown)
           .onLeave(turnon)
        ;
        dKeys.getValueLabel().toUpperCase(false);
     
     
dModif = sett.addScrollableList("Modifiers")
          .setPosition( (int)gridCols[20]+Betw2*0.8, gridRow[10]+rowBetw)
          .setSize(Betw2*6, Betw2*8)
          .plugTo( this, "modifiersBox" )
          .onEnter(toFront)
          .onLeave(close)
          .setId(2009)
          .setBarHeight(21)
          .setItemHeight(30)
          .addItems(keyboardList)
          .setType(ControlP5.DROPDOWN)
          .setOpen(false)
          .setColorBackground(color(color_R*0.8,color_G*0.8,color_B*0.8)) 
          .setColorForeground(color(color_R*0.7,color_G*0.7,color_B*0.7))  
          .setColorValueLabel(color(color_R*1.6,color_G*1.6,color_B*1.6)) 
          .setColorCaptionLabel(color(250))
          .setLabel(modifiersList[modifiers])
             .onEnter(shutdown)
           .onLeave(turnon)
          ;
  
 
mp = sett.addNumberbox("memorySlot",  (int) gridCols[17]+Betw2  , (int) gridRow[16]+rowBetw,  (int)(Betw2*3), (int)(rowBetw*1.5)) //  
          .onLeave(mp_doppioni)
          .plugTo( this, "memoryPosition" )
          .setId(2000)
          .setRange(0, 64)
          .setColorBackground(color(color_R*0.8,color_G*0.8,color_B*0.8)) 
          .setColorForeground(color(color_R*0.8,color_G*0.8,color_B*0.8))  
          .setColorValueLabel(color(250)) 
          .setColorCaptionLabel(color(color_R*1.6,color_G*1.6,color_B*1.6))
          .setLabel("Memory pos.")
          .onLeave(closenumberbox)
           .onChange(general_open) 
           .onChange(set_colore)
           .onClick(set_colore2)
           .onEnter(set_colore3)
          .setVisible(false);
          ;
    
    
    
    
led = sett.addNumberbox("LED-OUT", (int) gridCols[17]+Betw2  , (int) gridRow[14]+rowBetw, (int)(Betw2*3), (int)(rowBetw*1.5)) //   
          .plugTo( this, "led_" )
          .setId(2014)
          .setRange(0, 64)
          .setColorCaptionLabel(color(color_R*1.6,color_G*1.6,color_B*1.6))
          .setLabel("LED")
          .setColorBackground(color(color_R*0.8,color_G*0.8,color_B*0.8)) 
          .setColorForeground(color(color_R*0.8,color_G*0.8,color_B*0.8))  
          .setColorValueLabel(color(250)) 
          .setColorCaptionLabel(color(color_R*1.6,color_G*1.6,color_B*1.6))
           .onChange(general_open) 
           .onChange(set_colore)
           .onClick(set_colore2)
           .onEnter(set_colore3)
 
          .onLeave(closenumberbox)
          ;
    ;
      
      
    // makeEditable( mp );
    makeEditable( nT );
    makeEditable( mp );
    makeEditable( led );
    makeEditable( min ); 
    makeEditable( max ); 
    makeEditable( midiC ); 
    makeEditable( dmx );
    
    labeler ();

  }
  
   myUI (PApplet applet, ControlP5 cp5_, String label_, int mem_pos, int id_, float posx_, float posy_, int rW, int rH, int setView_) {
    label = label_;
    shape = setView_;

    posX = int(posx_);
    posY = int(posy_);
  
    radiusW = rW;
    radiusH = rH;

    sett = new ControlP5(applet);
    sett.setFont(f, (int)(Betw2*0.6));
    sett.setColorBackground(color(90, 90, 90) );
    
    

   
    
      cp5_.addKnob(label)
         .setId(id_)
               .setRange(0,127)
               .setValue(64)
               .setLabelVisible(false)            
               .setNumberOfTickMarks(0) 
               .setViewStyle(3)
               .setTickMarkLength(0)            
             //  .setDragDirection(Knob.VERTICAL)
               
                 .setDragDirection(Knob.HORIZONTAL)
              //  .setView(new Manopolox()) 
               .onChange(send_ctrl)
               .onReleaseOutside(leave_item)
               ;
    
     settingsBox();
      memoryPosition=mem_pos;
  }
  
  
  boolean getStateButton () {
  if (page_selected == false) { return stateButton;}
  else {return stateButton_2nd;}
  }
  void setStateButton (boolean b) {
    if (page_selected == false) { stateButton = b;}
    else  stateButton_2nd = b;
   
  }
  ////////////// THE BOX SETTING DESIGN AND TOGGLE///////////////////////

  void setDisplay (boolean b) {
    sett.setVisible(b); // to set visibility of box settings on
    if (b) {sett.show(); }else sett.hide();
  }
  
  boolean getStateDisplay() {
    //println("sett is visible: " +  sett.isVisible());
    return sett.isVisible();
  }

 

  // THE LISTENER EVENT TO CAPTURE THE TOGGLE EVENT //

  void note(int v) {
    note[0] = v;
    //    println("Value of the Note: "+ v);
  }
    void note_2nd(int v) {
    note[1] = v;
    //    println("Value of the Note: "+ v);
  }
  
  void setExcursionControllMin(int v) {
    setExcursionControllMin = v;
    // println(" Minimum Value: "+ v);
  }
  
  void setExcursionControllMax(int v) {
    setExcursionControllMax= v;
    //println(" Maximum Value: "+ v);
  }
  void midiChannel(int v) {
    midiChannel = v;
    //println("Value of MIDI Channel: "+v);
  }
  void addressDMX(int v) {
    addressDMX = v;
    //println("Value of DMX Address: "+v);
  }
  void toggleMode(int f) {  
   // println("toggle: " + f);
    toggleMode = f;
  }
  void labell(String f) {  
  //  println("toggle: " + f);
    label = f;
    labell.setText(elementData.get(idElement).label);
  labell.setFocus(false);
  }
  void label_hint (String f) {  
  //  println("toggle: " + f);
    hint_message = f;
  // label_hint.setText(elementData.get(idElement).hint_message);
  // labell.setFocus(false);
  //  label_hint.setFocus(false);
  }
  public void midiTypeOpt(int a) {
    indexMidiType =  a;
 //   println("index midi type: " + indexMidiType);
  }
 
  
  void modifiersBox(int f) {  
    //println("toggle", f);
    modifiers = f;
    //println(f);
  }
  
  
  
   void User_byte_1(int f) {
    //println("togglePage", f);
     User_byte_1 = f;
  }
  
  
   void led(int f) {
    //println("togglePage", f);
    led_= f;
    //  togglePage = f;
  }
  
  
  void memoryPosition(int v) {
     memoryPosition = v;
     mem = memoryPosition;
   //  mp.setValue(memoryPosition);
    //   println("Value of Memory Position: "+v);
  }
  
  //-----------------------------------------------------------------------------------------------------------
  void displaySettingsUI() {

      fill((color_R)*1.55,(color_G)*1.55,(color_B)*1.55);
    textSize(Betw2/1.6);
    // textSize(Betw2/1.7);
    text("Settings box: ITEM "
    , gridCols[17]+ Betw2*0.5
    , gridRow[3]  +rowBetw/2
    ); // box settings  label
 
   fill(255);
      text(label,                  // scrivi il nome dell'item
      gridCols[20]+ Betw2*0.5
    , gridRow[3]+rowBetw/2); // box settings  label
    
  }
  
  
  /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
  
