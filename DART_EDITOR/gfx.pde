//********************************** 
//  POSITION GLOBAL VARIABLES FOR THE LOOK OF THE EDITOR
//  interface grid for element location
//**********************************
int cols = 25;
int rows = 25;
float [] gridRow = new float [rows];
float [] gridCols = new float [cols];
float W, H;
PImage logo;//, jogWheel ; 
PImage circuit_position;
PImage piano;
PImage cursor;

PImage square1;PImage square2;PImage square3;PImage square4;PImage square5;
PImage circle1;PImage circle2;PImage circle3;PImage circle4;PImage circle5;
PImage mask1; PImage mask2; PImage mask3; PImage mask4; PImage mask5;

int spaceBetw;
int Betw2;
int Betw3;
int rowBetw;

//logo = loadImage("logo.gif") ;
//**********************************
/// THE AMBIENT VARIABLES
//***********************************
PFont f;
int fontDimension=10;
int rW = 60;
int rH = 60;
int rR = 30;

void settingScreen() {

  W=width/rows;
  H=height/cols;

  for (int i = 0; i < cols; i++) {
    gridCols[i] = W*i ;
    // println("i: "+ gridCols[i]);
  }
  for (int j = 0; j < rows; j++) {
    gridRow[j] =  H*j ;
    // println("j: "+ gridRow[j]);
  }
   spaceBetw =  int(gridCols[1]*1.3);
  rowBetw = int(gridRow[1]/2);
  Betw2 =  int(gridCols[1]/2);
  Betw3 =  int(gridCols[1]/1.5);
  
}

// *****************************************
//           the background look
//******************************************

void interfaceDisp() {
  // Background
 // background(colore_);
  background(background);
//   background(100, 50, 30);
  
  // box for Setting Label
  strokeWeight(3);



  stroke((color_R)*1.5,(color_G)*1.5,(color_B)*1.5);
  fill(color_R,color_G,color_B);

if (full_screen == false)  {
  rect(gridCols[17],  gridRow[2]+rowBetw, gridCols[7], gridRow[16]   ,7);  // settings box
   
     fill((color_R)*1.6,(color_G)*1.6,(color_B)*1.6);
   textSize(Betw2/1.7);
   
    
// String [] normal_labels = {"Modifier", "VALUE", "MINIMUM", "MAXIMUM", "MIDI CHANNEL", "DMX CHANNEL ", "MIDI TYPE", "MODE", "KEY", "KEY MODIFIERS", "-", "- "};
// String [] page_labels = {"Page switch", "VALUE", "-", "-", "MIDI CHANNEL", "- ", "MIDI TYPE", "-", "-", "-", "-", "- "};
// String [] spin_labels = {"SPINNER", "VALUE", "SPEED ", "INVERT", "MIDI CHANNEL", "endless type", "MIDI TYPE", "POT/ENDLESS", "TOUCH-STOP", "-", "-", "- "};
// String [] touch_labels = {"TOUCH SENSOR", "VALUE", "-", "-", "MIDI CHANNEL", "RESET VALUE ", "MIDI TYPE", "RESET MODE", "-", "-", "-", "-"};
// String [] mouse_labels = {"MOUSE EMULATOR", "ciao", "pippo", "baudo", "beppe", "grillo ", "gatto", "luigi", "di", "maio", "nese", "ciro "};
//  String [] distance_labels = {"DISTANCE SENSOR", "ciao", "pippo", "baudo", "beppe", "grillo ", "gatto", "luigi", "di", "maio", "nese", "ciro "}; 

         if ( elementData.get(idElement).dMode.getValue()  ==0)             // Scrivi etichette sotto i selettori a tendina.
 {text(page_labels[7], gridCols[20]+spaceBetw/3  , gridRow[6]-rowBetw*0.5  );
  text("", gridCols[20]+spaceBetw/3  , gridRow[8]-rowBetw*0.5  ); // comunque Scrivi MODE - page_labels [7]
  text("", gridCols[20]+spaceBetw/3 , gridRow[10]-rowBetw*0.5  );
  text("", gridCols[20]+spaceBetw/3 , gridRow[12]-rowBetw*0.5  );
   }
   
   else if ( elementData.get(idElement).dMode.getValue()  ==17)
   { text(page_labels[7], gridCols[20]+spaceBetw/3  , gridRow[6]-rowBetw*0.5 );
  text(page_labels[6], gridCols[20]+spaceBetw/3  , gridRow[8]-rowBetw*0.5 );
  text(page_labels[8], gridCols[20]+spaceBetw/3 , gridRow[10]-rowBetw*0.5 );
  text(page_labels[9], gridCols[20]+spaceBetw/3 , gridRow[12]-rowBetw*0.5 );
   }
      else 
         if ( elementData.get(idElement).dMode.getValue()  ==25)
   { text(mouse_labels[7], gridCols[20]+spaceBetw/3  , gridRow[6]-rowBetw*0.5 );
  text(mouse_labels[6], gridCols[20]+spaceBetw/3  , gridRow[8]-rowBetw*0.5 );
  text(mouse_labels[8], gridCols[20]+spaceBetw/3 , gridRow[10]-rowBetw*0.5 );
  text(mouse_labels[9], gridCols[20]+spaceBetw/3 , gridRow[12]-rowBetw*0.5 );
   }
      else
        if ( elementData.get(idElement).dMode.getValue()  ==18)
   { text(spin_labels[7], gridCols[20]+spaceBetw/3  , gridRow[6]-rowBetw*0.5 );
  text(distance_labels[6], gridCols[20]+spaceBetw/3  , gridRow[8]-rowBetw*0.5 );
  text(distance_labels[8], gridCols[20]+spaceBetw/3 , gridRow[10]-rowBetw*0.5 );
  text(distance_labels[9], gridCols[20]+spaceBetw/3 , gridRow[12]-rowBetw*0.5 );
   }
     else 
     if ( elementData.get(idElement).dMode.getValue()  ==19)
   { text(spin_labels[7], gridCols[20]+spaceBetw/3  , gridRow[6]-rowBetw*0.5 );
  text(spin_labels[6], gridCols[20]+spaceBetw/3  , gridRow[8]-rowBetw*0.5 );
 // text(spin_labels[8], gridCols[20]+spaceBetw/3 , gridRow[10]-rowBetw*0.5 );
 // text(spin_labels[9], gridCols[20]+spaceBetw/3 , gridRow[12]-rowBetw*0.5 );
   }
   else
      if ( elementData.get(idElement).dMode.getValue() < 11)
   {
  text(button_labels[7], gridCols[20]+spaceBetw/3 , gridRow[6]-rowBetw*0.5  );
  text(button_labels[6], gridCols[20]+spaceBetw/3
  , gridRow[8]-rowBetw*0.5
  );
  text(button_labels[8], gridCols[20]+spaceBetw/3
  , gridRow[10]-rowBetw*0.5
  );
  text(button_labels[9], gridCols[20]+spaceBetw/3
  , gridRow[12]-rowBetw*0.5
  );
   } 
      else
      if ( elementData.get(idElement).dMode.getValue() < 16)
   {
  text(pot_labels[7], gridCols[20]+spaceBetw/3 , gridRow[6]-rowBetw*0.5  );
  text(pot_labels[6], gridCols[20]+spaceBetw/3
  , gridRow[8]-rowBetw*0.5
  );
  text(pot_labels[8], gridCols[20]+spaceBetw/3
  , gridRow[10]-rowBetw*0.5
  );
  text(pot_labels[9], gridCols[20]+spaceBetw/3
  , gridRow[12]-rowBetw*0.5
  );
   } 
   
    else
      if ( elementData.get(idElement).dMode.getValue() == 16)
   {
  
   } 
  
  
     else //joystick_labels
      if ( elementData.get(idElement).dMode.getValue()  ==20)
   { text(PADS_labels[7], gridCols[20]+spaceBetw/3  , gridRow[6]-rowBetw*0.5  );
  text(PADS_labels[6], gridCols[20]+spaceBetw/3  , gridRow[8]-rowBetw*0.5  );
  text(PADS_labels[8], gridCols[20]+spaceBetw/3 , gridRow[10]-rowBetw*0.5  );
  text(PADS_labels[9], gridCols[20]+spaceBetw/3 , gridRow[12]-rowBetw*0.5  );
   }
      else
   if ( elementData.get(idElement).dMode.getValue()  <23)
   { text(spin_labels[7], gridCols[20]+spaceBetw/3  , gridRow[6]-rowBetw*0.5  );
  text(spin_labels[6], gridCols[20]+spaceBetw/3  , gridRow[8]-rowBetw*0.5  );
  text(spin_labels[8], gridCols[20]+spaceBetw/3 , gridRow[10]-rowBetw*0.5  );
  text(spin_labels[9], gridCols[20]+spaceBetw/3 , gridRow[12]-rowBetw*0.5  );
   }
      else
   if ( elementData.get(idElement).dMode.getValue()  <25)
   { text(touch_labels[7], gridCols[20]+spaceBetw/3  , gridRow[6]-rowBetw*0.5 );
  text(touch_labels[6], gridCols[20]+spaceBetw/3  , gridRow[8]-rowBetw*0.5 );
  text(touch_labels[8], gridCols[20]+spaceBetw/3 , gridRow[10]-rowBetw*0.5 );
  text(touch_labels[9], gridCols[20]+spaceBetw/3 , gridRow[12]-rowBetw*0.5 );
   }
     else
   if ( elementData.get(idElement).dMode.getValue()  == 26)
   { text(GENERAL_labels[7], gridCols[20]+spaceBetw/3  , gridRow[6]-rowBetw*0.5 );
  text(GENERAL_labels[6], gridCols[20]+spaceBetw/3  , gridRow[8]-rowBetw*0.5 );
  text(GENERAL_labels[8], gridCols[20]+spaceBetw/3 , gridRow[10]-rowBetw*0.5 );
  text(GENERAL_labels[9], gridCols[20]+spaceBetw/3 , gridRow[12]-rowBetw*0.5 );
   }
      else


   if ( elementData.get(idElement).dMode.getValue()  ==18)
   { text(spin_labels[7], gridCols[20]+spaceBetw/3  , gridRow[6]-rowBetw*0.5 );
  text(distance_labels[6], gridCols[20]+spaceBetw/3  , gridRow[8]-rowBetw*0.5 );
  text(distance_labels[8], gridCols[20]+spaceBetw/3 , gridRow[10]-rowBetw*0.5 );
  text(distance_labels[9], gridCols[20]+spaceBetw/3 , gridRow[12]-rowBetw*0.5 );
   }
 
}
   
}

void hintbox () {
  hints2 = hints.addTextarea("hints")
         .setPosition((int) gridCols[1],(int)gridRow[2]+rowBetw)
       .setSize((int) gridCols[15]+Betw2,(int) gridRow[5])
   
  
      .setFont(createFont("typeO.TTF", (Betw2/1.9)))
   .setId(2011)
  .setColor(color((color_R)*1.6,(color_G)*1.6,(color_B)*1.6))
                  .setColorBackground(color((color_R)*1.1,(color_G)*1.1,(color_B)*1.1))
               // .setColorBackground(color(100,100,500))
                //  .setColorForeground(color(255,100))   
                  
          
                  .setText("."
                    );
}




void midiMonitor() {
  // background
  //noStroke();
 
 // strokeWeight(3);
  stroke(color((color_R)*1.5,(color_G)*1.5,(color_B)*1.5));  
  noFill();
  rect(gridCols[17],  gridRow[18]+rowBetw, gridCols[7], gridRow[6],7); //red out
  //noStroke();
  noFill();
  strokeWeight(3);
  stroke(color((color_R)*1.28,(color_G)*1.28,(color_B)*1.28));  
  //rect((int)gridCols[18]+1, (int) gridCols[11]+25, (int)gridCols[5]-2, spaceBetw*1.8-2);// red in

  fill(0, 255, 0);
  textSize(Betw2/1.7);
//  if (channel_filter ==0) 
  {
  //text(""+scala1+"_"+scala2+"_"+scal1+"-"+scal2, gridCols[17]+(Betw2*0.4), (int) gridRow[17]+rowBetw*2.4);
  
  text("Channel  : "+ Channel, gridCols[17]+(Betw2*0.4), (int) gridRow[19]+rowBetw*2.4);
  text("Type         : "+ optCC, gridCols[17]+(Betw2*0.4), (int) gridRow[20]+rowBetw*2.4);
  text("Data b.1    : "+Note, gridCols[17]+(Betw2*0.4), (int) gridRow[21]+rowBetw*2.4);
  text("Data b.2    : "+Intensity, gridCols[17]+(Betw2*0.4), (int) gridRow[22]+rowBetw*2.4);
   fill(255, 150, 0);
   text("MOD/KEYS  : "+keyCode_+"/"+Key_, gridCols[17]+(Betw2*0.4), (int) gridRow[23]+rowBetw*2.4);
    
     
     
   //  fill(color(color_R*1.5,color_G*1.5,color_B*1.5));
     fill(color((color_R+color_G+color_B)/3 ));
  //  text("FILTER", gridCols[19]+(Betw2), (int) gridRow[18]+rowBetw*2.4);
      text("MONITOR", gridCols[17]+(Betw2*0.4)
  , (int) gridRow[18]+rowBetw*2.4);
  
  //fill(color(color_R*1.5,color_G*1.5,color_B*1.5));
  
  //text("test:"+ Intensity, gridCols[20]+Betw2, (int) gridRow[23]+rowBetw);
   text(monitor_flux6[0]+",  "+monitor_flux6[1]+",  "+monitor_flux6[2],  gridCols[21]+Betw2*1.4, (int) gridRow[23]+rowBetw*2.4);
   text(monitor_flux5[0]+",  "+monitor_flux5[1]+",  "+monitor_flux5[2],  gridCols[21]+Betw2*1.4, (int) gridRow[22]+rowBetw*2.4);
   text(monitor_flux4[0]+",  "+monitor_flux4[1]+",  "+monitor_flux4[2],  gridCols[21]+Betw2*1.4, (int) gridRow[21]+rowBetw*2.4);
   text(monitor_flux3[0]+",  "+monitor_flux3[1]+",  "+monitor_flux3[2],  gridCols[21]+Betw2*1.4, (int) gridRow[20]+rowBetw*2.4);
   text(monitor_flux2[0]+",  "+monitor_flux2[1]+",  "+monitor_flux2[2],  gridCols[21]+Betw2*1.4, (int) gridRow[19]+rowBetw*2.4);
   text(monitor_flux1[0]+",  "+monitor_flux1[1]+",  "+monitor_flux1[2],  gridCols[21]+Betw2*1.4, (int) gridRow[18]+rowBetw*2.4);
  }
   
 /*
   if (write_mon== true) {
    // if ( buffer_status == 1) 
    { monitor_flux1 [0]  = monitor_flux2 [0];   monitor_flux1 [1]  = monitor_flux2 [1];   monitor_flux1 [2]  = monitor_flux2 [2];
      monitor_flux2 [0]  = monitor_flux3 [0];   monitor_flux2 [1]  = monitor_flux3 [1];   monitor_flux2 [2]  = monitor_flux3 [2];
      monitor_flux3 [0]  = monitor_flux4 [0];   monitor_flux3 [1]  = monitor_flux4 [1];   monitor_flux3 [2]  = monitor_flux4 [2];
      monitor_flux4 [0]  = monitor_flux5 [0];   monitor_flux4 [1]  = monitor_flux5 [1];   monitor_flux4 [2]  = monitor_flux5 [2];
      monitor_flux5 [0]  = buffer_raw1;   monitor_flux5 [1]  = buffer_value;   monitor_flux5 [2]  = buffer_intensity;
      monitor_flux6 [0] = raw1; monitor_flux6 [1] = Note ; monitor_flux6 [2] = Intensity;}
    //  else  {}
   

   write_mon = false;
 //  buffer_status = 0;
   }
   */
   refresh_monitor();
   
     if (filter_channel.getValue() == 0) {  filter_channel.setValueLabel("off");}
    if (filter_value.getValue() == -1) {  filter_value.setValueLabel("off");}

}


void refresh_monitor ()
 {
    if (write_mon== true) {
    // if ( buffer_status == 1) 
    { monitor_flux1 [0]  = monitor_flux2 [0];   monitor_flux1 [1]  = monitor_flux2 [1];   monitor_flux1 [2]  = monitor_flux2 [2];
      monitor_flux2 [0]  = monitor_flux3 [0];   monitor_flux2 [1]  = monitor_flux3 [1];   monitor_flux2 [2]  = monitor_flux3 [2];
      monitor_flux3 [0]  = monitor_flux4 [0];   monitor_flux3 [1]  = monitor_flux4 [1];   monitor_flux3 [2]  = monitor_flux4 [2];
      monitor_flux4 [0]  = monitor_flux5 [0];   monitor_flux4 [1]  = monitor_flux5 [1];   monitor_flux4 [2]  = monitor_flux5 [2];
      monitor_flux5 [0]  = buffer_raw1;   monitor_flux5 [1]  = buffer_value;   monitor_flux5 [2]  = buffer_intensity;
      monitor_flux6 [0] = raw1; monitor_flux6 [1] = Note ; monitor_flux6 [2] = Intensity;}
    //  else  {}
   

   write_mon = false;
 //  buffer_status = 0;
   }
   }

void hide_all () {
 for (int del=1; del<61; del++)
   // if ( del< elementData.size()) 
   {
   //  if (elementData.get(del-1).shape != 1) 
     {
   del_ = (Knob)cp5.get(Integer.toString(del));
  //  elementData.get(del-1).hide = 1;
    del_.hide();  
     }
  }
  
  
}
