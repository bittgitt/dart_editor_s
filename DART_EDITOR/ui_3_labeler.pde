void labeler () 

{
  
    if (full_screen == false)    {   // modalità FULL disattivata
                                     //  fa funzionare l'editor come un controller su schermo, no midimonitor no settingsbox
      
       if (edit_mode == false){  
           labell.show();
           label_hint.show();
           mp.show(); 
  }
      
     

 if (dMode.getValue() == 0) { 
           nT.setLabel(PADS_labels[1]);
             nT.setLabel(PADS_labels[1]); 
          min.setLabel(PADS_labels[2]); 
           max.setLabel(PADS_labels[3]); 
            midiC.setLabel(PADS_labels[4]); 
             dmx.setLabel(PADS_labels[5]);  
             led.setLabel(PADS_labels[12]); 
                  dKeys.setItems(toggleList);
                //     dModif.setVisible(false);
                nT.hide();
           //     nT.setVisible(false);
                
                dModif.lock(); 
                dModif.hide(); 
            //    dModif.setVisible(false) ; 
                
                key_numeric.hide();
               
                
                dKeys.hide();
              //  dKeys.setVisible(false) ; 
                
                    min.hide();
                 //   min.setVisible(false) ; 
                    
                     min.setRange(0,127);
            max.hide(); 
          //  max.setVisible(false) ; 
            
             led.hide(); 
           //  led.setVisible(false) ; 
             led.lock();
             
                          dmx.hide(); 
                     //     dmx.setVisible(false) ; 
                          
            midiC.hide(); 
          //  midiC.setVisible(false) ; 
            
            midiTypeOpt.hide();
         //   midiTypeOpt.setVisible(false) ; 
            
            scale1.hide(); show_piano =0;
                dMode.show();
             //   dMode.setVisible(false) ; 
                
                Matrix_button.hide();
                 }
                 
                   
                 
 else if (dMode.getValue() <11 ) {                  // buttons -toggle groups
         nT.setLabel(button_labels[1]); 
          min.setLabel(button_labels[2]); 
           max.setLabel(button_labels[3]); 
            midiC.setLabel(button_labels[4]); 
             dmx.setLabel(button_labels[5]); 
             led.setLabel(button_labels[12]); 
       dKeys.setItems(keyboardList);
      //  dModif.setVisible(true); // dModif
        nT.show();
        nT.setVisible(true);
      
     //  dModif.setVisible(true);
      
      dModif.setItems(modifiersList);
      dModif.unlock(); 
       dModif.show(); 
        dModif.setVisible(true);
        dKeys.show();
         dKeys.setVisible(true);
            key_numeric.hide();
          min.show();
           min.setVisible(true);
          min.setRange(0,127);
            max.show(); 
             max.setVisible(true);
             led.show(); 
             led.setVisible(true);
             
             led.unlock();
              
            
              dmx.show(); 
               dmx.setVisible(true);
            midiC.show(); 
             midiC.setVisible(true);
            midiTypeOpt.show();
             midiTypeOpt.setVisible(true);
            scale1.hide(); show_piano =0;
                dMode.show(); 
          
                 Matrix_button.hide();
         }
         
else if (dMode.getValue() <16 ) {                          // pot e hypercurve
         nT.setLabel(pot_labels[1]); 
          min.setLabel(pot_labels[2]); 
           max.setLabel(pot_labels[3]); 
            midiC.setLabel(pot_labels[4]); 
             dmx.setLabel(pot_labels[5]); 
              led.setLabel(pot_labels[12]); 
       dKeys.setItems(keyboardList);
      //  dModif.setVisible(true); // dModif
        nT.show();
      //  nT.setVisible(true);

      dModif.setItems(keyboardList);
             dModif.show(); 
     //  dModif.setVisible(true);
      dModif.unlock(); 
      
      
        dKeys.show();
            key_numeric.hide();
          min.show(); 
          min.setRange(0,127);
            max.show(); 
             led.show(); 
             led.unlock();
             led.setVisible(true);
            
              dmx.show(); 
            midiC.show(); 
            midiTypeOpt.show();
            scale1.hide(); show_piano =0;
                dMode.show();     
              Matrix_button.hide();
         }
         
else if (dMode.getValue() == 16) {                               // seq-button
           nT.setLabel(SEQ_bt_labels[1]);
             nT.setLabel(SEQ_bt_labels[1]); 
          min.setLabel(SEQ_bt_labels[2]); 
           max.setLabel(SEQ_bt_labels[3]); 
            midiC.setLabel(SEQ_bt_labels[4]); 
             dmx.setLabel(SEQ_bt_labels[5]);   
               led.setLabel(SEQ_bt_labels[12]); 
                  dKeys.setItems(toggleList);
                //     dModif.setVisible(false);
                  nT.show();
                dModif.hide(); 
                dKeys.hide();
                    key_numeric.hide();
                    min.show(); 
                     min.setRange(0,127);
            max.show(); 
            led.show(); 
             led.setRange(0,127);
             led.unlock();
             //  led.setVisible(true);
             
                          dmx.show(); 
            midiC.show(); 
            midiTypeOpt.show();
            scale1.hide(); show_piano =0;
                dMode.show();
               Matrix_button.hide();
                 }
         
else if (dMode.getValue() == 17) {                               // page
           nT.setLabel(page_labels[1]);
             nT.setLabel(page_labels[1]); 
          min.setLabel(page_labels[2]); 
           max.setLabel(page_labels[3]); 
            midiC.setLabel(page_labels[4]); 
             dmx.setLabel(page_labels[5]);   
               led.setLabel(page_labels[12]); 
                  dKeys.setItems(toggleList);
                //     dModif.setVisible(false);
                  nT.show();
                dModif.hide(); 
                dKeys.hide();
                    key_numeric.hide();
                    min.show(); 
                     min.setRange(0,127);
            max.show(); 
            led.show(); 
             led.unlock();
               led.setVisible(true);
             
                          dmx.show(); 
            midiC.show(); 
            midiTypeOpt.show();
            scale1.hide(); show_piano =0;
                dMode.show();
               Matrix_button.hide();
                 }
                 
                 
else if (dMode.getValue() == 18) {                               // distance
           nT.setLabel(distance_labels[1]);
             nT.setLabel(distance_labels[1]); 
          min.setLabel(distance_labels[2]); 
           max.setLabel(distance_labels[3]); 
            midiC.setLabel(distance_labels[4]); 
             dmx.setLabel(distance_labels[5]);   
               led.setLabel(distance_labels[12]); 
                  dKeys.setItems(keylist_touch);
                 //    dModif.setVisible(false);
                   nT.show();
                 dModif.hide(); 
                 dKeys.show();
                     key_numeric.hide();
                     min.show(); 
                      min.setRange(0,127);
            max.show(); 
            led.hide(); 
             led.lock();
              led.setVisible(true);
                          dmx.show(); 
            midiC.show();
            midiTypeOpt.show();
         scale1.hide(); show_piano =0; //scale1.bringToFront();
          dMode.show();
        Matrix_button.hide();
                 }
                 
                  else if (dMode.getValue() == 19) {       // encoder generico
           nT.setLabel(spin_labels[1]);
             nT.setLabel(spin_labels[1]); 
          min.setLabel(spin_labels[2]); 
           max.setLabel(spin_labels[3]); 
            midiC.setLabel(spin_labels[4]); 
             dmx.setLabel(spin_labels[5]);   
                led.setLabel(spin_labels[12]);   
             dKeys.setItems(toggleList_spinner_TOUCHSTOP);
         //     dModif.setVisible(false);
           nT.show();
           nT.setVisible(true);
           
          dModif.hide(); 
          dModif.setVisible(false);
          
          dKeys.hide();
          //dKeys.setVisible(false);
          
              key_numeric.hide();
             // key_numeric.setVisible(false);
              
              min.show(); 
             // min.setVisible(true);
              
              min.setRange(-32,32);
              
            max.hide(); 
            //max.setVisible(false);
            
            led.hide(); 
            //led.setVisible(false);
          //   led.setVisible(true);
             led.lock();
                          dmx.show(); 
            midiC.show(); 
            midiTypeOpt.show();
              
          set_scale_view();
              dMode.show();
         
          Matrix_button.hide();
                 }
                 
else if (dMode.getValue() == 20) {            /// pads
           nT.setLabel(PADS_labels[1]);
             nT.setLabel(PADS_labels[1]); 
          min.setLabel(PADS_labels[2]); 
           max.setLabel(PADS_labels[3]); 
            midiC.setLabel(PADS_labels[4]); 
             dmx.setLabel(PADS_labels[5]); 
             led.setLabel(PADS_labels[12]);  
                  dKeys.setItems(toggleList);
                //     dModif.setVisible(false);
                  nT.show();
                dModif.hide(); 
                dKeys.hide();
                    key_numeric.hide();
                    min.hide(); 
                     min.setRange(0,127);
            max.hide(); 
            led.hide(); 
             led.lock();
                          dmx.hide(); 
            midiC.show(); 
            midiTypeOpt.show();
         scale1.hide(); show_piano =0;
             dMode.show();
           Matrix_button.hide();
                 }
                 
         
 else if (dMode.getValue() == 21 || dMode.getValue() == 22) {                   // spinner 1 o 2
           nT.setLabel(spin_labels[1]);
             nT.setLabel(spin_labels[1]); 
          min.setLabel(spin_labels[2]); 
           max.setLabel(spin_labels[3]); 
            midiC.setLabel(spin_labels[4]); 
             dmx.setLabel(spin_labels[5]);   
                led.setLabel(spin_labels[12]);   
             dKeys.setItems(toggleList_spinner_TOUCHSTOP);
         //     dModif.setVisible(false);
           nT.show();
          dModif.hide(); 
          dKeys.show();
              key_numeric.hide();
              min.show(); 
              min.setRange(-32,32);
            max.hide(); 
            led.hide(); 
             led.lock();
                          dmx.show(); 
            midiC.show(); 
            midiTypeOpt.show();
              
          set_scale_view();
              dMode.show();
        Matrix_button.hide();
          
                 }
                
else if (dMode.getValue() == 23 || dMode.getValue() == 24) {            // touh sensors
           nT.setLabel(touch_labels[1]);
             nT.setLabel(touch_labels[1]); 
          min.setLabel(touch_labels[2]); 
           max.setLabel(touch_labels[3]); 
            midiC.setLabel(touch_labels[4]); 
             dmx.setLabel(touch_labels[5]);     
               led.setLabel(touch_labels[12]);  
                  dKeys.setItems(toggleList_spinner); //toggleList_spinner
                 //    dModif.setVisible(false);
                   nT.show();
                 dModif.hide(); 
                 dKeys.show();
                     key_numeric.hide();
                     min.show(); 
                   
                 //    float store = setExcursionControllMin; 
                 //  if (dMode.getValue() == 23)  println("store ="+ store);
                      min.setRange(0,127);
                    //  min.setValue(store);
               //     setExcursionControllMin = int(store);
                //    min.setValue(setExcursionControllMin);
                    
            max.show(); 
            led.show(); 
             led.setVisible(true);
             led.unlock();
                          dmx.show(); 
            midiC.show(); 
            midiTypeOpt.show();
            scale1.hide(); show_piano =0;
                dMode.show();
        Matrix_button.hide();
                 }
               

 
                 
                 
 
                 
 else if (dMode.getValue() == 25) {                    // mouse
           nT.setLabel(mouse_labels[1]);
             nT.setLabel(mouse_labels[1]); 
          min.setLabel(mouse_labels[2]); 
           max.setLabel(mouse_labels[3]); 
            midiC.setLabel(mouse_labels[4]); 
             dmx.setLabel(mouse_labels[5]);     
                 led.setLabel(mouse_labels[12]); 
                  dKeys.setItems(toggleList);
                 //    dModif.setVisible(false);
                   nT.show();
                 dModif.hide(); 
                 dKeys.hide();
                     key_numeric.hide();
                     min.show(); 
                      min.setRange(0,127);
            max.show(); 
            led.show();
             led.setVisible(true);
            led.unlock();
                          dmx.show(); 
            midiC.hide(); 
            midiTypeOpt.hide();
            scale1.hide(); show_piano =0;
                dMode.show();
               Matrix_button.hide();
         
                 }
                 
else if (dMode.getValue() == 26) {  // 
           nT.setLabel(GENERAL_labels[1]);
             nT.setLabel(GENERAL_labels[1]); 
          min.setLabel(GENERAL_labels[2]); 
           max.setLabel(GENERAL_labels[3]); 
            midiC.setLabel(GENERAL_labels[4]); 
             dmx.setLabel(GENERAL_labels[5]);    
               led.setLabel(GENERAL_labels[12]); 
                //   dKeys.setItems(toggleList);
                   dKeys.setItems(keylist_general);
                     //    dKeys.setItems(keyboardList);
                 //    dModif.setVisible(false);
                   nT.show();
                 dModif.hide(); 
                 dKeys.show();
                     key_numeric.hide();
                     min.show(); 
                      min.setRange(0,127);
            max.show(); 
            led.show(); 
            led.setRange(0,127);
             led.unlock();
                          dmx.show(); 
            midiC.hide(); 
            midiTypeOpt.hide();
            scale1.hide(); show_piano =0;
              dMode.show();
        Matrix_button.hide();
                 }
                 
               else if (dMode.getValue() <27 ) {                  // buttons -toggle groups
         nT.setLabel(button_labels[1]); 
          min.setLabel(button_labels[2]); 
           max.setLabel(button_labels[3]); 
            midiC.setLabel(button_labels[4]); 
             dmx.setLabel(button_labels[5]); 
             led.setLabel(button_labels[12]); 
       dKeys.setItems(keyboardList);
      //  dModif.setVisible(true); // dModif
        nT.show();
        nT.setVisible(true);
      
     //  dModif.setVisible(true);
      
      dModif.setItems(modifiersList);
      dModif.unlock(); 
       dModif.show(); 
        dModif.setVisible(true);
        dKeys.show();
         dKeys.setVisible(true);
            key_numeric.hide();
          min.show();
           min.setVisible(true);
          min.setRange(0,127);
            max.show(); 
             max.setVisible(true);
             led.show(); 
             led.setVisible(true);
             
             led.unlock();
              
            
              dmx.show(); 
               dmx.setVisible(true);
            midiC.show(); 
             midiC.setVisible(true);
            midiTypeOpt.show();
             midiTypeOpt.setVisible(true);
            scale1.hide(); show_piano =0;
                dMode.show(); 
          
                 Matrix_button.hide();
         }
                 
 if (dMode.getValue() == 28) {      // sprite
       
            nT.hide();
            max.hide(); 
            dModif.hide();          
            dKeys.hide();
                key_numeric.hide();
            min.hide();
            led.hide(); 
            dmx.hide(); 
            midiC.hide(); 
            midiTypeOpt.hide();
            scale1.hide(); show_piano =0;
            dMode.show();
            Matrix_button.show();
            set_matrix_view();
            Matrix_button.bringToFront() ;
                 }
                 
                 else if (dMode.getValue() == 29) {  // 
           nT.setLabel(reset_labels[1]);
             nT.setLabel(reset_labels[1]); 
          min.setLabel(reset_labels[2]); 
           max.setLabel(reset_labels[3]); 
            midiC.setLabel(reset_labels[4]); 
             dmx.setLabel(reset_labels[5]);    
               led.setLabel(reset_labels[12]); 
                //   dKeys.setItems(toggleList);
                   dKeys.setItems(keylist_general);
                     //    dKeys.setItems(keyboardList);
                 //    dModif.setVisible(false);
                   nT.hide();
                 dModif.hide(); 
                 dKeys.hide();
                     key_numeric.hide();
                     min.show(); 
                      min.setRange(0,127);
            max.show(); 
            led.hide(); 
             led.lock();
                          dmx.hide(); 
            midiC.hide(); 
            midiTypeOpt.hide();
            scale1.hide(); show_piano =0;
              dMode.show();
        Matrix_button.hide();
                 }
                 
                          else if (dMode.getValue() == 30) {  //    shifter
           nT.setLabel(shifter_labels[1]);
             nT.setLabel(shifter_labels[1]); 
          min.setLabel(shifter_labels[2]); 
           max.setLabel(shifter_labels[3]); 
            midiC.setLabel(shifter_labels[4]); 
             dmx.setLabel(shifter_labels[5]);    
               led.setLabel(shifter_labels[11]); 
                //   dKeys.setItems(toggleList);
                   dKeys.setItems(keylist_general);
                     //    dKeys.setItems(keyboardList);
                 //    dModif.setVisible(false);
                   nT.hide();
                 dModif.hide(); 
                 dKeys.hide();
                     key_numeric.hide();
                     min.hide(); 
                      min.setRange(0,127);
            max.hide(); 
            led.show(); 
            led.setVisible(true);
             led.unlock();
                          dmx.hide(); 
            midiC.hide(); 
            midiTypeOpt.hide();
            scale1.hide(); show_piano =0;
              dMode.show();
        Matrix_button.hide();
                 }
                 
                 
                 
                 if (dMode.getValue() > 30 && dMode.getValue() < 35 )
                 {                                                          // USER
        nT.setLabel(user_labels[1]);
             nT.setLabel(user_labels[1]); 
          min.setLabel(user_labels[2]); 
           max.setLabel(user_labels[3]); 
            midiC.setLabel(user_labels[4]); 
             dmx.setLabel(user_labels[5]);    
               led.setLabel(user_labels[12]); 
                //   dKeys.setItems(toggleList);
                   dKeys.setItems(keylist_general);
                     //    dKeys.setItems(keyboardList);
                 //    dModif.setVisible(false);
                   nT.show();
                 dModif.hide(); 
                 dKeys.hide();
                     key_numeric.show();
                     min.show(); 
                      min.setRange(0,127);
            max.show(); 
            led.show(); 
             led.unlock();
                          dmx.show(); 
            midiC.show(); 
            midiTypeOpt.show();
            scale1.hide(); show_piano =0;
              dMode.show();
        Matrix_button.hide();
           
           
                 }
  


}

else {
    labell.hide();
      label_hint.hide();
      mp.hide();
      
      
      
                 nT.hide();
                 dModif.hide(); 
                 dKeys.hide();
                     key_numeric.hide();
                 min.hide(); 
                
            max.hide(); 
            led.hide(); 
             led.lock();
            dmx.hide(); 
            midiC.hide(); 
            midiTypeOpt.hide();
            scale1.hide(); show_piano =0;
            dMode.hide();
            Matrix_button.hide();
}
 }
 

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  
   
  
  void sendMidiMessageON() { // 
  
    if (infoGraph < 65)
    
    if (page_selected == false && elementData.get(idElement).toggleMode <11  // tutti i tipi di button
    && elementData.get(idElement).toggleMode >0) 
  {  
    
    
    if ( elementData.get(idElement).shape != 11) // 11 sarebbe il tondo senza arco - in questo caso è un pulsante , sempre a 127
    myBus.sendMessage(144+(elementData.get(idElement).indexMidiType)*16+elementData.get(idElement).midiChannel-1, 
  elementData.get(idElement).note[0], 
   int(cp5.get(Integer.toString(idElement+1)).getValue()) 
 // 127
  );
  else
  myBus.sendMessage(144+(elementData.get(idElement).indexMidiType)*16+elementData.get(idElement).midiChannel-1, 
  elementData.get(idElement).note[0], 
  127
  );
 
  
  //println(elementData.get(idElement).shape);
  
 
       wave.setFrequency( frequenza2(elementData.get(idElement).note[0]) );
       if (elementData.get(idElement).note[0] > soglia_wave2) 
       wave2.setFrequency( frequenza2(elementData.get(idElement).note[0]-detune) );
       else
       wave2.setFrequency( frequenza2(elementData.get(idElement).note[0]+detune) );
       
       if (soundstatus == false)  {
       wave.setAmplitude( 0.5 ); 
      // wave2.setAmplitude( 0.5 );
       wave2.setAmplitude(calc_freq_f2( elementData.get(idElement).note[0])  );
      
   
 }
       if (Monitor_OUT_button.isOn() )
       {
  buffer_value = Note; buffer_intensity = Intensity; buffer_raw1 = raw1;   /// sezione monitoraggio
  Channel = elementData.get(idElement).midiChannel;
  Note = elementData.get(idElement).note[0];
  optCC= typeMidiList[(indexMidiType)];
  
   if ( elementData.get(idElement).shape != 11) 
  Intensity = int(cp5.get(Integer.toString(idElement+1)).getValue());
  else Intensity = 127;
  
   current_db2 = Intensity;
  raw1 = indexMidiType*16+144+Channel-1;// (int)(message.getStatus() & 0xFF); 
  write_mon = true;}
  
  MIDI_OUT_LED = true;
  
}
    
    if (page_selected == true && elementData.get(idElement).toggleMode_2nd <11 && elementData.get(idElement).toggleMode_2nd >0) 
   { myBus.sendMessage(144+(elementData.get(idElement).indexMidiType_2nd)*16+elementData.get(idElement).midiChannel_2nd-1, 
   elementData.get(idElement).note[1], 
   int(cp5.get(Integer.toString(idElement+1)).getValue())
   );
      wave.setFrequency( frequenza2(elementData.get(idElement).note[0]) );
      if (elementData.get(idElement).note[0] > soglia_wave2 )
      wave2.setFrequency( frequenza2(elementData.get(idElement).note[0]-detune) );
      else
      wave2.setFrequency( frequenza2(elementData.get(idElement).note[0]+detune) );
      
      
      
      if (soundstatus == false)  {
      wave.setAmplitude( 0.5 );
      wave2.setAmplitude(calc_freq_f2( elementData.get(idElement).note[0] ));
      //wave2.setAmplitude( 0.5 );
  
}
       if (Monitor_OUT_button.isOn() )
       {
  buffer_value = Note; buffer_intensity = Intensity; buffer_raw1 = raw1;   /// sezione monitoraggio
  Channel = elementData.get(idElement).midiChannel_2nd;
  Note = elementData.get(idElement).note[1];
  optCC= typeMidiList[(indexMidiType_2nd)];
  Intensity = int(cp5.get(Integer.toString(idElement+1)).getValue());
   current_db2 = Intensity;
  raw1 = indexMidiType_2nd*16+144+Channel-1;// (int)(message.getStatus() & 0xFF); 
  write_mon = true;
       }
  MIDI_OUT_LED = true;

 }
  }
  
  
  void sendMidiMessageOFF() {
  /*  if ( noteCC == true) {
      myBus.sendMessage(0xB0, midiChannel-1, note, 0);
    } else if ( noteCC == false) {
      myBus.sendMessage(0x90, midiChannel-1, note, 0);
    }*/
    if (infoGraph < 65)
     if (page_selected == false && elementData.get(idElement).toggleMode <11 && elementData.get(idElement).toggleMode >0 && elementData.get(idElement).indexMidiType != 3) 
    { myBus.sendMessage(144+(elementData.get(idElement).indexMidiType)*16+elementData.get(idElement).midiChannel-1, elementData.get(idElement).note[0], 0);
  // saw.amp(0.0);
  wave.setAmplitude( 0.0 );
 // delay(2);
   wave2.setAmplitude( 0.0 );
   if (Monitor_OUT_button.isOn() )
       {
   buffer_value = Note; buffer_intensity = Intensity; buffer_raw1 = raw1;   /// sezione monitoraggio
  Channel = elementData.get(idElement).midiChannel;
  //if ( elementData.get(idElement).indexMidiType == 5) {Note = 64;}  else    
  Note = elementData.get(idElement).note[0];
  
  optCC= typeMidiList[(indexMidiType)];
  Intensity = 0;
   current_db2 = Intensity;
  raw1 = indexMidiType*16+144+Channel-1;// (int)(message.getStatus() & 0xFF); 
  write_mon = true;
  feedback_counter = 0;
       }
  MIDI_OUT_LED = true;
  
}
     
     if (page_selected == true && elementData.get(idElement).toggleMode_2nd <11 && elementData.get(idElement).toggleMode_2nd >0 
     && elementData.get(idElement).indexMidiType_2nd != 3) 
   {  myBus.sendMessage(144+(elementData.get(idElement).indexMidiType_2nd)*16+elementData.get(idElement).midiChannel_2nd-1, elementData.get(idElement).note[1], 0);
   // saw.amp(0.0);
     wave.setAmplitude( 0.0 );
    // delay(2);
      wave2.setAmplitude( 0.0 );
     
      if (Monitor_OUT_button.isOn() )
       {
  buffer_value = Note; buffer_intensity = Intensity; buffer_raw1 = raw1;   /// sezione monitoraggio
  Channel = elementData.get(idElement).midiChannel_2nd;
  
// if ( elementData.get(idElement).indexMidiType_2nd == 5) {Note = 64;} else
 Note = elementData.get(idElement).note[1]; 
  optCC= typeMidiList[(indexMidiType_2nd)];
  Intensity = 0;
   current_db2 = Intensity;
  raw1 = indexMidiType*16+144+Channel-1;// (int)(message.getStatus() & 0xFF); 
  write_mon = true;
  feedback_counter = 0;
       }
  MIDI_OUT_LED = true;
 }
  }
