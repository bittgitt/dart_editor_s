
  //---------------------------------------------------------------------------------------------------------------------
    CallbackListener send_ctrl = new CallbackListener() { // quando regolo un item col mouse, viene mandato fuori il relativo segnale midi e visualizato sul monitor.
  public void controlEvent(CallbackEvent theEvent) {

  
  if (elementData.get(idElement).toggleMode >10  && elementData.get(idElement).toggleMode <16 && page_selected == false)     // se è un pot o un hypercurve
  if ( elementData.get(idElement).indexMidiType == 2 || elementData.get(idElement).indexMidiType == 5)  // se è un CC o un PB
 
  if (MIDI_IN_LED == false) 
  {
 myBus.sendMessage(144+(elementData.get(idElement).indexMidiType)*16+elementData.get(idElement).midiChannel-1, 
 elementData.get(idElement).note[0], 
 int(cp5.get(Integer.toString(idElement+1)).getValue())      
 );
 
 
  wave.setFrequency( frequenza2( int(cp5.get(Integer.toString(idElement+1)).getValue())     ));
  if (int(cp5.get(Integer.toString(idElement+1)).getValue()) > soglia_wave2)
  wave2.setFrequency( frequenza2( int(cp5.get(Integer.toString(idElement+1)).getValue()) -detune   ));
  else
  wave2.setFrequency( frequenza2( int(cp5.get(Integer.toString(idElement+1)).getValue())  +detune   ));
  
 if (soundstatus == false) {
 wave.setAmplitude( 0.5 ); 
 wave2.setAmplitude(calc_freq_f2(  int(cp5.get(Integer.toString(idElement+1)).getValue())  ));
 //wave2.setAmplitude( 0.5 );
 
 }
 feedback_counter = 0;
 if (Monitor_OUT_button.isOn() )
 {
  buffer_value = Note; buffer_intensity = Intensity; buffer_raw1 = raw1;   /// sezione monitoraggio
  Channel = elementData.get(idElement).midiChannel;
  if ( elementData.get(idElement).indexMidiType == 5) {Note = 64;}  else Note = elementData.get(idElement).note[0];
  optCC= typeMidiList[(elementData.get(idElement).indexMidiType)];
  Intensity = int(cp5.get(Integer.toString(idElement+1)).getValue());
  current_db2 = Intensity;
  raw1 = elementData.get(idElement).indexMidiType*16+144+Channel-1;// (int)(message.getStatus() & 0xFF); 
  write_mon = true;
  }
  MIDI_OUT_LED = true;
  }
 
  if (
  elementData.get(idElement).toggleMode_2nd >10 && elementData.get(idElement).toggleMode_2nd <16 && page_selected == true )
  if (elementData.get(idElement).indexMidiType_2nd == 2 || elementData.get(idElement).indexMidiType_2nd == 5)
  
  if (MIDI_IN_LED == false) 
  {
 myBus.sendMessage(144+(elementData.get(idElement).indexMidiType_2nd)*16+elementData.get(idElement).midiChannel_2nd-1, 
 elementData.get(idElement).note[1], 
 int(cp5.get(Integer.toString(idElement+1)).getValue())      
 );
  wave.setFrequency( frequenza2( int(cp5.get(Integer.toString(idElement+1)).getValue())     ));
  if (  int(cp5.get(Integer.toString(idElement+1)).getValue()) > soglia_wave2) 
  wave2.setFrequency( frequenza2( int(cp5.get(Integer.toString(idElement+1)).getValue())   -detune  ));
  else
  wave2.setFrequency( frequenza2( int(cp5.get(Integer.toString(idElement+1)).getValue())  +detune   ));
  
  
  
   if (soundstatus == false) { 
     wave.setAmplitude( 0.5 ); 
     wave2.setAmplitude(calc_freq_f2(int(cp5.get(Integer.toString(idElement+1)).getValue())));
 //wave2.setAmplitude( 0.5 );
 
 }
 feedback_counter = 0;
 
 {
  buffer_value = Note; buffer_intensity = Intensity; buffer_raw1 = raw1;   /// sezione monitoraggio
  Channel = elementData.get(idElement).midiChannel_2nd;
   if ( elementData.get(idElement).indexMidiType_2nd == 5) {Note = 64;}  else Note = elementData.get(idElement).note[1];
  optCC= typeMidiList[(elementData.get(idElement).indexMidiType_2nd)];
  Intensity = int(cp5.get(Integer.toString(idElement+1)).getValue());
  current_db2 = Intensity;
  raw1 = elementData.get(idElement).indexMidiType_2nd*16+144+Channel-1;// (int)(message.getStatus() & 0xFF); 
  write_mon = true;
 }
  MIDI_OUT_LED = true;
  }
 
 
  }
};
  
 //-------------------------------------------------------------------------------------------------------------------------------------------
  
  
  
  CallbackListener general_open = new CallbackListener() {
  public void controlEvent(CallbackEvent theEvent) {
   if ( elementData.get(idElement).toggleMode == 26 )
  {general_send = 0;}
  }
};
  
  
  CallbackListener doppioni = new CallbackListener() { // richiamata quando si cambia MODE in un item
  public void controlEvent(CallbackEvent theEvent) {
  
         if (elementData.get(idElement).dMode.getValue() > 16 
             && elementData.get(idElement).dMode.getValue() < 27 // sprite
             ) // tutto tranne pot e buttons
  
{elementData.get(idElement).toggleMode = (int) elementData.get(idElement).dMode.getValue(); 
elementData.get(idElement).toggleMode_2nd = elementData.get(idElement).toggleMode;    
// rendi il MODE uguale nelle due pagine - solo per le modalità speciali - non pot e button
 if (elementData.get(idElement).toggleMode == 17 || elementData.get(idElement).toggleMode == 26)  //se è page o general
    {elementData.get(idElement).plugga_page(); }                                                  // vengono copiate anche i contenuti dei numberbox
};

elementData.get(idElement).togglestate = false; // il pulsante  adesso è pronto per essere premuto e riacceso.
 elementData.get(idElement).setStateButton(false); 

elementData.get(idElement).labeler();  
}
};




  CallbackListener mp_doppioni = new CallbackListener() { // evita doppioni sulla memoryposition e ricolloca i modificatori su numero libero
  public void controlEvent(CallbackEvent theEvent) {

   // elementData.get(idElement).labeler();
   byte risultato = 0;
   byte numero_inutilizzato =0;
   for (int i=0; i<60; i++) {
     //  
    if (elementData.get(idElement).mp.getValue() != 0 && idElement != i
    && elementData.get(idElement).mp.getValue() == elementData.get(i).memoryPosition 
    //&& elementData.get(i).hide == 0 
    )  
  { 
      for (byte numero=1; numero<61; numero++)  // induviduo un numero non usato e lo carico su numero_inutilizzato;
      {
        risultato=0;
      for (byte quale=0; quale<60; quale++)
      {
        if (numero == elementData.get(quale).memoryPosition ) 
      risultato++; }
      if (risultato ==0) { numero_inutilizzato = numero; break;}
      
      }
      
    
   elementData.get(i).memoryPosition =numero_inutilizzato; 
   elementData.get(i).mp.setValue(numero_inutilizzato); 
    
   // elementData.get(i).memoryPosition=8; 
   // elementData.get(i).mp.setValue(8); 
  //  elementData.get(idElement).memoryPosition =0; 
 // elementData.get(idElement).mp.setValue(0); 
  // elementData.get(idElement).mp.setValueLabel("null");
}
//for (int i=0; i<60; i++) {
 
//}

     
      }
    
  }
};
  
  
  
  
CallbackListener toFront = new CallbackListener() {
  public void controlEvent(CallbackEvent theEvent) {
    theEvent.getController().bringToFront();
    ((ScrollableList)theEvent.getController()).open();
  }
};
CallbackListener toFrontBox = new CallbackListener() {
  public void controlEvent(CallbackEvent theEvent) {
    theEvent.getController().bringToFront();
    ((ScrollableList)theEvent.getController()).open();
  }
};
CallbackListener close = new CallbackListener() {
  public void controlEvent(CallbackEvent theEvent) {
    ((ScrollableList)theEvent.getController()).close();
  }
};

CallbackListener closenumberbox = new CallbackListener() {
  public void controlEvent(CallbackEvent theEvent) {
    ((Numberbox)theEvent.getController()).update();
  }
};

CallbackListener set_colore = new CallbackListener() {
  public void controlEvent(CallbackEvent theEvent) {
    (theEvent.getController()).setColorBackground(color(color_R*0.8,color_G*0.8,color_B*0.8));
  }
};

CallbackListener set_colore2 = new CallbackListener() {
  public void controlEvent(CallbackEvent theEvent) {
    (theEvent.getController()).setColorBackground(color(color_R*0.1,color_G*0.1,color_B*0.1));
  }
};

CallbackListener set_colore3 = new CallbackListener() {
  public void controlEvent(CallbackEvent theEvent) {
    (theEvent.getController()).setColorBackground(color(color_R*0.7,color_G*0.7,color_B*0.7));
  }
};

CallbackListener shutdown = new CallbackListener() {
  public void controlEvent(CallbackEvent theEvent) {
  //                 mode,type,ey,hotkey,label
  int[] gruppozzo = {2007,2001,2010,2009,2012,2013};
    int id_attuale = (theEvent.getController()).getId();;
    byte alfa =0;
    
    for (byte i =0; i<6; i++)
    {
    if (gruppozzo[i] == id_attuale) alfa = i;
    }
    
      println(alfa);
      println(id_attuale);
      
      switch (alfa)
      {
      case 0:
         // elementData.get(idElement).dModif.setVisible(false); 
         //  elementData.get(idElement).dModif.hide();
           elementData.get(idElement).midiTypeOpt.setVisible(false); 
           elementData.get(idElement).midiTypeOpt.hide();
            elementData.get(idElement).dKeys.setVisible(false); 
            elementData.get(idElement).dKeys.hide();
                  elementData.get(idElement).dModif.setVisible(false); 
           elementData.get(idElement).dModif.hide();
        break;
      case 1:
      elementData.get(idElement).dKeys.setVisible(false); 
            elementData.get(idElement).dKeys.hide();
              elementData.get(idElement).dModif.setVisible(false); 
           elementData.get(idElement).dModif.hide();
      break;
      case 2:
           elementData.get(idElement).dModif.setVisible(false); 
           elementData.get(idElement).dModif.hide();
           elementData.get(idElement).label_hint.setVisible(false); 
           elementData.get(idElement).label_hint.hide();
           break;
      case 3:   
           elementData.get(idElement).label_hint.setVisible(false); 
           elementData.get(idElement).label_hint.hide();
           elementData.get(idElement).labell.setVisible(false); 
           elementData.get(idElement).labell.hide();
      break;
      }
      
      
 
  }
};

CallbackListener turnon = new CallbackListener() {
  public void controlEvent(CallbackEvent theEvent) {

   elementData.get(idElement).labeler(); 
   
   /*
  int[] gruppozzo = {2007,2001,2010,2009,2012,2013};
    int id_attuale = (theEvent.getController()).getId();;
    byte alfa =0;
    
    for (byte i =0; i<6; i++)
    {
    if (gruppozzo[i] == id_attuale) alfa = i;
    }
    
      println(alfa);
      println(id_attuale);
      
      switch (alfa)
      {
      case 0:
         // elementData.get(idElement).dModif.setVisible(false); 
         //  elementData.get(idElement).dModif.hide();
           elementData.get(idElement).midiTypeOpt.setVisible(true); 
           elementData.get(idElement).midiTypeOpt.show();
            elementData.get(idElement).dKeys.setVisible(true); 
            elementData.get(idElement).dKeys.show();
        break;
      case 1:
      elementData.get(idElement).dKeys.setVisible(false); 
            elementData.get(idElement).dKeys.show();
              elementData.get(idElement).dModif.setVisible(true); 
           elementData.get(idElement).dModif.show();
      break;
      case 2:
           elementData.get(idElement).dModif.setVisible(true); 
           elementData.get(idElement).dModif.show();
               elementData.get(idElement).label_hint.setVisible(true); 
           elementData.get(idElement).label_hint.show();
      break;
            case 3:   
           elementData.get(idElement).label_hint.setVisible(true); 
           elementData.get(idElement).label_hint.show();
           elementData.get(idElement).labell.setVisible(true); 
           elementData.get(idElement).labell.show();
      break;
           
      }
      
        */
   
  }
};

/*
CallbackListener shutdown2 = new CallbackListener() {
  public void controlEvent(CallbackEvent theEvent) {

 
     elementData.get(idElement).dKeys.setVisible(false); 
    elementData.get(idElement).dKeys.hide();
     elementData.get(idElement).dModif.setVisible(false); 
    elementData.get(idElement).dModif.hide();
         

  }
};

CallbackListener turnon2 = new CallbackListener() {
  public void controlEvent(CallbackEvent theEvent) {

     elementData.get(idElement).dKeys.setVisible(true); 
     elementData.get(idElement).dKeys.show();
     
     elementData.get(idElement).dModif.setVisible(true); 
     elementData.get(idElement).dModif.show();
    // dModif
       println("ciaooo");
  }
};

*/

CallbackListener closeBox = new CallbackListener() {
  public void controlEvent(CallbackEvent theEvent) {
    ((ScrollableList)theEvent.getController()).close();
  }
};


CallbackListener open_scale = new CallbackListener() {
  public void controlEvent(CallbackEvent theEvent) {
  //  if (theEvent.getController().getValue() == 2) scale1.show(); else scale1.hide();
 elementData.get(idElement).labeler();
  }
};



CallbackListener leave_item = new CallbackListener() {
  public void controlEvent(CallbackEvent theEvent) {
    if (theEvent.getController().getId() == idElement )
   // println('-');
   // println( theEvent.getController().getId() );
     
     // println(idElement);
     if (elementData.get(idElement).togglestate == false && page_selected == false) 
     {
     elementData.get(idElement).setStateButton(false); 
    // elementData.get(idElement).sendMidiMessageOFF();
    if (elementData.get(idElement).selected == true)
    myBus.sendMessage(144+(elementData.get(idElement).indexMidiType)*16+elementData.get(idElement).midiChannel-1, 
    elementData.get(idElement).note[0],
    0);
    
     wave.setAmplitude( 0.0 );
     wave2.setAmplitude( 0.0 );
     }
     
      if (elementData.get(idElement).togglestate_2nd == false && page_selected == true) 
     {
     elementData.get(idElement).setStateButton(false); 
    if (elementData.get(idElement).selected == true)
    myBus.sendMessage(144+(elementData.get(idElement).indexMidiType_2nd)*16+elementData.get(idElement).midiChannel_2nd-1, 
    elementData.get(idElement).note[1],
    0);
    
     wave.setAmplitude( 0.0 );
     wave2.setAmplitude( 0.0 );
     }
     
  }
};
