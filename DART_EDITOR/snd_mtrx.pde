String Matrix_byte[]  = {"","","","","","","","","",""};

byte order_row[] = {6,1,5,3,7,2,4,0}; // alto basso

//byte order_line[] = {5,6,0,1,3,4,7,2}; // destra sinistra
  byte order_line[] = {5,4,0,3,2,7,6,1}; // destra sinistra



//___________________________________________________________________________________________________________________________________

CallbackListener Matrix2tables = new CallbackListener() {
public void controlEvent(CallbackEvent theEvent) {

 /*
  // versione originale funzionante, non rimappata
  for (byte ii = 0; ii < 8; ii++){ // riga - posizione dall'alto al basso
    Matrix_byte[ii]="";
    
      for (byte iz = 0; iz < 7; iz++) {   // colonna - iz = posizione da destra a sinistra
        if ( Matrix_button.get(iz,ii) == true) 
 Matrix_byte[ii] ="1"+Matrix_byte[ii]; else Matrix_byte[ii]= "0"+Matrix_byte[ii];
      
     }  // fine  for ii
  } // fine for iz
  */
  
  Matrix_byte[8] ="";
  Matrix_byte[9] ="";
    // versione originale funzionante, non rimappata
  for (byte ii = 0; ii < 8; ii++){ // riga - posizione dall'alto al basso
    Matrix_byte[ii]="";
    
      for (byte iz = 0; iz < 7; iz++) {   // colonna - iz = posizione da destra a sinistra
        if ( Matrix_button.get(order_line[iz],ii) == true)  //  order_line[iz] significa che prima rimappo e poi leggo // byte order_line[] = {5,6,0,1,3,4,7,2}; 
 Matrix_byte[ii] ="1"+Matrix_byte[ii]; else Matrix_byte[ii]= "0"+Matrix_byte[ii]; 
     }  // fine  for ii
     
     if (ii < 4)
       {if ( Matrix_button.get(order_line[7],ii) == true)
       Matrix_byte[8] ="1"+Matrix_byte[8]; else Matrix_byte[8]= "0"+Matrix_byte[8];}
       else
       {if ( Matrix_button.get(order_line[7],ii) == true)
       Matrix_byte[9] ="1"+Matrix_byte[9]; else Matrix_byte[9]= "0"+Matrix_byte[9];}
       
  } // fine for iz
 // Matrix_byte[8] =Matrix_byte[8]+"1111";
 // Matrix_byte[9] =Matrix_byte[9]+"1111";
  
  
  if(page_selected == false) {                               // note reminder: arduino non può accettare segnali midi oltre il 127 cioè SETTE bit (non 256 OTTO BIT)
                                                             // da ciò deriva tutto un incasinamento, utilizzo due byte in più (qui sotto userbyte 4 e 5) come memorizzatori di servizio
 elementData.get(idElement).note[0] =                  unbinary(Matrix_byte[0]);     // arduino - valuetable[]
 elementData.get(idElement).setExcursionControllMax =  unbinary(Matrix_byte[1]);     // arduino - maxvalue[]
 elementData.get(idElement).setExcursionControllMin =  unbinary(Matrix_byte[2]);     // arduino - minvalue[]
 elementData.get(idElement).User_byte_1 =              unbinary(Matrix_byte[3]);     // arduino - modetable[]        
 
 elementData.get(idElement).addressDMX =               unbinary(Matrix_byte[4]);     // arduino - dmxtable[]
 elementData.get(idElement).keyBoard =                 unbinary(Matrix_byte[5]);     // arduino - qwertyvalue[]
 elementData.get(idElement).User_byte_2 =              unbinary(Matrix_byte[6]);     // arduino - typetable[]
 elementData.get(idElement).User_byte_3 =              unbinary(Matrix_byte[7]);     // arduino - lightable[]
 
 elementData.get(idElement).User_byte_4 =              unbinary(Matrix_byte[8]);     // arduino - 
 elementData.get(idElement).User_byte_5 =              unbinary(Matrix_byte[9]);     // arduino - 
 
 

 
  }
  else
  {
  elementData.get(idElement).note[1] = unbinary(Matrix_byte[0]);
 elementData.get(idElement).setExcursionControllMax_2nd = unbinary(Matrix_byte[1]);  //
 elementData.get(idElement).setExcursionControllMin_2nd = unbinary(Matrix_byte[2]);  //
 elementData.get(idElement).User_byte_6 = unbinary(Matrix_byte[3]);              //
 
 elementData.get(idElement).addressDMX_2nd = unbinary(Matrix_byte[4]);             //
 elementData.get(idElement).keyBoard_2nd = unbinary(Matrix_byte[5]);
 elementData.get(idElement).User_byte_7 = unbinary(Matrix_byte[6]);                   //
 elementData.get(idElement).User_byte_8 = unbinary(Matrix_byte[7]);
 
 elementData.get(idElement).User_byte_9 = unbinary(Matrix_byte[8]);
  elementData.get(idElement).User_byte_14 = unbinary(Matrix_byte[9]);
 
  }
}
};





//////////////////////////////////////////////////////////////////////////////////////////////////////////////

  void set_matrix_view() // caricare le impostazioni memorizzate sui numberbox nella matrice 8x8 dedicata agli sprites
  {
   String string_matrix;
   String string_matrix_byte9;
   String string_matrix_byte9a;
   char char_matrix;
   byte byte_matrix =0;
   byte byte_matrix_a =0;
   boolean bool_matrix;
   
  //  print(elementData.get(idElement).User_byte_1); 
  //  println(" - "); 
  //  print(" x "); 
   
    if (elementData.get(idElement).dMode.getValue() == 28) { // se l'elemento selezionato è un item SPRITE
   
  
   for (byte iz =0; iz<8; iz++) {
     
   if (page_selected== false) 
           { switch (iz) {    // carico su byte_matrix (temporaneo) il dato della riga che voglio
       case 0 :
       byte_matrix =(byte)elementData.get(idElement).note[0];
       break;
       case 1 :
       byte_matrix =(byte)elementData.get(idElement).setExcursionControllMax;
       break;
       case 2 :
       byte_matrix =(byte)elementData.get(idElement).setExcursionControllMin;
       break;
       case 3 :
       byte_matrix =(byte)elementData.get(idElement).User_byte_1;
       break;
       
       case 4 :
       byte_matrix =(byte)elementData.get(idElement).addressDMX;
       break;
       case 5 :
       byte_matrix =(byte)elementData.get(idElement).keyBoard;
       break;
       case 6 :
       byte_matrix =(byte)elementData.get(idElement).User_byte_2;
       break;
       case 7 :
       byte_matrix =(byte)elementData.get(idElement).User_byte_3;
       break;
                         }
       }
           else                                                    // se siamo sulla seconda pagina
         {
          switch (iz) {  
       case 0 :
       byte_matrix =(byte)elementData.get(idElement).note[1];
       break;
       case 1 :
       byte_matrix =(byte)elementData.get(idElement).setExcursionControllMax_2nd;
       break;
       case 2 :
       byte_matrix =(byte)elementData.get(idElement).setExcursionControllMin_2nd;
       break;
       case 3 :
       byte_matrix =(byte)elementData.get(idElement).User_byte_6;
       break;
       
       case 4 :
       byte_matrix =(byte)elementData.get(idElement).addressDMX_2nd;
       break;
       case 5 :
       byte_matrix =(byte)elementData.get(idElement).keyBoard_2nd;
       break;
       case 6 :
       byte_matrix =(byte)elementData.get(idElement).User_byte_7;
       break;
       case 7 :
       byte_matrix =(byte)elementData.get(idElement).User_byte_8;
       break;
          }
     }
         
         
           
   string_matrix = binary(byte_matrix);  // converto il mio byte in una stringa 
   //string_matrix_byte9 =
   
   for (byte i =0; i<7; i++)     // note reminder : da 0 a 7 sempre per il discorso che io per comunicare via midi devo limitarmi a 7 bit e per arrivare a 8 uso altri due user_byte
                                 // 4 a 5 per la prima pagina - 8 e 9 per la seconda... salvo correzioni
                                 
                                 // mi servono due nyte di servizio perchè mi mancano 8 bit per completare la matrice e non posso scriverne 8, quindi ne scivo 4 in uno e 4 in un altro.
          {  
     
      char_matrix = string_matrix.charAt(7-i);  // 7-i significa che comincio a leggere da 7 verso 0 - leggo un solo bit per volta
     
      if (char_matrix == '0') bool_matrix = false; else bool_matrix = true; // trasformo il risultato in un valore boolean pronto da scrivere 
      
      Matrix_button.set(order_line[i],iz,bool_matrix);  // scrivo sulla matrice e visualizzo. // iz è l'ordine verticale 
                                                        // byte order_line[] = {5,6,0,1,3,4,7,2}; precedentemente il bit 5 lo hai scritto in posizione 0
          
                                            
          }
   }  
   
    if (page_selected== false)  {
    byte_matrix = (byte)elementData.get(idElement).User_byte_4;
    byte_matrix_a =(byte)elementData.get(idElement).User_byte_5;}
    
    else {
    byte_matrix = (byte)elementData.get(idElement).User_byte_9;
     byte_matrix_a =(byte)elementData.get(idElement).User_byte_14;}
    
   string_matrix_byte9 = binary(byte_matrix);  // trasformo i miei byte in stringhe
   string_matrix_byte9a = binary(byte_matrix_a);

  ///  println(" ");
  // print("string_matrix_byte9 - "); print(string_matrix_byte9); print(" - "); println(elementData.get(idElement).User_byte_4);
  //print("string_matrix_byte9a - "); print(string_matrix_byte9a); print(" - "); println(elementData.get(idElement).User_byte_5);
  
   for (byte im =0; im<4; im++)
          {  
          char_matrix = string_matrix_byte9.charAt(7-im);
          if (char_matrix == '0') bool_matrix = false; else bool_matrix = true; 
          Matrix_button.set(order_line[7],im,bool_matrix);
          //  Matrix_button.set(order_line[7],im,true);
           
          }
  
          
          
  
  for (byte it =0; it<4; it++)
          {  
          char_matrix = string_matrix_byte9a.charAt(7-it);
          if (char_matrix == '0') bool_matrix = false; else bool_matrix = true; 
           Matrix_button.set(order_line[7],it+4,bool_matrix);
          }
          
          
          /*
          Matrix_button.set(order_line[7],7,true);
            Matrix_button.set(order_line[7],6,true);
             Matrix_button.set(order_line[7],5,true);
              Matrix_button.set(order_line[7],4,true);
              */
         
   
  }
  }
  
