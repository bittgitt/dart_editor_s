void changeshape(int elemento) {
 
 // if (elementData.get(elemento).shape != 1)
  {
  
  Knob t = (Knob)cp5.get(Integer.toString(elemento+1));

  elementData.get(elemento).shape= mousewheel;  
  
 switch (mousewheel) {
 case 1: 
 t.setView(new Manopolox()); // tondo con arco arancione
 break;
 case 2: 
 t.setView(new Manopolox2()); // tondo con arco bianco
 break;
 case 3:
 t.setView(new SisButton2()); // quadrato con vumeter bianco orizzontale
 break;
 case 4:
 t.setView(new RectButton()); // rettangolo con vumeter bianco orizzontale
 break;
 case 5:
 t.setView(new fader());  // rettangolo verticale con vumeter 
 break;
 case 6:  
 t.setView(new polygon_());   // pentagono con arco arancione 
 break;
 case 7:
 t.setView(new Marrone_polygon()); // pentagono con tondo centrale e arco arancione 
 break;  
 case 8:
 t.setView(new SensorButton()); // quadrato vuoto con vumeter orizzontale
 break;
 case 9:
 t.setView(new fader2_orizz()); // fader con knob che si muove
 break;
 case 10:
 t.setView(new fader3_vert());  // fader verticale con knob che si muove
 break;
 case 11: 
 t.setView(new Grigio());  /// tondo senza arco // con bordo arancione
 break;
 case 12: 
 t.setView(new faderox()); // fader rettangolare con vumeter arancione
 break;

 
 }
 
}

}

//------------------------------------------------------------------------------------------------------------------




class SisButton implements ControllerView<Button> { 
  public void display(PGraphics theApplet, Button theButton) {
    theApplet.pushMatrix();
    theApplet.textSize(Betw2/1.7);
    strokeWeight(3);
    stroke(color_R*1.5,color_G*1.5,color_B*1.5);
 
    fill(color_R*0.7,color_G*0.7,color_B*0.7);
 
    rect(0, 0,   theButton.getWidth()  ,   theButton.getHeight(),7   );
    theApplet.strokeWeight(3);


    if (theButton.isInside()) {
       // mouse hovers the button
        theApplet.fill(ControlP5.ORANGE);
        theApplet.rect(0, 0, theButton.getWidth(), theButton.getHeight(),7); 
        
      if (theButton.isPressed() == true ) { // button is pressed
        theApplet.fill(ControlP5.RED);
        theApplet.rect(0, 0, theButton.getWidth(), theButton.getHeight(),7);
        move_element ();
          
      } // else {theApplet.fill(180, 150, 255); theApplet.rect(0, 0, theButton.getWidth(), theButton.getHeight(),7);}
    
    }
        

    

    
   
        
    // center the caption label 
    theApplet.textSize(Betw2/1.7);
    int x = theButton.getWidth()/2 - theButton.getCaptionLabel().getWidth()/2;
    int y = theButton.getHeight()/2 - theButton.getCaptionLabel().getHeight()/2;
    translate(x, y);
    theButton.getCaptionLabel().draw(theApplet);
   // stroke(127,9,0);
 //   fill(0, 255, 0); // scrivo pippo in verde
   //  text("pippo", x, y);
    theApplet.popMatrix();
  }
}

//------------------------------------------------------------------------------------------------------------------

class MIDI_IN_OUT implements ControllerView<Button> { 
  public void display(PGraphics theApplet, Button theButton) {
    theApplet.pushMatrix();
     theApplet.textSize(4);
    strokeWeight(2);
    noStroke();
   // stroke(50);
   // if (theButton.isOn() == false ) 
   if (theButton.isOn() == true ) fill(80,80,250); 
   else 
    fill(50);
 
   // rect(0, 0,   theButton.getWidth()  ,   theButton.getHeight(),7   );
  //  theApplet.strokeWeight(1);


    if (theButton.isInside()) {
       // mouse hovers the button
        theApplet.fill(ControlP5.ORANGE);
      //  theApplet.rect(0, 0, theButton.getWidth(), theButton.getHeight(),7); 
    }
        

    {

    if (theButton.isPressed() == true ) { // button is pressed
        theApplet.fill(ControlP5.RED);
      //  theApplet.rect(0, 0, theButton.getWidth(), theButton.getHeight(),7);
        move_element ();
          
      } // else {theApplet.fill(180, 150, 255); theApplet.rect(0, 0, theButton.getWidth(), theButton.getHeight(),7);}
    } 
   if (MIDI_OUT_LED == true && theButton.getId()  == 2018 ){ theApplet.fill(ControlP5.GREEN); }
   if (MIDI_IN_LED == true && theButton.getId()  == 2017 ){ theApplet.fill(ControlP5.GREEN); }
   
    rect(0, 0,   theButton.getWidth()  ,   theButton.getHeight(),4  );
      //  theButton.fill(0, 102, 153);
    // center the caption label 
  //   theApplet.textSize(Betw2/4);
    //  theButton.getCaptionLabel().textSize(Betw2/4);
    int x = theButton.getWidth()/2 - theButton.getCaptionLabel().getWidth()/2;
    int y = theButton.getHeight()/2 - int(theButton.getCaptionLabel().getHeight()/1.8);
    translate(x, y);
    theButton.getCaptionLabel().draw(theApplet);
    theApplet.popMatrix();
    strokeWeight(0);
    stroke(100);
  }
}

//------------------------------------------------------------------------------------------------------------------

class SisButton_page implements ControllerView<Button> { 
  public void display(PGraphics theApplet, Button theButton) {
    theApplet.pushMatrix();
    theApplet.textSize(Betw2/1.7);
//    strokeWeight(3);
    stroke(color_R*1.5,color_G*1.5,color_B*1.5);
   // if (theButton.isOn() == false ) 
   if (theButton.isOn() == true ) fill(80,80,250); 
   else 
    fill(color_R*0.7,color_G*0.7,color_B*0.7);
 
    rect(0, 0,   theButton.getWidth()  ,   theButton.getHeight(),7   );
    theApplet.strokeWeight(3);


    if (theButton.isInside()) {
       // mouse hovers the button
        theApplet.fill(ControlP5.ORANGE);
        theApplet.rect(0, 0, theButton.getWidth(), theButton.getHeight(),7); }
        

    {

    if (theButton.isPressed() == true ) { // button is pressed
        theApplet.fill(ControlP5.RED);
        theApplet.rect(0, 0, theButton.getWidth(), theButton.getHeight(),7);
        move_element ();
          
      } // else {theApplet.fill(180, 150, 255); theApplet.rect(0, 0, theButton.getWidth(), theButton.getHeight(),7);}
    } 
   
        
    // center the caption label 
    theApplet.textSize(Betw2/1.7);
    int x = theButton.getWidth()/2 - theButton.getCaptionLabel().getWidth()/2;
    int y = theButton.getHeight()/2 - theButton.getCaptionLabel().getHeight()/2;
    translate(x, y);
    theButton.getCaptionLabel().draw(theApplet);
    theApplet.popMatrix();
  }
}

//------------------------------------------------------------------------------------------------------------------

/*
class SisButton_selectpage implements ControllerView<Button> { 
  public void display(PGraphics theApplet, Button theButton) {
    theApplet.pushMatrix();
    theApplet.textSize(Betw2/1.7);
//    strokeWeight(3);
    stroke(150);
   // if (theButton.isOn() == false ) 
    fill(70);
 
    rect(0, 0,   theButton.getWidth()  ,   theButton.getHeight(),7   );
    theApplet.strokeWeight(3);


    if (theButton.isInside()) {
       // mouse hovers the button
        theApplet.fill(ControlP5.ORANGE);
        theApplet.rect(0, 0, theButton.getWidth(), theButton.getHeight(),7); }
        

    {

    if (theButton.isPressed() == true ) { // button is pressed
        theApplet.fill(ControlP5.RED);
        theApplet.rect(0, 0, theButton.getWidth(), theButton.getHeight(),7);
        move_element ();
          
      } // else {theApplet.fill(180, 150, 255); theApplet.rect(0, 0, theButton.getWidth(), theButton.getHeight(),7);}
    } 
   
        
    // center the caption label 
    theApplet.textSize(Betw2/1.7);
    int x = theButton.getWidth()/2 - theButton.getCaptionLabel().getWidth()/2;
    int y = theButton.getHeight()/2 - theButton.getCaptionLabel().getHeight()/2;
    translate(x, y);
    theButton.getCaptionLabel().draw(theApplet);
    theApplet.popMatrix();
  }
}
*/

//------------------------------------------------------------------------------------------------------------------

class SisButton2 implements ControllerView<Knob> { 
  public void display(PGraphics theApplet, Knob theButton) {
    theApplet.pushMatrix();
    theApplet.textSize(Betw2/1.7);
//    strokeWeight(3);
    stroke(150);
   
    int roundness = theButton.getWidth()/5;
   if (elementData.get(theButton.getId()).selected == true) { theApplet.fill(ControlP5.BLUE); //elementData.get(idElement).selected= false;
    }
      else { theApplet.fill(70); //elementData.get(idElement).selected= true;
    }
     

    if (theButton.isInside()) { // mouse hovers the button    
        theApplet.fill(ControlP5.YELLOW);   
     }  

    if (theButton.isMousePressed() == true ) { // button is pressed
        theApplet.fill(ControlP5.RED); 
        move_element ();       
      }
      
     if (elementData.get(theButton.getId()).feedback_ == true)     theApplet.fill(0, 255, 0); //// FEEDBACK GREEN 
    
     if (elementData.get(theButton.getId()).User_byte_10 > 1)  strokeWeight(0); else strokeWeight(3);
     
      if (elementData.get(theButton.getId()).User_byte_12 == 0)
     rect(0, 0,   theButton.getWidth()  ,   theButton.getHeight(),roundness   );
               
        masks( elementData.get(theButton.getId()).User_byte_10, // quale sprite
   (int)(0- theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/36))+ theButton.getWidth()/48,
   (int)(0- theButton.getHeight()* (( (float)elementData.get(theButton.getId()).User_byte_11)/36))+ theButton.getHeight()/48,    // posizione x y
   (int)(theButton.getWidth()+theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/18))- theButton.getWidth()/24, 
   (int)(theButton.getHeight()+theButton.getHeight()* (( (float)elementData.get(theButton.getId()).User_byte_11)/18))- theButton.getHeight()/24);  // grandezza x y
  
    if (elementData.get(theButton.getId()).User_byte_12 == 1)
     rect(0, 0,   theButton.getWidth()  ,   theButton.getHeight(),roundness   );
        
  
        noFill();
  theApplet.strokeWeight(map(elementData.get(theButton.getId()).radiusW/30,1,5,5,9));
  theApplet.stroke(255, 255, 255);
//   line(theButton.getWidth()*0.1, theButton.getHeight()*1, theButton.getWidth()*0.1, theButton.getHeight()*1-(theButton.getValue()*theButton.getHeight()/127));
 
  line(0
  +theButton.getWidth()*0.2//-(theButton.getWidth()/127)
  , theButton.getHeight(), 
  //theButton.getWidth()-
  (theButton.getValue()*theButton.getWidth()/210)+theButton.getWidth()*0.2, 
  theButton.getHeight());
  
  
 // theApplet.rect(0, 0, theButton.getWidth(), theButton.getHeight(),7);
   
  
  
  theApplet.strokeWeight(3);
  theApplet.stroke(150, 150, 150);
  
    // center the caption label 
    theApplet.textSize(Betw2/1.7);
    int x = theButton.getWidth()/2 - theButton.getCaptionLabel().getWidth()/2;
    int y = theButton.getHeight()/2 - theButton.getCaptionLabel().getHeight()/2;
    translate(x, y);
    
    
   // theButton.getCaptionLabel().draw(theApplet);
     if (infoGraph==1006)
 {
   theButton.setCaptionLabel(Integer.toString(elementData.get(theButton.getId()).memoryPosition));
 theButton.getCaptionLabel().draw(theApplet);
   }
 else 
 {
    theButton.setCaptionLabel(elementData.get(theButton.getId()).label);
    theButton.getCaptionLabel().draw(theApplet);
 }
    
    theApplet.popMatrix();
  }
}


//------------------------------------------------------------------------------------------------------------------
class SisButton_toggle implements ControllerView<Button> { 
  public void display(PGraphics theApplet, Button theButton) {
    theApplet.pushMatrix();
    theApplet.textSize(Betw2/1.7);
//    strokeWeight(3);
  //  stroke(150);
       stroke(color_R*1.5,color_G*1.5,color_B*1.5);
 
  //  fill(color_R*0.7,color_G*0.7,color_B*0.7);
  //  if (edit_mode == true )// theApplet.fill(ControlP5.GRAY);
    if (theButton.isOn() == false)
   // fill(70);
    fill(color_R*0.7,color_G*0.7,color_B*0.7);
    else theApplet.fill(ControlP5.RED);
 //   fill(70);
    rect(0, 0,   theButton.getWidth()  ,   theButton.getHeight(),7   );
    theApplet.strokeWeight(3);


    if (theButton.isInside()) {
       // mouse hovers the button
        theApplet.fill(ControlP5.ORANGE);
        theApplet.rect(0, 0, theButton.getWidth(), theButton.getHeight(),7); }
        

    {

    if (theButton.isPressed() == true ) { // button is pressed
        if (edit_mode == true ) { theApplet.fill(ControlP5.RED);
        theApplet.rect(0, 0, theButton.getWidth(), theButton.getHeight(),7);}
      //   move_element ();
          
      }  // else {theApplet.fill(180, 150, 255); theApplet.rect(0, 0, theButton.getWidth(), theButton.getHeight(),7);}
    } 
    
    
   
           //   if (elementData.get(theButton.getId()).feedback_ == true)     theApplet.fill(0, 255, 0); //// FEEDBACK
    // center the caption label 
    theApplet.textSize(Betw2/1.7);
    int x = theButton.getWidth()/2 - theButton.getCaptionLabel().getWidth()/2;
    int y = theButton.getHeight()/2 - theButton.getCaptionLabel().getHeight()/2;
    translate(x, y);
    theButton.getCaptionLabel().draw(theApplet);
    theApplet.popMatrix();
  }
}

//------------------------------------------------------------------------------------------------------------------

class RectButton implements ControllerView<Knob> {
  public void display(PGraphics theApplet, Knob theButton) {
    theApplet.pushMatrix();
    strokeWeight(3);
    stroke(175);
     int roundness = theButton.getWidth()/5;
     
  if (elementData.get(theButton.getId()).selected == true) { theApplet.fill(ControlP5.BLUE); //elementData.get(idElement).selected= false;
    }
      else { theApplet.fill(ControlP5.BLACK); //elementData.get(idElement).selected= true;
       if (elementData.get(theButton.getId()).getStateButton() == true) { theApplet.fill(170, 170, 170);  } // GREY
    } //Width //Height

    theApplet.strokeWeight(3);
 
    if (theButton.isInside()) {
      if (theButton.isMousePressed()) { // button is pressed
        theApplet.fill(ControlP5.RED);
   //     theApplet.rect(-theButton.getWidth()*0.24, theButton.getHeight()*0.22, theButton.getWidth()*1.5, theButton.getHeight()*0.6,7);
        move_element ();
      } else { // mouse hovers the button
        theApplet.fill(ControlP5.YELLOW);
     //   theApplet.rect(-theButton.getWidth()*0.24, theButton.getHeight()*0.22, theButton.getWidth()*1.5, theButton.getHeight()*0.6,7);
      }
    } else { // the mouse is located outside the button area
     // theApplet.fill(180, 150, 255);
       // theApplet.fill(ControlP5.BLACK);
    }
         if (elementData.get(theButton.getId()).feedback_ == true)     theApplet.fill(0, 255, 0); //// FEEDBACK  // verde
    
    // center the caption label 
    
     if ( elementData.get(theButton.getId()).User_byte_10 <2 )  { theApplet.stroke(150, 150, 150); theApplet.strokeWeight(3);} 
     else theApplet.strokeWeight(0);
    
    
      if (elementData.get(theButton.getId()).User_byte_12 == 0)
    
    theApplet.rect(-theButton.getWidth()*0.24, 
    theButton.getHeight()*0.22, 
    theButton.getWidth()*1.5, 
    theButton.getHeight()*0.6,roundness);
    
       masks( elementData.get(theButton.getId()).User_byte_10, // quale sprite
   (int)(((-theButton.getWidth()*0.42) - theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/18))+ theButton.getWidth()/24 ),
   (int)((theButton.getHeight()*0.22)- theButton.getHeight()* (( (float)elementData.get(theButton.getId()).User_byte_11)/18))+ theButton.getHeight()/24,    // posizione x y
   (int)( (theButton.getWidth()*1.86+theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/9))- theButton.getWidth()/12), 
   (int)(theButton.getHeight()*0.6+theButton.getHeight()* (( (float)elementData.get(theButton.getId()).User_byte_11)/9))- theButton.getHeight()/12);  // grandezza x y
  
   if (elementData.get(theButton.getId()).User_byte_12 == 1)
    
    theApplet.rect(-theButton.getWidth()*0.24, 
    theButton.getHeight()*0.22, 
    theButton.getWidth()*1.5, 
    theButton.getHeight()*0.6,roundness);

     noFill();
  theApplet.strokeWeight(map(elementData.get(theButton.getId()).radiusW/30,1,5,5,9));
  theApplet.stroke(255, 255, 255);
//   line(theButton.getWidth()*0.1, theButton.getHeight()*1, theButton.getWidth()*0.1, theButton.getHeight()*1-(theButton.getValue()*theButton.getHeight()/127));
 
  line(0
  +theButton.getWidth()*0.2//-(theButton.getWidth()/127)
  , theButton.getHeight()*0.22, 
  (theButton.getValue()*theButton.getWidth()/210)+theButton.getWidth()*0.2, 
  theButton.getHeight()*0.22);
  
  theApplet.strokeWeight(3);
  theApplet.stroke(150, 150, 150);
    
    int x = theButton.getWidth()/2 - theButton.getCaptionLabel().getWidth()/2;
    int y = theButton.getHeight()/2 - theButton.getCaptionLabel().getHeight()/2;
    translate(x, y);
   // theButton.getCaptionLabel().draw(theApplet);
   
 if (infoGraph==1006)
 {
   theButton.setCaptionLabel(Integer.toString(elementData.get(theButton.getId()).memoryPosition));
 theButton.getCaptionLabel().draw(theApplet);
   }
 else 
 {
    theButton.setCaptionLabel(elementData.get(theButton.getId()).label);
    theButton.getCaptionLabel().draw(theApplet);
 }
 
    theApplet.popMatrix();
  }
}
  //    rect(theButton.getHeight()*0.35, -theButton.getWidth()*0.8, theButton.getHeight()*0.3, theButton.getWidth()*2.6,7);
    //      rect( theButton.getWidth()*0.35, -theButton.getHeight()*0.8, theButton.getWidth()*0.3 , theButton.getHeight()*2.6 ,7);

class fader2_orizz implements ControllerView<Knob> {
  public void display(PGraphics theApplet, Knob theButton) {
    theApplet.pushMatrix();
    strokeWeight(3);
    stroke(175);
    theApplet.fill(ControlP5.BLACK);
     
    int roundness = theButton.getWidth()/5;
    rect( -theButton.getWidth()*0.8, theButton.getHeight()*0.35, theButton.getWidth()*2.6 , theButton.getHeight()*0.3 ,7);  // stanghetta lunga
    
    if (elementData.get(theButton.getId()).getStateButton() == true) { theApplet.fill(170, 170, 170);  } // GREY
    
    if (elementData.get(theButton.getId()).selected == true) { theApplet.fill(ControlP5.BLUE); //elementData.get(idElement).selected= false;
    }
      else { theApplet.fill(150, 150, 150);
     // theApplet.fill(ControlP5.BLACK); //elementData.get(idElement).selected= true;
    }
     
    theApplet.strokeWeight(3);
    if (theButton.isInside()) {
      if (theButton.isMousePressed()) { // button is pressed
        theApplet.fill(ControlP5.RED);
      //  theApplet.rect((theButton.getValue()*theButton.getWidth()/127) -theButton.getWidth()/2 , 0, theButton.getWidth(), theButton.getHeight(), roundness);
        move_element ();
      } else { // mouse hovers the button
        theApplet.fill(ControlP5.YELLOW);
     //   theApplet.rect((theButton.getValue()*theButton.getWidth()/127) -theButton.getWidth()/2  , 0, theButton.getWidth(), theButton.getHeight(), roundness);
       //  int roundness = theButton.getWidth()/5;
      }
    } else { // the mouse is located outside the button area
     // theApplet.fill(180, 150, 255);
    }
        //  if (elementData.get(theButton.getId()).feedback_ == true)     theApplet.fill(0, 255, 0); //// FEEDBACK
           if (elementData.get(theButton.getId()).feedback_ == true)     theApplet.fill(0, 255, 0); //// FEEDBACK
           
            if (elementData.get(theButton.getId()).User_byte_10 > 1)  strokeWeight(0);
  
   if (elementData.get(theButton.getId()).User_byte_12 == 0)    
   theApplet.rect((theButton.getValue()*theButton.getWidth()/127) -theButton.getWidth()/2  , 0, theButton.getWidth(), theButton.getHeight(), roundness);
            
   masks( elementData.get(theButton.getId()).User_byte_10, // quale sprite
   (int)(((theButton.getValue()*theButton.getWidth()/127) -theButton.getWidth()/2 ) // sposta a destra e sinistra
   - theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/18))+ theButton.getWidth()/24,  
   (int)(0- theButton.getHeight()* (( (float)elementData.get(theButton.getId()).User_byte_11)/18))+ theButton.getHeight()/24,    // posizione x y
   (int)(theButton.getWidth()+theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/9))- theButton.getWidth()/12, 
   (int)(theButton.getHeight()+theButton.getHeight()* (( (float)elementData.get(theButton.getId()).User_byte_11)/9))- theButton.getHeight()/12);  // grandezza x y
      
   if (elementData.get(theButton.getId()).User_byte_12 == 1)      
   theApplet.rect((theButton.getValue()*theButton.getWidth()/127) -theButton.getWidth()/2  , 0, theButton.getWidth(), theButton.getHeight(), roundness);
  
  
       
    int x = int(theButton.getValue()*theButton.getWidth()/127   - theButton.getCaptionLabel().getWidth()/2);
    int y = theButton.getHeight()/2 - 2 -theButton.getCaptionLabel().getHeight()/2;
    theApplet.popMatrix();
    translate(x, y);
    //theButton.getCaptionLabel().draw(theApplet);
     if (infoGraph==1006)
 {
   theButton.setCaptionLabel(Integer.toString(elementData.get(theButton.getId()).memoryPosition));
 theButton.getCaptionLabel().draw(theApplet);
   }
 else 
 {
    theButton.setCaptionLabel(elementData.get(theButton.getId()).label);
    theButton.getCaptionLabel().draw(theApplet);
 }
  }
}


//------------------------------------------------------------------------------------------------------------------

class fader3_vert implements ControllerView<Knob> {
  public void display(PGraphics theApplet, Knob theButton) {
    theApplet.pushMatrix();
    strokeWeight(3);
    stroke(175);
    theApplet.fill(ControlP5.BLACK);
    
    int roundness = theButton.getWidth()/5;
  
   rect( theButton.getWidth()*0.35, -theButton.getHeight()*0.8, theButton.getWidth()*0.3 , theButton.getHeight()*2.6 ,7);
       if (elementData.get(theButton.getId()).getStateButton() == true) { theApplet.fill(170, 170, 170);  } // GREY
       
    if (elementData.get(theButton.getId()).selected == true) { theApplet.fill(ControlP5.BLUE); //elementData.get(idElement).selected= false;
    }
      else { //theApplet.fill(ControlP5.BLACK); //elementData.get(idElement).selected= true;
       theApplet.fill(150, 150, 150);
    }
     //  if (elementData.get(theButton.getId()).feedback_ == true)     theApplet.fill(0, 255, 0); //// FEEDBACK - COLORE VERDE
 //  ;
 //   rect(0  , 
  //   theButton.getHeight()/2-(theButton.getValue()*theButton.getHeight()/127) 
  //  ,                                // muovo il cursore - secondo il feedback midi
  //  theButton.getWidth(), theButton.getHeight(), 7);  
    
    theApplet.strokeWeight(3);
    if (theButton.isInside()) {
      if (theButton.isMousePressed()) { // button is pressed
        theApplet.fill(ControlP5.RED);
      //  theApplet.rect(0  , theButton.getHeight()/2-(theButton.getValue()*theButton.getHeight()/127) , theButton.getWidth(), theButton.getHeight(), 7);
        move_element ();
      } else { // mouse hovers the button
        theApplet.fill(ControlP5.YELLOW);
       // theApplet.rect(0  ,  theButton.getHeight()/2-(theButton.getValue()*theButton.getHeight()/127) , theButton.getWidth(), theButton.getHeight(), 7);
      }
    } else { // the mouse is located outside the button area
     // theApplet.fill(180, 150, 255);
    }
    
      if (elementData.get(theButton.getId()).feedback_ == true)     theApplet.fill(0, 255, 0); //// FEEDBACK - COLORE VERDE
      
      if (elementData.get(theButton.getId()).User_byte_10 > 1)  strokeWeight(0); else  strokeWeight(3);
            
      
        if (elementData.get(theButton.getId()).User_byte_12 == 0) 
        theApplet.rect(0  ,  theButton.getHeight()/2-(theButton.getValue()*theButton.getHeight()/127) , theButton.getWidth(), theButton.getHeight(), roundness);
      
          masks( elementData.get(theButton.getId()).User_byte_10, // quale sprite
   (int)(
  // ((theButton.getValue()*theButton.getWidth()/127) -theButton.getWidth()/2 ) // sposta a destra e sinistra
   - theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/18))+ theButton.getWidth()/24,
   
   (int)(
     ( theButton.getHeight()/2-(theButton.getValue()*theButton.getHeight()/127))
   - theButton.getHeight()* (( (float)elementData.get(theButton.getId()).User_byte_11)/18))+ theButton.getHeight()/24,    // posizione x y
   
   (int)(theButton.getWidth()+theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/9))- theButton.getWidth()/12, 
   (int)(theButton.getHeight()+theButton.getHeight()* (( (float)elementData.get(theButton.getId()).User_byte_11)/9))- theButton.getHeight()/12);  // grandezza x y
    
    
        if (elementData.get(theButton.getId()).User_byte_12 == 1) 
        theApplet.rect(0  ,  theButton.getHeight()/2-(theButton.getValue()*theButton.getHeight()/127) , theButton.getWidth(), theButton.getHeight(), roundness);
      

    int x =   theButton.getWidth()/2 - 1 - theButton.getCaptionLabel().getWidth()/2;
    int y = int(
    
    theButton.getHeight() 
    -theButton.getCaptionLabel().getHeight()/2 
    - theButton.getValue()*theButton.getHeight()/127   )
    ;
    
    theApplet.popMatrix();
    translate(x, y);
   // theButton.getCaptionLabel().draw(theApplet);
     if (infoGraph==1006)
 {
   theButton.setCaptionLabel(Integer.toString(elementData.get(theButton.getId()).memoryPosition));
 theButton.getCaptionLabel().draw(theApplet);
   }
 else 
 {
    theButton.setCaptionLabel(elementData.get(theButton.getId()).label);
    theButton.getCaptionLabel().draw(theApplet);
 }
  }
}



//------------------------------------------------------------------------------------------------------------------

class fader implements ControllerView<Knob> {
  public void display(PGraphics theApplet, Knob theButton) {
    theApplet.pushMatrix();
    strokeWeight(3);
    stroke(175);
    int roundness = theButton.getWidth()/5;
    
     if (elementData.get(theButton.getId()).selected == true) { theApplet.fill(ControlP5.BLUE); //elementData.get(idElement).selected= false;
    }
      else { theApplet.fill(ControlP5.BLACK); //elementData.get(idElement).selected= true;
       if (elementData.get(theButton.getId()).getStateButton() == true) { theApplet.fill(170, 170, 170);  } // GREY
    }
  
   //  theApplet.strokeWeight(3);
 
    if (theButton.isInside()) {
      if (theButton.isMousePressed()) { // button is pressed
        theApplet.fill(ControlP5.RED);
     //   theApplet.rect(theButton.getWidth()*0.1, -theButton.getHeight()*0.2, theButton.getWidth()*0.8, theButton.getHeight()*1.5,7);
        move_element ();
      } else { // mouse hovers the button
        theApplet.fill(ControlP5.YELLOW);
      //  theApplet.rect(theButton.getWidth()*0.1, -theButton.getHeight()*0.2, theButton.getWidth()*0.8, theButton.getHeight()*1.5,7);
      }
    } else { // the mouse is located outside the button area
     // theApplet.fill(180, 150, 255);
    }
    
       if (elementData.get(theButton.getId()).feedback_ == true)     theApplet.fill(0, 255, 0); //// FEEDBACK // verde
     
     if (elementData.get(theButton.getId()).User_byte_10 > 0 )     strokeWeight(0);
     
      if (elementData.get(theButton.getId()).User_byte_12 == 0) // background
      rect(theButton.getWidth()*0.1, -theButton.getHeight()*0.2, theButton.getWidth()*0.8, theButton.getHeight()*1.5,roundness);
         
           masks( elementData.get(theButton.getId()).User_byte_10, // quale sprite
   (int)(theButton.getWidth()*0.1- theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/18))+ theButton.getWidth()/24,
   (int)((-theButton.getHeight()*0.38) -theButton.getHeight()* (( (float)elementData.get(theButton.getId()).User_byte_11)/18))+ theButton.getHeight()/24,    // posizione x y
   (int)(theButton.getWidth()*0.8+theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/9))- theButton.getWidth()/12, 
   (int)(theButton.getHeight()*1.85+theButton.getHeight()* (( (float)elementData.get(theButton.getId()).User_byte_11)/9))- theButton.getHeight()/12);  // grandezza x y
  
     if (elementData.get(theButton.getId()).User_byte_12 == 1)
     rect(theButton.getWidth()*0.1, -theButton.getHeight()*0.2, theButton.getWidth()*0.8, theButton.getHeight()*1.5,roundness);
       
  
                    noFill();
  theApplet.strokeWeight(map(elementData.get(theButton.getId()).radiusW/30,1,5,5,9));
  theApplet.stroke(255, 255, 255);
  line(theButton.getWidth()*0.1, theButton.getHeight()*1, theButton.getWidth()*0.1, theButton.getHeight()*1-(theButton.getValue()*theButton.getHeight()/127));
  theApplet.strokeWeight(3);
  theApplet.stroke(150, 150, 150);
          
    // center the caption label 
    int x = theButton.getWidth()/2 - theButton.getCaptionLabel().getWidth()/2;
    int y = theButton.getHeight()/2 - theButton.getCaptionLabel().getHeight()/2;
    translate(x, y);
    //theButton.getCaptionLabel().draw(theApplet);
    
     if (infoGraph==1006)
 {
   theButton.setCaptionLabel(Integer.toString(elementData.get(theButton.getId()).memoryPosition));
 theButton.getCaptionLabel().draw(theApplet);
   }
 else 
 {
    theButton.setCaptionLabel(elementData.get(theButton.getId()).label);
    theButton.getCaptionLabel().draw(theApplet);
 }
    theApplet.popMatrix();
  }
}


//------------------------------------------------------------------------------------------------------------------

class polygon_ implements ControllerView<Knob> {
  public void display(PGraphics theApplet, Knob theButton) {
    theApplet.pushMatrix();
    strokeWeight(3);
    stroke(175);
  //  fill(250);
     if (elementData.get(theButton.getId()).selected == true) { theApplet.fill(ControlP5.BLUE); //elementData.get(idElement).selected= false;
    }
      else { theApplet.fill(ControlP5.BLACK); //elementData.get(idElement).selected= true;
       if (elementData.get(theButton.getId()).getStateButton() == true) { theApplet.fill(170, 170, 170);  } // GREY
    }
 /*
  pushMatrix();
  translate(theButton.getHeight()/2, theButton.getHeight()/2);
  rotate(frameCount / 100.0 + 
  radians(elementData.get(theButton.getId()).encoder_rotate));
  polygon(0, 0, theButton.getWidth(), 5);  
  popMatrix();
  */
  
  
    theApplet.strokeWeight(3);
  
    if (theButton.isInside()) {
      if (theButton.isMousePressed()) { // button is pressed
        theApplet.fill(ControlP5.RED);
     
          //    if (elementData.get(theButton.getId()).feedback_ == true)     theApplet.fill(0, 255, 0); //// FEEDBACK
 /* pushMatrix();
  translate(theButton.getHeight()/2, theButton.getHeight()/2);
  rotate(frameCount / 100.0 + 
  radians(elementData.get(theButton.getId()).encoder_rotate));
  polygon(0, 0, theButton.getHeight(), 5);  // Triangle
  popMatrix();
  */
  
        move_element ();
      } else { // mouse hovers the button
        theApplet.fill(ControlP5.YELLOW);
       // theApplet.rect(-theButton.getHeight()*0.2, theButton.getWidth()*0.2, theButton.getHeight()*1.5, theButton.getWidth()*0.6,7);
 /* pushMatrix();
  translate(theButton.getHeight()/2, theButton.getHeight()/2); 
  rotate(frameCount / 100.0 + 
  radians(elementData.get(theButton.getId()).encoder_rotate));
  polygon(0, 0, theButton.getHeight(), 5);  // Triangle
  popMatrix();
  */
  
  
  
      }
    } else { // the mouse is located outside the button area
    //  theApplet.fill(180, 150, 255);
      
      
      
    }
    
     if (elementData.get(theButton.getId()).feedback_ == true)     theApplet.fill(0, 255, 0); //// FEEDBACK

  pushMatrix();
  translate(theButton.getHeight()/2, theButton.getHeight()/2); 
  rotate(frameCount / 100.0 + 
  radians(elementData.get(theButton.getId()).encoder_rotate));
  polygon(0, 0, theButton.getHeight(), 5);  // Triangle
  popMatrix();
    
    
    
    
    // center the caption label 
    int x = theButton.getWidth()/2 - theButton.getCaptionLabel().getWidth()/2;
    int y = theButton.getWidth()/2 - theButton.getCaptionLabel().getWidth()/2;
    translate(x, y);
 //   theButton.getCaptionLabel().draw(theApplet);
     if (infoGraph==1006)
 {
   theButton.setCaptionLabel(Integer.toString(elementData.get(theButton.getId()).memoryPosition));
 theButton.getCaptionLabel().draw(theApplet);
   }
 else 
 {
    theButton.setCaptionLabel(elementData.get(theButton.getId()).label);
    theButton.getCaptionLabel().draw(theApplet);
 }
    theApplet.popMatrix();
    
      noFill();
  theApplet.strokeWeight(map(elementData.get(theButton.getId()).radiusW/30,1,5,5,9));
  theApplet.stroke(255, 128, 0);
  arc(0, 0, theButton.getWidth(), theButton.getWidth(), theButton.getStartAngle(), theButton.getAngle());
  theApplet.strokeWeight(3);
   theApplet.stroke(150, 150, 150);
    
    
  }
}


//------------------------------------------------------------------------------------------------------------------
class Grigio implements ControllerView<Knob> {
  public void display(PGraphics theApplet, Knob theButton) {
    theApplet.pushMatrix();
    
   // int roundness = theButton.getWidth()/5;
    
  
     
    if (theButton.isInside()) {
      if (theButton.isMousePressed()) { // button is pressed
        theApplet.fill(ControlP5.RED);
        move_element ();
      } else { // mouse hovers the button
        theApplet.fill(ControlP5.YELLOW);
      }
    } else { // the mouse is located outside the button area
    //  if (theButton.isActive() == true) theApplet.fill(ControlP5.BLACK); else  theApplet.fill(ControlP5.WHITE);
      if (elementData.get(theButton.getId()).selected == true) { theApplet.fill(ControlP5.BLUE); //elementData.get(idElement).selected= false;
    }
      else { theApplet.fill(ControlP5.BLACK); //elementData.get(idElement).selected= true;
       if (elementData.get(theButton.getId()).getStateButton() == true) { theApplet.fill(170, 170, 170);  } // GREY
    }
   //   theApplet.fill(ControlP5.BLACK); 
    
    
    }

       if (elementData.get(theButton.getId()).User_byte_10 ==0){
      theApplet.strokeWeight(3);
    theApplet.stroke(255, 128, 0);
   } 
     else {
   //   theApplet.strokeWeight(0);
   //  theApplet.stroke(255, 128, 0);} 
     }
     
     
               if ( elementData.get(theButton.getId()).User_byte_10 < 2  )  { theApplet.stroke(150, 150, 150); theApplet.strokeWeight(3);} 
               else theApplet.strokeWeight(0);

          if (elementData.get(theButton.getId()).feedback_ == true)     theApplet.fill(0, 255, 0); //// FEEDBACK
      
       if (elementData.get(theButton.getId()).User_byte_12 == 0)   
        theApplet.ellipse(0, 0, theButton.getWidth(), theButton.getHeight());   
 
    masks( elementData.get(theButton.getId()).User_byte_10, // quale sprite
   (int)(0- theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/12))+ theButton.getWidth()/24,
   (int)(0- theButton.getHeight()* (( (float)elementData.get(theButton.getId()).User_byte_11)/12))+ theButton.getHeight()/24,    // posizione x y
   (int)(theButton.getWidth()+theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/6))- theButton.getWidth()/12, 
   (int)(theButton.getHeight()+theButton.getHeight()* (( (float)elementData.get(theButton.getId()).User_byte_11)/6))- theButton.getHeight()/12);  // grandezza x y
  
 
      if (elementData.get(theButton.getId()).User_byte_12 == 1)   
        theApplet.ellipse(0, 0, theButton.getWidth(), theButton.getHeight());   
 
 
    // center the caption label 
    int x = theButton.getWidth()/2  - theButton.getCaptionLabel().getWidth()/2;
    int y = theButton.getWidth()/2  -theButton.getCaptionLabel().getHeight()/2;
 // int y = theButton.getWidth()/2 - 1 - theButton.getCaptionLabel().getWidth()/2;
    theApplet.pushMatrix();
    translate(x, y);
    // ellipse(0, 0, elementData.get(theButton.getId()).radiusW, elementData.get(theButton.getId()).radiusW);
    // ellipse(0, 0, theButton.getWidth(), theButton.getWidth());
   if (infoGraph==1006)
 {
   theButton.setCaptionLabel(Integer.toString(elementData.get(theButton.getId()).memoryPosition));
 theButton.getCaptionLabel().draw(theApplet);
   }
 else 
 {
    theButton.setCaptionLabel(elementData.get(theButton.getId()).label);
    theButton.getCaptionLabel().draw(theApplet);
 }
    
  
    theApplet.popMatrix();
    theApplet.popMatrix();
    // translate(0, 0);
  }
}

//------------------------------------------------------------------------------------------------------------------

class Manopolox implements ControllerView<Knob> {
  public void display(PGraphics theApplet, Knob theButton) {
    theApplet.pushMatrix();
    
    if (theButton.isInside()) {
      if (theButton.isMousePressed()) { // button is pressed
        theApplet.fill(ControlP5.RED);  theApplet.stroke(ControlP5.RED);
        move_element ();
      } else { // mouse hovers the button
        theApplet.fill(ControlP5.YELLOW);  theApplet.stroke(ControlP5.YELLOW);
      }
    } else { // the mouse is located outside the button area
      // if (theButton.isActive() == true) theApplet.fill(ControlP5.BLACK); else  // theApplet.fill(ControlP5.WHITE);
      theApplet.fill(ControlP5.BLACK);
      if (elementData.get(theButton.getId()).getStateButton() == true) { theApplet.fill(170, 170, 170);   } // GREY
      
      if (elementData.get(theButton.getId()).selected == true) { theApplet.fill(ControlP5.BLUE);  theApplet.stroke(ControlP5.BLUE);} //elementData.get(idElement).selected= false;
  
    
    
           }
           
       
     if ( elementData.get(theButton.getId()).User_byte_10 < 2  )  { theApplet.stroke(150, 150, 150); theApplet.strokeWeight(3);} 
     else theApplet.strokeWeight(0);

           if (elementData.get(theButton.getId()).feedback_ == true)     theApplet.fill(0, 255, 0); //// FEEDBACK // verde
           
   if (elementData.get(theButton.getId()).User_byte_12 == 0)
   theApplet.ellipse(0, 0, theButton.getWidth(), theButton.getWidth());
  
        
   masks( elementData.get(theButton.getId()).User_byte_10, // quale sprite
   (int)(0- theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/18))+ theButton.getWidth()/24,
   (int)(0- theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/18))+ theButton.getWidth()/24,    // posizione x y
   (int)(theButton.getWidth()+theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/9))- theButton.getWidth()/12, 
   (int)(theButton.getWidth()+theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/9))- theButton.getWidth()/12);  // grandezza x y
  
     if (elementData.get(theButton.getId()).User_byte_12 == 1)
   theApplet.ellipse(0, 0, theButton.getWidth(), theButton.getWidth());
    
  noFill();
  theApplet.strokeWeight(map(elementData.get(theButton.getId()).radiusW/30,1,5,5,9));
  theApplet.stroke(255, 128, 0);
  arc(0, 0, theButton.getWidth(), theButton.getWidth(), theButton.getStartAngle(), theButton.getAngle());
  theApplet.strokeWeight(3);


    
   
    theApplet.pushMatrix();
  
   // cp5.setFont(f, (int)(theButton.getWidth()));
      
    int x =   (int) (theButton.getWidth()/2 
    - theButton.getCaptionLabel().getWidth()/1.85
   )
    ;
    int y =   (int) (theButton.getHeight()/2 
    -theButton.getCaptionLabel().getHeight()/1.85
   )
    ;
    
    translate(x, y);
  
 

  // theButton.getCaptionLabel().draw(theApplet);
   
   
 if (infoGraph==1006)
 {
   theButton.setCaptionLabel(Integer.toString(elementData.get(theButton.getId()).memoryPosition));
 theButton.getCaptionLabel().draw(theApplet);
   }
 else 
 {
    theButton.setCaptionLabel(elementData.get(theButton.getId()).label);
    theButton.getCaptionLabel().draw(theApplet);
 }
 
    theApplet.popMatrix();
    theApplet.popMatrix();
  }
}


//------------------------------------------------------------------------------------------------------------------
class Manopolox2 implements ControllerView<Knob> {
  public void display(PGraphics theApplet, Knob theButton) {
    theApplet.pushMatrix();
    
     theApplet.strokeWeight(3);
    if (theButton.isInside()) {
      if (theButton.isMousePressed()) { // button is pressed
        theApplet.fill(ControlP5.RED);
        move_element ();
      } else { // mouse hovers the button
        theApplet.fill(ControlP5.YELLOW);
      }
    } else { // the mouse is located outside the button area
   
      theApplet.fill(ControlP5.BLACK);
       if (elementData.get(theButton.getId()).getStateButton() == true) { theApplet.fill(170, 170, 170);  } // GREY
       
      if (elementData.get(theButton.getId()).selected == true) { theApplet.fill(ControlP5.BLUE);} //elementData.get(idElement).selected= false;

          theApplet.strokeWeight(3);
     theApplet.stroke(150, 150, 150);
    
    }

           if (elementData.get(theButton.getId()).feedback_ == true)     theApplet.fill(0, 255, 0); //// FEEDBACK  // verde
   
   if ( elementData.get(theButton.getId()).User_byte_10 <2 )  { theApplet.stroke(150, 150, 150); theApplet.strokeWeight(3);} 
     else theApplet.strokeWeight(0);
          
      if (elementData.get(theButton.getId()).User_byte_12 == 0)      
    theApplet.ellipse(0, 0, theButton.getWidth(),theButton.getWidth());
    
      masks( elementData.get(theButton.getId()).User_byte_10, // quale sprite
   (int)(0- theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/18))+ theButton.getWidth()/24,
   (int)(0- theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/18))+ theButton.getWidth()/24,    // posizione x y
   (int)(theButton.getWidth()+theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/9))- theButton.getWidth()/12, 
   (int)(theButton.getWidth()+theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/9))- theButton.getWidth()/12);  // grandezza x y
  
     if (elementData.get(theButton.getId()).User_byte_12 == 1)      
    theApplet.ellipse(0, 0, theButton.getWidth(),theButton.getWidth());
    
  noFill();
  theApplet.strokeWeight(map(elementData.get(theButton.getId()).radiusW/30,1,5,5,9));
  theApplet.stroke(255, 255, 255);
  arc(0, 0, theButton.getWidth(), theButton.getWidth(), theButton.getStartAngle(), theButton.getAngle());
  theApplet.strokeWeight(3);
   theApplet.stroke(150, 150, 150);
    // center the caption label 
    int x = (int)(theButton.getWidth()/2  - theButton.getCaptionLabel().getWidth()/1.85);
    int y = (int)(theButton.getWidth()/2  -theButton.getCaptionLabel().getHeight()/1.85);
    theApplet.pushMatrix();
    translate(x, y);
    // theButton.setColor(0,120,0);
  //cp5.setColorActive(0xffff0000);
  
  
 if (infoGraph==1006)
 {
 theButton.setCaptionLabel(Integer.toString(elementData.get(theButton.getId()).memoryPosition));
 theButton.getCaptionLabel().draw(theApplet);
   }
 else 
 {
    theButton.setCaptionLabel(elementData.get(theButton.getId()).label);
    theButton.getCaptionLabel().draw(theApplet);
 }
 
 
   // theButton.getValueLabel().draw(theApplet);
  // theButton.draw(theApplet);
    theApplet.popMatrix();
    theApplet.popMatrix();
  }
}
//------------------------------------------------------------------------------------------------------------------
class faderox implements ControllerView<Knob> {
  public void display(PGraphics theApplet, Knob theButton) {
    theApplet.pushMatrix();
    strokeWeight(3);
    stroke(175);
    //fill(32);
     if (elementData.get(theButton.getId()).selected == true) { theApplet.fill(ControlP5.BLUE); //elementData.get(idElement).selected= false;
    }
      else { theApplet.fill(ControlP5.BLACK); //elementData.get(idElement).selected= true;
       if (elementData.get(theButton.getId()).getStateButton() == true) { theApplet.fill(170, 170, 170);  } // GREY
    }
    // if (elementData.get(theButton.getId()).feedback_ == true)     theApplet.fill(0, 255, 0); //// FEEDBACK
  //  rect(theButton.getWidth()*0.1, -theButton.getHeight()*0.2, theButton.getWidth()*0.8, theButton.getHeight()*1.5,7);
    theApplet.strokeWeight(3);
    //stroke(255, 0, 0);
    //strokeWeight(2);
    //line(0, theButton.getHeight()/2, theButton.getWidth()/5, theButton.getHeight()/2);
    //line(theButton.getWidth()*4/5, theButton.getHeight()/2, theButton.getWidth(), theButton.getHeight()/2);
    if (theButton.isInside()) {
      if (theButton.isMousePressed()) { // button is pressed
        theApplet.fill(ControlP5.RED);
      //  theApplet.rect(theButton.getWidth()*0.1, -theButton.getHeight()*0.2, theButton.getWidth()*0.8, theButton.getHeight()*1.5,7);
        move_element ();
      } else { // mouse hovers the button
        theApplet.fill(ControlP5.YELLOW);
      //  theApplet.rect(theButton.getWidth()*0.1, -theButton.getHeight()*0.2, theButton.getWidth()*0.8, theButton.getHeight()*1.5,7);
      }
    } else { // the mouse is located outside the button area
     // theApplet.fill(180, 150, 255);
    }
    
     if (elementData.get(theButton.getId()).feedback_ == true)     theApplet.fill(0, 255, 0); //// FEEDBACK
     if ( elementData.get(theButton.getId()).User_byte_10 >1 )  theApplet.strokeWeight(0); else theApplet.strokeWeight(3);
     
     if (elementData.get(theButton.getId()).User_byte_12 == 0)
     theApplet.rect(theButton.getWidth()*0.1, -theButton.getHeight()*0.2, theButton.getWidth()*0.8, theButton.getHeight()*1.5,7);
                 
           masks( elementData.get(theButton.getId()).User_byte_10, // quale sprite
   (int)(theButton.getWidth()*0.1- theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/18))+ theButton.getWidth()/24,
   (int)((-theButton.getHeight()*0.2) - theButton.getHeight()* (( (float)elementData.get(theButton.getId()).User_byte_11)/18))+ theButton.getHeight()/24,    // posizione x y
   (int)(theButton.getWidth()*0.8+theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/9))- theButton.getWidth()/12, 
   (int)(theButton.getHeight()*1.5+theButton.getHeight()* (( (float)elementData.get(theButton.getId()).User_byte_11)/9))- theButton.getHeight()/12);  // grandezza x y
    
     if (elementData.get(theButton.getId()).User_byte_12 == 1)
     theApplet.rect(theButton.getWidth()*0.1, -theButton.getHeight()*0.2, theButton.getWidth()*0.8, theButton.getHeight()*1.5,7);
         
          
          noFill();
  theApplet.strokeWeight(map(elementData.get(theButton.getId()).radiusW/30,1,5,5,9));
  theApplet.stroke(255, 128, 0);
  line(theButton.getWidth()*0.1, theButton.getHeight()*1, theButton.getWidth()*0.1, theButton.getHeight()*1-(theButton.getValue()*theButton.getHeight()/127));
  theApplet.strokeWeight(3);
  theApplet.stroke(150, 150, 150);
   
    // center the caption label 
    int x = theButton.getWidth()/2 - theButton.getCaptionLabel().getWidth()/2;
    int y = theButton.getHeight()/2 - theButton.getCaptionLabel().getHeight()/2;
    translate(x, y);
    if (infoGraph==1006)
 {
   theButton.setCaptionLabel(Integer.toString(elementData.get(theButton.getId()).memoryPosition));
 theButton.getCaptionLabel().draw(theApplet);
   }
 else 
 {
    theButton.setCaptionLabel(elementData.get(theButton.getId()).label);
    theButton.getCaptionLabel().draw(theApplet);
 }
    theApplet.popMatrix();
  }
}

//------------------------------------------------------------------------------------------------------------------
class SensorButton implements ControllerView<Knob> {
  public void display(PGraphics theApplet, Knob theButton) {
    theApplet.pushMatrix();
    strokeWeight(3);
     stroke(180, 180, 180);
    noFill();
    
    int roundness = theButton.getWidth()/5;
    if (elementData.get(theButton.getId()).selected == true) { theApplet.fill(ControlP5.BLUE); //elementData.get(idElement).selected= false;
    }
      else { theApplet.noFill(); //elementData.get(idElement).selected= true;
       if (elementData.get(theButton.getId()).getStateButton() == true) { theApplet.fill(170, 170, 170);  } // GREY
    }
    
   
    theApplet.strokeWeight(3);
    stroke(180, 180, 180);
    strokeWeight(3);
    if (theButton.isInside()) {
      if (theButton.isMousePressed()) { // button is pressed
         move_element ();
        theApplet.fill(ControlP5.RED);
     //   theApplet.rect(0, 0, theButton.getWidth(), theButton.getHeight(),7);
      } else { // mouse hovers the button
        theApplet.fill(ControlP5.YELLOW);
     //   theApplet.rect(0, 0, theButton.getWidth(), theButton.getHeight(),7);
      }
    } else { // the mouse is located outside the button area
    //  theApplet.fill(180, 150, 255);
    }
    
   if (elementData.get(theButton.getId()).feedback_ == true)     theApplet.fill(0, 255, 0); //// FEEDBACK
  
   if (elementData.get(theButton.getId()).User_byte_10 > 1 )  strokeWeight(0); else strokeWeight(3);
   if (elementData.get(theButton.getId()).User_byte_12 == 0) 
   theApplet.rect(0, 0, theButton.getWidth(), theButton.getHeight(),roundness);
    
   masks( elementData.get(theButton.getId()).User_byte_10, // quale sprite
   (int)(0- theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/18))+ theButton.getWidth()/24,
   (int)(0- theButton.getHeight()* (( (float)elementData.get(theButton.getId()).User_byte_11)/18))+ theButton.getHeight()/24,    // posizione x y
   (int)(theButton.getWidth()+theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/9))- theButton.getWidth()/12, 
   (int)(theButton.getHeight()+theButton.getHeight()* (( (float)elementData.get(theButton.getId()).User_byte_11)/9))- theButton.getHeight()/12);  // grandezza x y
  
   if (elementData.get(theButton.getId()).User_byte_12 == 1) 
   theApplet.rect(0, 0, theButton.getWidth(), theButton.getHeight(),roundness);
    
  noFill();
  theApplet.strokeWeight(map(elementData.get(theButton.getId()).radiusW/30,1,5,5,9));
  theApplet.stroke(255, 255, 255);
//   line(theButton.getWidth()*0.1, theButton.getHeight()*1, theButton.getWidth()*0.1, theButton.getHeight()*1-(theButton.getValue()*theButton.getHeight()/127));
 
  if (elementData.get(theButton.getId()).toggleMode < 17 && page_selected == false || 
  elementData.get(theButton.getId()).toggleMode_2nd < 17 && page_selected == true )
 
  line(0
  +theButton.getWidth()*0.2//-(theButton.getWidth()/127)
  , theButton.getHeight(), 
  //theButton.getWidth()-
  (theButton.getValue()*theButton.getWidth()/210)+theButton.getWidth()*0.2, 
  theButton.getHeight());
  
  
  theApplet.strokeWeight(3);
  theApplet.stroke(150, 150, 150);
  
    // center the caption label 
    int x = theButton.getWidth()/2 - theButton.getCaptionLabel().getWidth()/2;
    int y = theButton.getHeight()/2 - theButton.getCaptionLabel().getHeight()/2;
    translate(x, y);
    //theApplet.textSize(80);
  //  theButton.getCaptionLabel().draw(theApplet);
     if (infoGraph==1006)
 {
   theButton.setCaptionLabel(Integer.toString(elementData.get(theButton.getId()).memoryPosition));
 theButton.getCaptionLabel().draw(theApplet);
   }
 else 
 {
    theButton.setCaptionLabel(elementData.get(theButton.getId()).label);
    theButton.getCaptionLabel().draw(theApplet);
 }
    theApplet.popMatrix();
  }
}



//------------------------------------------------------------------------------------------------------------------
class Marrone implements ControllerView<Button> {
  public void display(PGraphics theApplet, Button theButton) {
    theApplet.pushMatrix();
       
    if (theButton.isInside()) {
      if (theButton.isPressed()) { // button is pressed
        theApplet.fill(ControlP5.RED);
  
  move_element ();
       
      } else { // mouse hovers the button
        theApplet.fill(ControlP5.YELLOW);
      }
    } else { // the mouse is located outside the button area
       if (elementData.get(theButton.getId()).selected == true) { theApplet.fill(ControlP5.BLUE); //elementData.get(idElement).selected= false;
    }
      else { theApplet.fill(ControlP5.BLACK); //elementData.get(idElement).selected= true;
       if (elementData.get(theButton.getId()).getStateButton() == true) { theApplet.fill(170, 170, 170);  } // GREY
    }
    }
    
    if (elementData.get(theButton.getId()).feedback_ == true)     theApplet.fill(0, 255, 0); /// FEEDBACK
    theApplet.strokeWeight(3);
     //theApplet.fill(0);
      theApplet.stroke(255, 255, 255);
       //  theApplet.fill(255,128,0);
       
   // theApplet.ellipse(0, 0, theButton.getWidth(), theButton.getHeight());
    theApplet.ellipse(0, 0, theButton.getWidth(), theButton.getWidth());
    
    int x = theButton.getWidth()/2 - 1 - theButton.getCaptionLabel().getWidth()/2;
    int y = theButton.getWidth()/2 - 2 -theButton.getCaptionLabel().getHeight()/2;
    theApplet.pushMatrix();
    translate(x, y);
    theButton.getCaptionLabel().draw(theApplet);
    theApplet.popMatrix();
    theApplet.popMatrix();
  }
}

//------------------------------------------------------------------------------------------------------------------
class Marrone_polygon implements ControllerView<Knob> {
  public void display(PGraphics theApplet, Knob theButton) {
       //  theApplet.
          fill(100);
         theApplet.stroke(180);
    pushMatrix();
    noFill();
  translate(theButton.getWidth()/2, theButton.getWidth()/2);
  // color(255,0,0);
  // fill(32);
  
  rotate(frameCount / 100.0 + 
  radians(elementData.get(theButton.getId()).encoder_rotate)
  );
  polygon(0, 0, theButton.getWidth()*1.5, 5);  // Triangle
  popMatrix();
    
    theApplet.pushMatrix();
       
    if (theButton.isInside()) {
      if (theButton.isMousePressed()) { // button is pressed
      //  theApplet.
        fill(ControlP5.RED);
  
  move_element ();
       
      } else { // mouse hovers the button
      //  theApplet.
        fill(ControlP5.YELLOW);
      }
    } else { // the mouse is located outside the button area
     // theApplet.
     // fill(ControlP5.BLACK);
     
      if (elementData.get(theButton.getId()).selected == true) { theApplet.fill(ControlP5.BLUE); //elementData.get(idElement).selected= false;
    }
      else { theApplet.fill(ControlP5.BLACK); //elementData.get(idElement).selected= true;
       if (elementData.get(theButton.getId()).getStateButton() == true) { theApplet.fill(170, 170, 170);  } // GREY
    }
    }
    theApplet.strokeWeight(3);
     //theApplet.fill(0);
      theApplet.stroke(150, 150, 150);
       //  theApplet.fill(255,128,0);
       if (elementData.get(theButton.getId()).feedback_ == true)     theApplet.fill(0, 255, 0); //// FEEDBACK
  if (elementData.get(theButton.getId()).User_byte_10 > 1 ) theApplet.strokeWeight(0); else  theApplet.strokeWeight(3);
  
   if (elementData.get(theButton.getId()).User_byte_12 == 0)
    theApplet.ellipse(0, 0, theButton.getWidth(), theButton.getWidth());
    
     masks( elementData.get(theButton.getId()).User_byte_10, // quale sprite
   (int)(0- theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/18))+ theButton.getWidth()/24,
   (int)(0- theButton.getHeight()* (( (float)elementData.get(theButton.getId()).User_byte_11)/18))+ theButton.getHeight()/24,    // posizione x y
   (int)(theButton.getWidth()+theButton.getWidth()* (( (float)elementData.get(theButton.getId()).User_byte_11)/9))- theButton.getWidth()/12, 
   (int)(theButton.getHeight()+theButton.getHeight()* (( (float)elementData.get(theButton.getId()).User_byte_11)/9))- theButton.getHeight()/12);  // grandezza x y
  
     if (elementData.get(theButton.getId()).User_byte_12 == 1)
    theApplet.ellipse(0, 0, theButton.getWidth(), theButton.getWidth());
    
      noFill();
  theApplet.strokeWeight(map(elementData.get(theButton.getId()).radiusW/30,1,5,5,9));
  theApplet.stroke(255, 128, 0);
  //line(theButton.getWidth()*0.1, theButton.getHeight()*1, theButton.getWidth()*0.1, theButton.getHeight()*1-(theButton.getValue()*theButton.getHeight()/127));
 arc(0, 0, theButton.getWidth(), theButton.getWidth(), theButton.getStartAngle(), theButton.getAngle());

  theApplet.strokeWeight(3);
   theApplet.stroke(150, 150, 150);
    
    
    int x = theButton.getWidth()/2 - 1 - theButton.getCaptionLabel().getWidth()/2;
    int y = theButton.getWidth()/2 - 2 -theButton.getCaptionLabel().getHeight()/2;
    theApplet.pushMatrix();
    translate(x, y);
   // theButton.getCaptionLabel().draw(theApplet);
     if (infoGraph==1006)
 {
   theButton.setCaptionLabel(Integer.toString(elementData.get(theButton.getId()).memoryPosition));
 theButton.getCaptionLabel().draw(theApplet);
   }
 else 
 {
    theButton.setCaptionLabel(elementData.get(theButton.getId()).label);
    theButton.getCaptionLabel().draw(theApplet);
 }
    theApplet.popMatrix();
    theApplet.popMatrix();
  }
}


//------------------------------------------------------------------------------------------------------------------
public class NumberboxInput {
  String text = "";
  Numberbox n;
  boolean active;
  NumberboxInput(Numberbox theNumberbox) {
    n = theNumberbox;
    registerMethod("keyEvent", this );
  }
  public void keyEvent(KeyEvent k) {
    // only process key event if input is active 
    if (k.getAction()==KeyEvent.PRESS && active) {
      if (k.getKey()=='\n') { // confirm input with enter
        submit();
        return;
      } else if (k.getKeyCode()==BACKSPACE) { 
        text = text.isEmpty() ? "":text.substring(0, text.length()-1);
        //text = ""; // clear all text with backspace
      } else if (k.getKey()<255) {
        // check if the input is a valid (decimal) number
        // final String regex = "\\d+([.]\\d{0,2})?";
        final String regex = "\\d+";
        String s = text + k.getKey();
        if ( java.util.regex.Pattern.matches(regex, s ) ) {
          text += k.getKey();
        }
      }
      n.getValueLabel().setText(this.text);
    }
  }
  public void setActive(boolean b) {
    active = b;
    if (active) {
      n.getValueLabel().setText("");
      text = "";
    }
  }
  public void submit() {
    if (!text.isEmpty()) {
      n.setValue( int( text ) );
      text = "";
    } else {
      n.getValueLabel().setText(""+n.getValue());
    }
  }
}

//------------------------------------------------------------------------------------------------------------------
void move_element () 
{
     if (keyPressed == true) {
    if (keyCode == SHIFT && edit_mode == false) {
      Knob t = (Knob)cp5.get(Integer.toString(idElement+1));
  int mouse_X = (mouseX/15)*15;
  int mouse_Y = (mouseY/15)*15;
 // mouse_X = mouse_X*50;
   t.setPosition(constrain(mouse_X,0,(width/3)*2), constrain(mouse_Y,height/3, height));
   elementData.get(idElement).posX = constrain(mouse_X,0,(width/3)*2);
    elementData.get(idElement).posY = constrain(mouse_Y,height/3, height); 
  // keyCode = 0 ;
    }
     }
}

//------------------------------------------------------------------------------------------------------------------
void polygon(float x, float y, float radius, int npoints) {
  float angle = TWO_PI / npoints;
  beginShape();
  for (float a = 0; a < TWO_PI; a += angle) {
    float sx = x + cos(a) * radius;
    float sy = y + sin(a) * radius;
    vertex(sx, sy);
  }
  endShape(CLOSE);
}

void masks (int numero,int xpos, int ypos, int width_, int height_)
{

    switch ( numero ) {
        
         case 2 :
        {image(square1, 
          xpos, ypos,  // posizione di partenza
         width_, height_);}
        break;
      
          case 3 :
        {image(square2, 
         xpos, ypos,  // posizione di partenza
         width_, height_
       );}
        break;
        
         case 4 :
        {image(square3, 
         xpos, ypos,  // posizione di partenza
         width_, height_
       );}
        break;
        
         case 5 :
        {image(square4, 
         xpos, ypos,  // posizione di partenza
         width_, height_
       );}
        break;
        
         case 6 :
        {image(square5, 
         xpos, ypos,  // posizione di partenza
         width_, height_
       );}
        break;
        
         case 7 :
        {image(circle1,   
         xpos, ypos,  // posizione di partenza
         width_, height_
       );}
        break;
        
         case 8 :
        {image(circle2,     
         xpos, ypos,  // posizione di partenza
         width_, height_
       );}
        break;
        
         case 9 :
        {image(circle3,    
         xpos, ypos,  // posizione di partenza
         width_, height_
       );}
        break;
        
         case 10 :
        {image(circle4,     
         xpos, ypos,  // posizione di partenza
         width_, height_
       );}
        break;
        
         case 11 :
        {image(circle5,  
         xpos, ypos,  // posizione di partenza
         width_, height_
       );}
        break;
        
           case 12 :
        {image(mask1,  
         xpos, ypos,  // posizione di partenza
         width_, height_
       );}
        break;
        
             case 13 :
        {image(mask2, 
         xpos, ypos,  // posizione di partenza
         width_, height_
       );}
        break;
        
               case 14 :
        {image(mask3, 
         xpos, ypos,  // posizione di partenza
         width_, height_
       );}
        break;
        
        
               case 15 :
        {image(mask4,     
         xpos, ypos,  // posizione di partenza
         width_, height_
       );}
        break;
        
        
               case 16 :
        {image(mask5,    
         xpos, ypos,  // posizione di partenza
         width_, height_
       );}
        break;
       
        
        
        } // fie switch/case

}
