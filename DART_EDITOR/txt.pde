void explanationText() {   if (change_do == true)
  if (infoGraph<65) {
    // generic
 //   text("HiNT BOX \n" + "Each Modifier input on the DartMobo circuit can be configured for Pots or Buttons. \n"+ "Set Mode to POT or HYPERCURVE to manage pot-like modifiers or sensors. \n" 
 //   + "Set Mode to BUTTON or TOGGLE to manage on-off modifiers like buttons or switches.", 
 //    gridCols[1]+spaceBetw/9, gridRow[3]+rowBetw*0.4);
 //   
// elementData.get(idElement).hintarea.setText("HiNT BOX \n" + "Each Modifier input on the DartMobo circuit can be configured for Pots or Buttons. \n"+ "Set Mode to POT or HYPERCURVE to manage pot-like modifiers or sensors. \n" 
  //  + "Set Mode to BUTTON or TOGGLE to manage on-off modifiers like buttons or switches.");
    
 //   elementData.get(idElement).hintarea.setText(elementData.get(infoGraph).hint_message); 
   hint_message_send (infoGraph, idElement);
 } 
  
   else if  (infoGraph==2000)            // MEMORYPOSITION
   {  hints2.setColor(190);
   hints2.setText("MEMORY POSITION: \n The position (from 0 to 64) where the current item's is stored, in the EEPROM memory. \n The memory-position is directly related to the CIRCUIT-POSITION, wich is the number of the input pin to wich the modifier (pot/button/sensor) is connected. \n"+
   " Example : a button connected to the input CIRCUIT POSITION 26 will control a MIDI, DMX or HID signal, defined and stored on the MEMORY POSITION 26 \n"+
   " A Memory-position number can NOT be shared between more modifiers. When a number that is already used is selected, it will be reset to 0. The modifiers with Memory-position 0 are not sent out to the DART unit for setup.");
 
  /// if ( elementData.get(idElement).toggleMode < 11 )
hide_all ();

 // image(circuit_position, 10, 400, 1000, 600); delay (10);
 }
  else if (infoGraph==2002) {    hints2.setColor(190);
  //  elementData.get(idElement).hintarea.setText("VALUE of the MIDI message. \n it's the FIRST DATA BYTE of the midi message. \n In a common MIDI NOTE message this value defines the PITCH of the note.");
  hints2.setText(VALUE_Strings[hint_strings_map[(elementData.get(idElement).toggleMode)]-1]);
  } 
   else if (infoGraph==2001) {    hints2.setColor(190);
   hints2.setText("Midi event TYPE:  \n Note, Poly-AT, Control-Change, Program-Change, Channel-AT and Pitch-Bend messages.");
 
  }
  
  else if (infoGraph==2003) {  hints2.setColor(190);
  //  text("HiNT BOX: \n"+  "Value of the note", gridCols[1]+spaceBetw/9, gridRow[3]+rowBetw*0.4);
   //  elementData.get(idElement).hintarea.setText("MINIMUM Value \n sets the lower limit of the second DATA BYTE of the midi message.");
   
    hints2.setText(MIN_Strings[hint_strings_map[(elementData.get(idElement).toggleMode)]-1]);
  } 
  else if (infoGraph==2011) {  hints2.setColor(190);
  //  text("HiNT BOX: \n"+  "Value of the note", gridCols[1]+spaceBetw/9, gridRow[3]+rowBetw*0.4);
    hints2.setText("hint box itself");
  }
   else if (infoGraph==2012) { hints2.setColor(190);
  //  text("HiNT BOX: \n"+  "Value of the note", gridCols[1]+spaceBetw/9, gridRow[3]+rowBetw*0.4);
     hints2.setText("HINTs - suggestions to users \n It is possible to write a note that explains what a modifier does. \n Use '_' ASCII symbol to separate lines.");
  }
  
   else if (infoGraph==2013) {  hints2.setColor(190);
  //  text("HiNT BOX: \n"+  "Value of the note", gridCols[1]+spaceBetw/9, gridRow[3]+rowBetw*0.4);
    hints2.setText("name of the modifier");
  }
  
  
     else if (infoGraph==2014)   /// LED
     {  hints2.setColor(190);
  //  text("HiNT BOX: \n"+  "Value of the note", gridCols[1]+spaceBetw/9, gridRow[3]+rowBetw*0.4);
//    hints2.setText("LED OUTPUT \n This value defines wich digital output will be controlled by the current modifier. \n choose a value that is outside the 74HC595 chain (higher than 31) to have NO LED output, and avoid conflicts. ");
  
   hints2.setText(LED_Strings[hint_strings_map[(elementData.get(idElement).toggleMode)]-1]);
     
     view_circuit();
    if (view_ciruit_bool == true) hide_all ();
// image(circuit_position, 10, 400, 1000, 600); delay (10);
  }
  
  else if (infoGraph==2004) {  hints2.setColor(190);                                    // MAXVALUE
 //   text("HiNT BOX: \n"+  "Max Value", gridCols[1]+spaceBetw/9, gridRow[3]+rowBetw*0.4);
    // elementData.get(idElement).hintarea.setText("MAXIMUM Value \n sets the upper limit of the second DATA BYTE of the midi message. \n  In a common MIDI NOTE message this value defines the VELOCITY of the note-ON message.");
  hints2.setText(MAX_Strings[hint_strings_map[(elementData.get(idElement).toggleMode)]-1]);
  
   view_circuit();
    if (view_ciruit_bool == true) hide_all ();

} 
  
  
  else if (infoGraph==2005) { hints2.setColor(190);
    //text("HiNT BOX: \n"+ "Midi Channel", gridCols[1]+spaceBetw/9, gridRow[3]+rowBetw*0.4);
   hints2.setText("MIDI Channel");
  } else if (infoGraph==2006) {  hints2.setColor(190);   // DMX

   hints2.setText(DMX_Strings[hint_strings_map[(elementData.get(idElement).toggleMode)]-1]);
      
        view_circuit();
    if (view_ciruit_bool == true) hide_all ();
     
  } else if (infoGraph==2007) {  hints2.setColor(190);
  
   if (elementData.get(idElement).toggleMode == 0)
     hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "BLIND INPUT: the input is not read.  \n_\n"  //0
      ); 
      else 
       if (elementData.get(idElement).toggleMode == 1)
     hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "BUTTON: momentary on-off output for pushbuttons.  \n commonly used to send midi on/off messages,\nit can be assigned to a HID message to emulate the keys of a common computer keyboard. \na DMX channel can be defined to be controlled by pressing the button \n(the min and max values ​​adapted to the 0-255 range typical of dmx)\n--------\n" //0
      ); 
      else
        if (elementData.get(idElement).toggleMode == 2)
     hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "TOGGLE: Toggle mode.\n_\n" //0
      ); 
      else 
      if (elementData.get(idElement).toggleMode < 7)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "TOGGLE GROUPS: Only one can be ON at a time in a group.\n"
      ); 
      else 
            if (elementData.get(idElement).toggleMode < 11)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "RADIO GROUPS: a button can be turned off only by another button of the same group.\n_\n"
      ); 
      else 
             if (elementData.get(idElement).toggleMode == 11)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "POT: Continuous output, for potentiometers and sensors. \ncommonly used to send MIDI CC (Control Change) messages.\nit is possible to assign two HID messages to a pot, in order to emulate the keys of a computer keyboard.\n it is also possible to define a DMX channel to be controlled by the pot \n(the min max range is adapted to the 0-255 range of the dmx). \nthe value assigned to LED activates or deactivates the light effects."
      ); 
      else 
              if (elementData.get(idElement).toggleMode < 14)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
       "HYPERCURVE 1-2: the maximum value is reached in a reduced range \n(from top to mid in hypercurve-1 / from bottom to mid in hypercurve-2) \n_\n"
      ); 
      else 
               if (elementData.get(idElement).toggleMode < 16)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "CENTER-CURVE 1-2: the center value (64) has a larger flat range.\n_\n"
      ); 
      else 
      if (elementData.get(idElement).toggleMode == 16)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "SEQ mode for push buttons only -  \n produces a sequence of midi signals where A is the size of the increment  \n if B is > 0, the sequence alternates the two fixed values A B \n the sequence can extend up to four fixed values ​​with C > 0 and D > 0"
      ); 
      else 
       if (elementData.get(idElement).toggleMode == 17)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "PAGE SWITCH: switch between page1 and page2 presets. \n_ The page-switch itself can control a MIDI message. \n_\n"
      ); 
      else 
      if (elementData.get(idElement).toggleMode == 18)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "DISTANCE SENSOR: It can be set to work as a normal POT or as a ON-OFF virtual button \n_\n"
      ); 
      else 
        if (elementData.get(idElement).toggleMode == 19)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "ENCODERS: rotary encoders can work in relative or pot emulation mode"
      ); 
      else 
        if (elementData.get(idElement).toggleMode == 20)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "PADS: settings for piezo-sensors inputs. the selected value is used for the first Pad-input, all other inputs are set to the following pitch value \nThe input pins dedicated to reading the piezoelectric pads are 45 46 47 and 48, regardless of the memoryposition number.\nnote: the pads must be activated by correctly setting the GENERAL item, you must also pay attention to the inputs 41 42 43 44, if used, because they will need external 100k pullup resistors."
      ); 
      else 
          if (elementData.get(idElement).toggleMode <23)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "SPINNERs: high definition encoders with special interactions with touch sensors. \n_\n"
      ); 
      else 
         if (elementData.get(idElement).toggleMode <25)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "TOUCH SENSORs: Midi values, sensitivity, touch-reset and virtual-touch functions.\n_\n"
      ); 
      else 
          if (elementData.get(idElement).toggleMode == 25)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "MOUSE: Mouse and mousewheel emulation setting. .\n_\n"
      ); 
      else
             if (elementData.get(idElement).toggleMode == 26)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
       "GENERAL: ciruit settings of the entire machine. \nA GENERAL item must be present within a preset, otherwise the controller will work in auto-detect mode!!\nIt can be assigned to any MEMORYPOSITION and the data it contains is the same for page1 and page2\n" 
      ); 
      else 
            if (elementData.get(idElement).toggleMode == 27)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
       "velocity sensitive PADS - created using force sensors" 
      ); 
      else 
        if (elementData.get(idElement).toggleMode == 28)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
       "SPRITE - Draw and memorize a symbol that can be assigned to a 8x8 led-matrix-pad" 
      ); 
      else 
         if (elementData.get(idElement).toggleMode == 29)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
       "ENCODER RESET function - for pushbuttons only \n TARGET is the memoryposition of the encoder that will be reset \n RESET VALUE 0-127 is the value to wich target encoder will be reset, by pushing the button." 
      ); 
      else 
            if (elementData.get(idElement).toggleMode == 30)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
       " SHIFTER function for pushbuttons \n select the TARGET position of the modfier you want to be affected by the shifter button, \n so once pushed the midi Data Byte 1 will e shifted by 1" 
      ); 
      else 
            if (elementData.get(idElement).toggleMode < 35)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
       "USER functions " 
      ); 
     println(elementData.get(idElement).toggleMode);
      
     // control2.hints.setText
     /*"Hypercurve 1",





"BLIND INPUT",  //19
"PADS",  //20
"SPINNER1 SET.",  //21
"SPINNER2 SET.", // 22
"touch 1", // 23
"touch 2", // 24
"MOUSE/ARROWS",  //25
"GENERAL" }; // 26
*/

/*
switch(elementData.get(idElement).toggleMode)
{ case 0:
 //if (elementData.get(idElement).toggleMode == 0)
     hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "BLIND INPUT: the input is not read.  \n_\n"  //0
      ); 
      break;
   //   else 
    //   if (elementData.get(idElement).toggleMode == 1)
     hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "BUTTON: on-off output.  \n_\nTOGGLE: Toggle mode.\n_\n" //0
      ); 
   //   else
    //    if (elementData.get(idElement).toggleMode == 2)
     hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "TOGGLE: Toggle mode.\n_\n" //0
      ); 
   //   else 
   //   if (elementData.get(idElement).toggleMode < 7)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "TOGGLE GROUPS: Only one can be ON at a time in a group.\n_\n"
      ); 
    //  else 
    //        if (elementData.get(idElement).toggleMode < 11)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "RADIO GROUPS: a button can be turned off only by another button of the same group.\n_\n"
      ); 
   //   else 
     //        if (elementData.get(idElement).toggleMode == 11)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "POT: Continuous output, for potentiometers and sensors. \n_\n"
      ); 
    //  else 
    //          if (elementData.get(idElement).toggleMode < 14)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
       "HYPERCURVE 1-2: the maximum value is reached in a reduced range \n(from top to mid in hypercurve-1 / from bottom to mid in hypercurve-2) \n_\n"
      ); 
    //  else 
     //          if (elementData.get(idElement).toggleMode < 16)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "CENTER-CURVE 1-2: the center value (64) has a larger flat range.\n_\n"
      ); 
     // else 
    //  if (elementData.get(idElement).toggleMode == 16)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "SEQ mode for push buttons - \n_\n"
      ); 
     // else 
     //  if (elementData.get(idElement).toggleMode == 17)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "PAGE SWITCH: switch between page1 and page2 presets. the page-switch itself can control a MIDI message. \n_\n"
      ); 
    //////  else 
    //  if (elementData.get(idElement).toggleMode == 18)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "DISTANCE SENSOR: It can be set to work as a normal POT or as a ON-OFF virtual button \n_\n"
      ); 
    //  else 
    //    if (elementData.get(idElement).toggleMode == 19)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "ENCODERS: rotary encoders can work in relative or pot emulation mode"
      ); 
   //   else 
    //    if (elementData.get(idElement).toggleMode == 20)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "PADS: settings for piezo-sensors inputs. the selected value is used for the first Pad-input, all other inputs are set to the following pitch value \n_\n"
      ); 
    //  else 
     //     if (elementData.get(idElement).toggleMode <23)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "SPINNERs: high definition encoders with special interactions with touch sensors. \n_\n"
      ); 
    //  else 
     //    if (elementData.get(idElement).toggleMode <25)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "TOUCH SENSORs: Midi values, sensitivity, touch-reset and virtual-touch functions.\n_\n"
      ); 
     // else 
      //    if (elementData.get(idElement).toggleMode == 25)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
      "MOUSE: Mouse and mousewheel emulation setting. .\n_\n"
      ); 
      //       if (elementData.get(idElement).toggleMode == 26)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
       "GENERAL: ciruit settings of the entire machine. A GENERAL modifier must alwais be present in a preset, otherwise the controller will work in auto-detect mode!!\n_\n" 
      ); 
    //  else 
      //      if (elementData.get(idElement).toggleMode == 27)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
       "MATRIX PADS" 
      ); 
     // else 
     //   if (elementData.get(idElement).toggleMode == 28)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
       "SPRITE" 
      ); 
    //  else 
     //    if (elementData.get(idElement).toggleMode == 29)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
       "RESET function" 
      ); 
    //  else 
     //       if (elementData.get(idElement).toggleMode == 30)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
       "SHIFTER function" 
      ); 
    //  else 
     //       if (elementData.get(idElement).toggleMode < 35)
      hints2.setText("MODE - how a modifier works \n---------------------------------\n"+ 
       "USER functions " 
      ); 

*/
   //  hide_all ();
    // hints2.setSize((int) gridCols[15]+Betw2,(int) gridRow[22]);
      
  } 
  /*
  else if (infoGraph==2008) {  hints2.setColor(190);   // MODIFIERS
     hints2.setText(  "MODIFIERS - CTRL , ALT , SHIFT\n"+ 
      "if QWERTY keys emulation is active, it's possible to add MODIFIERS and combinations of them.\n"+
      "So, it's possible to emulate complex QWERTY keystrokes on a single button.");
   } 
   */  
      

  
  else if (infoGraph==2009) {  hints2.setColor(190);
       hints2.setText(  "SPECIAL KEYS - SHIFT CTRL ALT\n"+ 
      "if QWERTY keys emulation is active, it's possible to add SPECIAL KEYS and combinations of them.\n"+
      "So, it's possible to emulate complex QWERTY keystrokes on a single button.\n-----\nin POT mode, you can specify here the key to associate with the DOWN position of the potentiometer" );
    
  //  text("HiNT BOX: \n"+  "MODIFIERS\n"+ 
   //   "if QWERTY keys emulation is active, it's possible to add MODIFIERS and combinations of them.\n"+
    //  "So, it's possible to emulate complex QWERTY keystrokes on a single button.", gridCols[1]+spaceBetw/9, gridRow[3]+rowBetw*0.4);
  } else if (infoGraph==2010) {  //elementData.get(idElement).hintarea.setColor(190);
   hints2.setColor(190);
   //  elementData.get(idElement).hintarea.setText("HiNT BOX: \n"+  "KEYS: QWERTY keyboard emulation.\n"+
    //  "list of ASCII keys.\n"+
     // "MIDI data will not be output in QWERTY key mode, choose NULL to have midi output." );
      
       hints2.setText(KEY_Strings[hint_strings_map[(elementData.get(idElement).toggleMode)]-1]);
     // text("HiNT BOX: \n"+  "KEYS: QWERTY keyboard emulation.\n"+
    //  "list of ASCII keys.\n"+
    //  "MIDI data will not be output in QWERTY key mode, choose NULL to have midi output.", gridCols[1]+spaceBetw/9, gridRow[3]+rowBetw*0.4);
  }
  else if (infoGraph==1003) {  hints2.setColor(190);
   hints2.setText("SEND ALL \n - \n The complete preset of both two pages will be sent to the DartMobo controller. ");
   // text("HiNT BOX: \n"+  "name"
     // , gridCols[1]+spaceBetw/9, gridRow[3]+rowBetw*0.4);
  }
  else if (infoGraph==1004) {  hints2.setColor(190);
   hints2.setText("SAVE \n - \n Presets are saved in a .csv (comma separated values) files that can be edited with a common notepad-app. \n A preset contains : \n *all infos on the graphic layout configuration (modifier's type, dimension, name, notes) \n *all infos regarding the MIDI, DMX, HID messages to be configured and sent to the DartMobo controller unit.");
   // text("HiNT BOX: \n"+  "name"
     // , gridCols[1]+spaceBetw/9, gridRow[3]+rowBetw*0.4);
  }
   else if (infoGraph==1005) {  hints2.setColor(190);
   hints2.setText("LOAD \n - \n Presets are saved in a .csv (comma separated values) files that can be edited with a common notepad-app. \n A preset contains : \n *all infos on the graphic layout configuration (modifier's type, dimension, name, notes) \n *all infos regarding the MIDI, DMX, HID messages to be configured and sent to the DartMobo controller unit.");
   // text("HiNT BOX: \n"+  "name"
     // , gridCols[1]+spaceBetw/9, gridRow[3]+rowBetw*0.4);
  }
  else if (infoGraph==1006) {  hints2.setColor(190);
   hints2.setText("DRAW MODE: to modify the visual components of a preset.\n SHIFT + Mouse-left-click and drag = move a modifier, within the Modifiers Area. \n SHIFT + MouseWheel = change modifier size \n CTRL + Mouse Wheel = change modifier shape \n 'w' or 'h' + MouseWheel = change modifier width or height \n 'm' or 'n' or 'b' + mousewheel = change MASK type or size or layer. \n CTRL + '-' or CTRL + '8' = hide current modifier. \n CTRL + '0' = hide all modifiers. \n CTRL + '+' or CTRL + '9'= Add a modifier ( up to 64 modifiers can be addedd) \nby placing the mouse pointer on DRAW, the memory position number will be displayed on each item ");
   // text("HiNT BOX: \n"+  "name"
     // , gridCols[1]+spaceBetw/9, gridRow[3]+rowBetw*0.4);
  }
   else if (infoGraph==1001) {  hints2.setColor(190);
   hints2.setText("SELECT PAGE 1 \n - \n A DartMobo based controller can store two presets (pages).");
   // text("HiNT BOX: \n"+  "name"
     // , gridCols[1]+spaceBetw/9, gridRow[3]+rowBetw*0.4);
  }
   else if (infoGraph==1002) {  hints2.setColor(190);
   hints2.setText("SELECT PAGE 2 \n - \n A DartMobo based controller can store two presets (pages).");
   // text("HiNT BOX: \n"+  "name"
     // , gridCols[1]+spaceBetw/9, gridRow[3]+rowBetw*0.4);
  }
  else if (infoGraph==1000) {  hints2.setColor(190);
   hints2.setText("EXIT \n Quit Dart_editor without saving");
   // text("HiNT BOX: \n"+  "name"
     // , gridCols[1]+spaceBetw/9, gridRow[3]+rowBetw*0.4);
  }
  
  else if (infoGraph==1007) {  hints2.setColor(190);
   hints2.setText("SEND single element \n - \n The config of the current modifier is sent to DartMobo controller on the specified page.");
   // text("HiNT BOX: \n"+  "name"
     // , gridCols[1]+spaceBetw/9, gridRow[3]+rowBetw*0.4);
  }
   else if (infoGraph==1008) {  hints2.setColor(190);
   hints2.setText("Enable / disable buzzer sound");
   // text("HiNT BOX: \n"+  "name"
     // , gridCols[1]+spaceBetw/9, gridRow[3]+rowBetw*0.4);
  }
  
    else if (infoGraph==2020) {  hints2.setColor(190);
 //  hints2.setText("");
   hints2.setText("Hint box:  \n"+
   custom_intro_string+"\n"+
   version+"\n"+
   "/////      www.DARTMOBO.com      \n");
  //  immagine_circuito = true;
   // text("HiNT BOX: \n"+  "name"
     // , gridCols[1]+spaceBetw/9, gridRow[3]+rowBetw*0.4);
  }
     else if (infoGraph==2021) {
        // fill(255,0,0);
        hints2.setColor(color(255,150,0));
   hints2.setText("ITEM ALREADY ACTIVATED!!"); delay(300);
   
 //  fill(140);
  //  immagine_circuito = true;
   // text("HiNT BOX: \n"+  "name"
     // , gridCols[1]+spaceBetw/9, gridRow[3]+rowBetw*0.4);
  }
      else if (infoGraph==2015) {  hints2.setColor(190);
   hints2.setText("CHANNEL FILTER \n Show incoming MIDI data only on selected channel 0-16 \n If set ot OFF (0) - show all channels.");
  //  immagine_circuito = true;
   // text("HiNT BOX: \n"+  "name"
     // , gridCols[1]+spaceBetw/9, gridRow[3]+rowBetw*0.4);
  }
  
        else if (infoGraph==2016) {  hints2.setColor(190);
   hints2.setText("VALUE FILTER \n Show incoming MIDI data of the selected value. \n All other messages are fitered \n If set ot OFF (-1) - all messages are shown.");
  //  immagine_circuito = true;
   // text("HiNT BOX: \n"+  "name"
     // , gridCols[1]+spaceBetw/9, gridRow[3]+rowBetw*0.4);
  }
          else if (infoGraph==2017) {  hints2.setColor(190);
   hints2.setText("enable / disable monitoring on incoming MIDI data");
  //  immagine_circuito = true;
   // text("HiNT BOX: \n"+  "name"
     // , gridCols[1]+spaceBetw/9, gridRow[3]+rowBetw*0.4);
  }
         else if (infoGraph==2018) {  hints2.setColor(190);
   hints2.setText("enable / disable monitoring on outgoing MIDI data");
  //  immagine_circuito = true;
   // text("HiNT BOX: \n"+  "name"
     // , gridCols[1]+spaceBetw/9, gridRow[3]+rowBetw*0.4);
  }
  
  
}
