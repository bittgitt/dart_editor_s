  
  void loadTableRow(TableRow t) { // viene ripetuta per ogni item al'interno di un FOR
  
  //  labeler();
       hide = t.getInt("Hide");   
   
   if (hide == 0 ) // se NON è in hide - nota: 1 = item nascosto.
   { toggleMode = t.getInt("Toggle");
    toggleMode_2nd = t.getInt("Toggle_2nd");
    if (toggleMode == 26 || toggleMode== 17)  { plugga_page(); }
   }
    else 
    { toggleMode = 0;
    toggleMode_2nd = 0;}
    
     if (toggleMode == 28) {splugga();} // 28 = sprite
     
    posX = int ( (width/ ex_dimensionX   )* t.getInt("posX"));
    posY = int ( ( height / ex_dimensionY )* t.getInt("posY"));   
    shape = t.getInt("shape");
    radiusW = int (( width/ ex_dimensionX   )*t.getInt("dimensionX"));
    radiusH = int (( height / ex_dimensionY )*t.getInt("dimensionY"));
   
    dMode.setValue(toggleMode);
    labeler();
     
    memoryPosition = t.getInt("MemoryPosition");
    label = t.getString("Name");    
    indexMidiType = t.getInt("midiType"); 
    midiTypeOpt.setValue(indexMidiType);
    indexMidiType_2nd = t.getInt("midiType_2nd");
    note[0]= t.getInt("Note");
    note[1]= t.getInt("Note_2nd");
    
    setExcursionControllMax= t.getInt("MaximumValue");
    setExcursionControllMax_2nd= t.getInt("MaximumValue_2nd");
    setExcursionControllMin =t.getInt("MinimumValue");
    
    setExcursionControllMin_2nd =t.getInt("MinimumValue_2nd");
   

    
    midiChannel = t.getInt("MidiChannel");
    midiChannel_2nd = t.getInt("MidiChannel_2nd"); 
    keyBoard = t.getInt("Keys");
    keyBoard_2nd = t.getInt("Keys_2nd");
    modifiers = t.getInt("Modifiers");
    modifiers_2nd = t.getInt("Modifiers_2nd");
    hint_message = t.getString("hint_message");    
        
    led_ = t.getInt("led");  
    
    addressDMX = t.getInt("DMXAddress");
    addressDMX_2nd = t.getInt("DMXAddress_2nd");
    
        User_byte_1 = t.getInt("Page");
        User_byte_2 = t.getInt("labeler");
        
      //7  print("--column count--");
      ///  println(table.getColumnCount()) ; 
        
        
 if (table.getColumnCount() > 30)  {     // voglio contare quante colonne ci sono - per evitare che l'editor non sia retrocompatibile con versioni 
                                        // che non caricano l'USER_BYTE_3 4 etc etc
    User_byte_3 = t.getInt("User_byte_3");
    User_byte_4 = t.getInt("User_byte_4");
    User_byte_5 = t.getInt("User_byte_5");
    User_byte_6 = t.getInt("User_byte_6");
    User_byte_7 = t.getInt("User_byte_7");
    User_byte_8 = t.getInt("User_byte_8");
    User_byte_9 = t.getInt("User_byte_9");
    User_byte_10 = t.getInt("User_byte_10");
    User_byte_11 = t.getInt("User_byte_11");
    User_byte_12 = t.getInt("User_byte_12");
  //  User_byte_14 = t.getInt("User_byte_14");
 }
     if (table.getColumnCount() > 40)  {
        User_byte_14 = t.getInt("User_byte_14");
     //   led_2nd = t.getInt("led_2nd"); 
    } 
    if (table.getColumnCount() > 41)  {
       // User_byte_14 = t.getInt("User_byte_14");
        led_2nd = t.getInt("led_2nd"); 
    }
      
    //   labeler();
    /*
       mp.setRange(-255,255);
    mp.setValue(memoryPosition);   
    
       dKeys.setLabel(keyboardList[keyBoard_2nd]);
    dKeys.setValue(keyBoard);  
    
       dModif.setLabel(keyboardList[modifiers]);
    dModif.setValue(modifiers);  
    nT.setValue(note[0]); 
    
    min.setRange(-255,255);
    min.setValue(setExcursionControllMin);
    
    max.setRange(-255,255);
    max.setValue(setExcursionControllMax);
    
    midiC.setValue(midiChannel);
    
      dmx.setRange(-255,255);
    dmx.setValue(addressDMX);
    
     led.setRange(-255,255);
    led.setValue(led_);
    */
    
    /*
      mp.setRange(-255,255);
      dKeys.setLabel(keyboardList[keyBoard_2nd]);
     dModif.setLabel(keyboardList[modifiers]);
     min.setRange(-255,255);
     max.setRange(-255,255);
     dmx.setRange(-255,255);
     led.setRange(-255,255);
     
     */
     
   //  min.setRange(-32,32);
     
   // labeler();
    
    mp.setValue(memoryPosition);        
    dKeys.setValue(keyBoard);     
    dModif.setValue(modifiers);  
    nT.setValue(note[0]); 
    //println(setExcursionControllMin);
    min.setValue(setExcursionControllMin);
    max.setValue(setExcursionControllMax);
    midiC.setValue(midiChannel);  
    dmx.setValue(addressDMX);
    led.setValue(led_);
    
   }
   
   /*
   void loadTable2 ()
   {
       mp.setValue(memoryPosition);        
    dKeys.setValue(keyBoard);     
    dModif.setValue(modifiers);  
    nT.setValue(note[0]); 
    min.setValue(setExcursionControllMin);
    max.setValue(setExcursionControllMax);
    midiC.setValue(midiChannel);  
    dmx.setValue(addressDMX);
    led.setValue(led_);
   
   }
   */
   
}
  
  
  
  
  
  
  
  
  
  
