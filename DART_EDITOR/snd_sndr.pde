 
 byte delay_send = 30;
 
 void sender_single (int i)
        {
          
        {
       if (page_selected == false  ){
// 0   
       if ( elementData.get(i).toggleMode == 28) // se è un item SPRITE - mandiamo un pitch-bend che conterrà una riga verticale (8 bit divisi in due byte)
       myBus.sendMessage(224, elementData.get(i).User_byte_4, elementData.get(i).User_byte_5);
         
// 1
        myBus.sendMessage(144, elementData.get(i).note[0], elementData.get(i).memoryPosition-1);  // note pitch  - memoryposition
         
       //  print(binary(byte(elementData.get(i).note[0]))); println(" - note value");
// 2
        delay(delay_send);
       
        if (  elementData.get(i).modifiers == 0) // per modifier intendiamo i tasti shift alt ctrl
        {
                                      if (elementData.get(i).toggleMode == 21 || elementData.get(i).toggleMode == 22 || elementData.get(i).toggleMode == 19)  // encoders e spinners
                              {
                                       if (elementData.get(i).addressDMX > 1 && elementData.get(i).indexMidiType == 0)   //  pot mode + note // modalità scale
                                       /*
                                       nota: ciò che poi succede nello schetch per processare il valore contenuto in indexmiditype è questo: 
                                         // note: velocity = miditype proveniente dall'editor numerato da 0 a 6 - vedi tbl riga 19 (note = 0, cc= 2)
                                         viene moltiplicato per 16 e sistemato da 144 in poi a seconda del canale
                                         
                                         
                                         // type contiene ovviamente anche l'informazione del canale midi 0-16
     
                                       */
                                       
                                       
                                    {    myBus.sendMessage(176+elementData.get(i).midiChannel-1, 
                                      elementData.get(i).setExcursionControllMax, 
                                     // int(map(abs(elementData.get(i).setExcursionControllMin),0,32,32,0))+32
                                      
                                      int(
                                      (elementData.get(i).setExcursionControllMin)
                                      )+32
                                      ); // velocità scorrimento per quando suoniamo una scala
                                     
                                     /*
                                     print(
                                      elementData.get(i).setExcursionControllMin
                                       );
                                      println("min");
                                      print(  elementData.get(i).indexMidiType 
                                      );
                                           println("type");
                                           */
                                    }
                                    else 
                                    
                                    {
                                     myBus.sendMessage(176+elementData.get(i).midiChannel-1, 
                                      elementData.get(i).setExcursionControllMax, 
                                      elementData.get(i).setExcursionControllMin+32);   // normale range
                                     //  println(elementData.get(i).setExcursionControllMin+32);
                                    }
                                    
                                     } // speed range -32 +32 viene inviata come 0-64
          else 
          myBus.sendMessage(176+elementData.get(i).midiChannel-1, 
          elementData.get(i).setExcursionControllMax, elementData.get(i).setExcursionControllMin);  // mix - max // invio normale
        } 
        
        else {
          if ( elementData.get(i).toggleMode == 11)                            
            myBus.sendMessage(176+elementData.get(i).midiChannel-1, 
            elementData.get(i).setExcursionControllMax, int(keyboard_out(elementData.get(i).modifiers))    
           
            // l'informazione relativa ai modificatori (ctrl - shift - alt) 
            //viene allocata nella posizione normalmente usata per MINVALUE
          );
          else
          myBus.sendMessage(176+elementData.get(i).midiChannel-1, 
          elementData.get(i).setExcursionControllMax, elementData.get(i).modifiers
          );
        }
// 3
        delay(delay_send);
        myBus.sendMessage(176+elementData.get(i).midiChannel-1, int(elementData.get(i).toggleMode), elementData.get(i).addressDMX);
//4
        delay(delay_send);
        if ( elementData.get(i).toggleMode == 28) 
        {
        myBus.sendMessage(176+elementData.get(i).midiChannel-1, int(elementData.get(i).keyBoard), int ( elementData.get(i).indexMidiType )  );
        }
        
        else
        {
        myBus.sendMessage(176+elementData.get(i).midiChannel-1, int(keyboard_out(elementData.get(i).keyBoard)), int ( elementData.get(i).indexMidiType ));
       ///  print(binary(byte(elementData.get(i).indexMidiType))); println(" - type");
      }
//5
         delay(delay_send);
        myBus.sendMessage(176+elementData.get(i).midiChannel-1, int(elementData.get(i).led_), 0);

          if ( elementData.get(i).toggleMode == 28) {
// 6       
     
          delay(delay_send);
           myBus.sendMessage(176+elementData.get(i).midiChannel-1,
       //  myBus.sendMessage(144,
          int(elementData.get(i).User_byte_1), 
          int(elementData.get(i).User_byte_2)  
          ); 
// 7    
          delay(delay_send);
          myBus.sendMessage(176+elementData.get(i).midiChannel-1,
        // myBus.sendMessage(145,
          int(elementData.get(i).User_byte_3), 
         0
          ); 
          
          }
        
        
        delay(delay_send);
        
      }
     
      else // //////////////////////////////////////////////////////////////////7SECONDA PAGINA
      {
// 0   
       if ( elementData.get(i).toggleMode_2nd == 28) // se è un item SPRITE - mandiamo un pitch-bend che conterrà una riga verticale (8 bit divisi in due byte)
       myBus.sendMessage(224, elementData.get(i).User_byte_9, elementData.get(i).User_byte_14);
//1
       myBus.sendMessage(144, elementData.get(i).note[1], elementData.get(i).memoryPosition+63);
//2
        delay(delay_send);
        /*
        if (  elementData.get(i).modifiers_2nd == 0) 
        {
           if (elementData.get(i).toggleMode_2nd == 21 || elementData.get(i).toggleMode_2nd == 22 || elementData.get(i).toggleMode_2nd == 19) 
          myBus.sendMessage(176+elementData.get(i).midiChannel_2nd-1, elementData.get(i).setExcursionControllMax_2nd, elementData.get(i).setExcursionControllMin_2nd+32);
          else 
          myBus.sendMessage(176+elementData.get(i).midiChannel_2nd-1, elementData.get(i).setExcursionControllMax_2nd, elementData.get(i).setExcursionControllMin_2nd);
        
        } 
        else {
            if ( elementData.get(i).toggleMode_2nd == 11) 
          myBus.sendMessage(176+elementData.get(i).midiChannel_2nd-1, elementData.get(i).setExcursionControllMax_2nd,int(keyboard_out(elementData.get(i).modifiers) )
          );
          else
           myBus.sendMessage(176+elementData.get(i).midiChannel_2nd-1, elementData.get(i).setExcursionControllMax_2nd, elementData.get(i).modifiers_2nd);
        }
        */
        
         if (  elementData.get(i).modifiers_2nd == 0) 
        {
          if (elementData.get(i).toggleMode_2nd == 21 || elementData.get(i).toggleMode_2nd == 22 || elementData.get(i).toggleMode_2nd == 19)  // encoders e spinners
          {
         
            
           if (elementData.get(i).addressDMX_2nd > 1 && elementData.get(i).indexMidiType_2nd == 0)
        {    myBus.sendMessage(176+elementData.get(i).midiChannel_2nd-1, 
          elementData.get(i).setExcursionControllMax_2nd, 
         // int(map(abs(elementData.get(i).setExcursionControllMin_2nd),0,32,32,0))+32
                                   int(
                                      (elementData.get(i).setExcursionControllMin_2nd)
                                      )+32
         ); 
          
          // velocità scorrimento per quando suoniamo una scala
       //   println(int(map(abs(elementData.get(i).setExcursionControllMin),0,32,32,0))+32);
        // print(binary(byte(elementData.get(i).setExcursionControllMax_2nd))); // println(" - max");
        }
        else 
        
        
        {
         myBus.sendMessage(176+elementData.get(i).midiChannel_2nd-1, 
          elementData.get(i).setExcursionControllMax_2nd, 
          elementData.get(i).setExcursionControllMin_2nd+32);
         // println(elementData.get(i).setExcursionControllMin+32);
        }
        
      } // speed range -32 +32 viene inviata come 0-64
          else 
          myBus.sendMessage(176+elementData.get(i).midiChannel_2nd-1, 
          elementData.get(i).setExcursionControllMax_2nd, elementData.get(i).setExcursionControllMin_2nd);  // mix - max
        } 
        
        else {
          if ( elementData.get(i).toggleMode_2nd == 11)                            
            myBus.sendMessage(176+elementData.get(i).midiChannel_2nd-1, 
            elementData.get(i).setExcursionControllMax_2nd, int(keyboard_out(elementData.get(i).modifiers_2nd))    
           
            // l'informazione relativa ai modificatori (ctrl - shift - alt) 
            //viene allocata nella posizione normalmente usata per MINVALUE
          );
          else
          myBus.sendMessage(176+elementData.get(i).midiChannel_2nd-1, 
          elementData.get(i).setExcursionControllMax_2nd, elementData.get(i).modifiers_2nd
          );
        }
        
//3
        delay(delay_send);
        myBus.sendMessage(176+elementData.get(i).midiChannel_2nd-1, int(elementData.get(i).toggleMode_2nd), elementData.get(i).addressDMX_2nd);
//4
        delay(delay_send);
         if ( elementData.get(i).toggleMode_2nd == 28)
        {
        myBus.sendMessage(176+elementData.get(i).midiChannel-1, int(elementData.get(i).keyBoard_2nd), int ( elementData.get(i).indexMidiType_2nd )  );
        }
        else
        myBus.sendMessage(176+elementData.get(i).midiChannel_2nd-1, keyboard_out(int(elementData.get(i).keyBoard_2nd)), int ( elementData.get(i).indexMidiType_2nd ));
//5  
        delay(delay_send);
        myBus.sendMessage(176+elementData.get(i).midiChannel_2nd-1, int(elementData.get(i).led_2nd), 0);
               
                  
          if ( elementData.get(i).toggleMode == 28) {
// 6       
     
          delay(delay_send);
          myBus.sendMessage(176+elementData.get(i).midiChannel-1,
          int(elementData.get(i).User_byte_6), 
          int(elementData.get(i).User_byte_7)  
          ); 
// 7    
          delay(delay_send);
          myBus.sendMessage(176+elementData.get(i).midiChannel-1,
          int(elementData.get(i).User_byte_8), 
          0
          ); 
          }
        
        
        delay(delay_send);
        
      }
      
      }
        }
        
 //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
 

 
  void sender ()
 {
          
        i_sender ++;
       
    int i = i_sender-1;
 
    {
   
      {
     
       {
         
       
// 0   
          if ( elementData.get(i).toggleMode == 28) // se è un item SPRITE - mandiamo un pitch-bend che conterrà una riga verticale (8 bit divisi in due byte)
          myBus.sendMessage(224, elementData.get(i).User_byte_4, elementData.get(i).User_byte_5);
          
//1     - note / memoryposition
          myBus.sendMessage(144, elementData.get(i).note[0], elementData.get(i).memoryPosition-1); // valuetable[]
          delay(delay_send);
          
 //2    - max /min (oppure hotkeys)
 
 /*
          if (  elementData.get(i).modifiers == 0) { // per modifiers si intende hotkeys
          if (elementData.get(i).toggleMode == 21 || elementData.get(i).toggleMode == 22 || elementData.get(i).toggleMode == 19) 
          myBus.sendMessage(176+elementData.get(i).midiChannel-1, elementData.get(i).setExcursionControllMax, elementData.get(i).setExcursionControllMin+32); 
          // speed range -32 +32 viene inviata come 0-64
          else 
          myBus.sendMessage(176+elementData.get(i).midiChannel-1, elementData.get(i).setExcursionControllMax, elementData.get(i).setExcursionControllMin);
          // maxvalue[] minvalue[]
          } else {
          if ( elementData.get(i).toggleMode == 11) // se è un pot
          myBus.sendMessage(176+elementData.get(i).midiChannel-1, elementData.get(i).setExcursionControllMax, int(keyboard_out(elementData.get(i).modifiers)));
          else
          myBus.sendMessage(176+elementData.get(i).midiChannel-1, elementData.get(i).setExcursionControllMax, elementData.get(i).modifiers);
          
          }
          
          */
          
          
        if (  elementData.get(i).modifiers == 0) 
        {
          if (elementData.get(i).toggleMode == 21 || elementData.get(i).toggleMode == 22 || elementData.get(i).toggleMode == 19)  // encoders e spinners
          {
         
            
           if (elementData.get(i).addressDMX > 1 && elementData.get(i).indexMidiType == 0 )
        {    myBus.sendMessage(176+elementData.get(i).midiChannel-1, 
          elementData.get(i).setExcursionControllMax, 
         //  int(map(abs(elementData.get(i).setExcursionControllMin),0,32,32,0))+32); // velocità scorrimento per quando suoniamo una scala
                                      int(
                                      (elementData.get(i).setExcursionControllMin)
                                      )+32
                              );
       //   println(int(map(abs(elementData.get(i).setExcursionControllMin),0,32,32,0))+32);
       //  print(binary(byte(elementData.get(i).setExcursionControllMax))); println(" - max");
        }
        else 
        
        
        {
         myBus.sendMessage(176+elementData.get(i).midiChannel-1, 
          elementData.get(i).setExcursionControllMax, 
          elementData.get(i).setExcursionControllMin+32);
         // println(elementData.get(i).setExcursionControllMin+32);
        }
        
      } // speed range -32 +32 viene inviata come 0-64
          else 
          myBus.sendMessage(176+elementData.get(i).midiChannel-1, 
          elementData.get(i).setExcursionControllMax, elementData.get(i).setExcursionControllMin);  // mix - max
        } 
        
        else {
          if ( elementData.get(i).toggleMode == 11)                            
            myBus.sendMessage(176+elementData.get(i).midiChannel-1, 
            elementData.get(i).setExcursionControllMax, int(keyboard_out(elementData.get(i).modifiers))    
           
            // l'informazione relativa ai modificatori (ctrl - shift - alt) 
            //viene allocata nella posizione normalmente usata per MINVALUE
          );
          else
          myBus.sendMessage(176+elementData.get(i).midiChannel-1, 
          elementData.get(i).setExcursionControllMax, elementData.get(i).modifiers
          );
        }
          
          
          delay(delay_send);
//3     - mode / dmx
          if (elementData.get(i).hide == 0)     //    se è visibile
          myBus.sendMessage(176+elementData.get(i).midiChannel-1, int(elementData.get(i).toggleMode), elementData.get(i).addressDMX); // modetable[] dmxtable[]
          else                                  //    metti a zero la modetable
          myBus.sendMessage(176+elementData.get(i).midiChannel-1, 0, elementData.get(i).addressDMX); // se l'item è nascosto/invisibile, mode sarà = 0
// 4    - keyboard / type
          delay(delay_send);
          
        if ( elementData.get(i).toggleMode == 28)
        {
        myBus.sendMessage(176+elementData.get(i).midiChannel-1, int(elementData.get(i).keyBoard), int ( elementData.get(i).indexMidiType )  );
        }
        else
          myBus.sendMessage(176+elementData.get(i).midiChannel-1, int(keyboard_out(elementData.get(i).keyBoard)), int ( elementData.get(i).indexMidiType ));
          // qwertyvalue[] typetable[]
// 5    - led / x
          delay(delay_send);
          myBus.sendMessage(176+elementData.get(i).midiChannel-1,
          int(elementData.get(i).led_), 
          0 // int(elementData.get(i).last_matrix_byte) 
          ); // lightable[] 



          if ( elementData.get(i).toggleMode == 28) {
// 6       
     
          delay(delay_send);
          myBus.sendMessage(176+elementData.get(i).midiChannel-1,
          int(elementData.get(i).User_byte_1), 
          int(elementData.get(i).User_byte_2)  
          ); 
// 7    
          delay(delay_send);
          myBus.sendMessage(176+elementData.get(i).midiChannel-1,
          int(elementData.get(i).User_byte_3), 
          0
          ); 
          }
        
          delay(delay_send); // delay finale
        
      //  break;
      }
    
     
//  SECONDA PAGINA____________________________________________________________________________________________________________
     
     
      {
// 0   
          if ( elementData.get(i).toggleMode_2nd == 28) // se è un item SPRITE - mandiamo un pitch-bend che conterrà una riga verticale (8 bit divisi in due byte)
          myBus.sendMessage(224, elementData.get(i).User_byte_9, elementData.get(i).User_byte_14);
       
// 1
          myBus.sendMessage(144, elementData.get(i).note[1], elementData.get(i).memoryPosition+63);
          delay(delay_send);
// 2
        /*
          if (  elementData.get(i).modifiers_2nd == 0) {
          if (elementData.get(i).toggleMode_2nd == 21 || elementData.get(i).toggleMode_2nd == 22 || elementData.get(i).toggleMode_2nd == 19) 
          myBus.sendMessage(176+elementData.get(i).midiChannel_2nd-1, elementData.get(i).setExcursionControllMax_2nd, elementData.get(i).setExcursionControllMin_2nd+32);
          else 
          myBus.sendMessage(176+elementData.get(i).midiChannel_2nd-1, elementData.get(i).setExcursionControllMax_2nd, elementData.get(i).setExcursionControllMin_2nd);
        
          } else {
          if ( elementData.get(i).toggleMode_2nd == 11) 
          myBus.sendMessage(176+elementData.get(i).midiChannel_2nd-1, elementData.get(i).setExcursionControllMax_2nd, int(keyboard_out(elementData.get(i).modifiers_2nd)));
          else
          myBus.sendMessage(176+elementData.get(i).midiChannel_2nd-1, elementData.get(i).setExcursionControllMax_2nd, elementData.get(i).modifiers_2nd);
          }
          
          */
          
             if (  elementData.get(i).modifiers_2nd == 0) 
        {
          if (elementData.get(i).toggleMode_2nd == 21 || elementData.get(i).toggleMode_2nd == 22 || elementData.get(i).toggleMode_2nd == 19)  // encoders e spinners
          {
         
            
           if (elementData.get(i).addressDMX_2nd > 1 && elementData.get(i).indexMidiType_2nd == 0)
        {    myBus.sendMessage(176+elementData.get(i).midiChannel_2nd-1, 
          elementData.get(i).setExcursionControllMax_2nd, 
         // int(map(abs(elementData.get(i).setExcursionControllMin_2nd),0,32,32,0))+32); // velocità scorrimento per quando suoniamo una scala
          int(
                                      (elementData.get(i).setExcursionControllMin_2nd)
                                      )+32
                               );
       //   println(int(map(abs(elementData.get(i).setExcursionControllMin),0,32,32,0))+32);
        // print(binary(byte(elementData.get(i).setExcursionControllMax_2nd))); // println(" - max");
        }
        else 
        
        
        {
         myBus.sendMessage(176+elementData.get(i).midiChannel_2nd-1, 
          elementData.get(i).setExcursionControllMax_2nd, 
          elementData.get(i).setExcursionControllMin_2nd+32);
         // println(elementData.get(i).setExcursionControllMin+32);
        }
        
      } // speed range -32 +32 viene inviata come 0-64
          else 
          myBus.sendMessage(176+elementData.get(i).midiChannel_2nd-1, 
          elementData.get(i).setExcursionControllMax_2nd, elementData.get(i).setExcursionControllMin_2nd);  // mix - max
        } 
        
        else {
          if ( elementData.get(i).toggleMode_2nd == 11)                            
            myBus.sendMessage(176+elementData.get(i).midiChannel_2nd-1, 
            elementData.get(i).setExcursionControllMax_2nd, int(keyboard_out(elementData.get(i).modifiers_2nd))    
           
            // l'informazione relativa ai modificatori (ctrl - shift - alt) 
            //viene allocata nella posizione normalmente usata per MINVALUE
          );
          else
          myBus.sendMessage(176+elementData.get(i).midiChannel_2nd-1, 
          elementData.get(i).setExcursionControllMax_2nd, elementData.get(i).modifiers_2nd
          );
        }
          
          
// 3
          delay(delay_send);
          if (elementData.get(i).hide == 0)  myBus.sendMessage(176+elementData.get(i).midiChannel_2nd-1, int(elementData.get(i).toggleMode_2nd), elementData.get(i).addressDMX_2nd);
          else myBus.sendMessage(176+elementData.get(i).midiChannel_2nd-1, 0, elementData.get(i).addressDMX_2nd);
        
// 4
          delay(delay_send);
          if ( elementData.get(i).toggleMode_2nd == 28)
          {
          myBus.sendMessage(176+elementData.get(i).midiChannel-1, int(elementData.get(i).keyBoard_2nd), int ( elementData.get(i).indexMidiType_2nd )  );
          }
          else
          myBus.sendMessage(176+elementData.get(i).midiChannel_2nd-1, int(keyboard_out(elementData.get(i).keyBoard_2nd)), int ( elementData.get(i).indexMidiType_2nd ));
// 5
          delay(delay_send);
          myBus.sendMessage(176+elementData.get(i).midiChannel_2nd-1, int(elementData.get(i).led_2nd), 0 );
 

          if ( elementData.get(i).toggleMode == 28) {
// 6       
     
          delay(delay_send);
          myBus.sendMessage(176+elementData.get(i).midiChannel-1,
          int(elementData.get(i).User_byte_6), 
          int(elementData.get(i).User_byte_7)  
          ); 
// 7    
          delay(delay_send);
          myBus.sendMessage(176+elementData.get(i).midiChannel-1,
          int(elementData.get(i).User_byte_8), 
          0
          ); 
          }

        delay(delay_send);
     //   break;
      }
      
      }



  
    } 
    
    
   
    
  stroke(100);
  fill(0, 200, 0);

 rect(gridCols[17],  gridRow[0]+rowBetw, 
 
 gridCols[1] /3  // +(Betw2/5)
 +(i_sender*(Betw2/9)), 
 
 gridRow[1]-rowBetw*1.2 
 ,4);
        
        }
