
/*
11 1 25
ved opens_scale
30 12 24 - ho fatto in modo che non spuntino messaggi hint indesiderati passando col mouse su numberbox messi in hide - ho usato setVisible(false)
         - ho messo una hint specifica per ogni mode - cioè scegliendo una voce nella tendina e posizionandosi sopra col mouse spunta la hint giusta
         - in modalità encoder spuntava la tendina del touchstop - ora non spunta più
         
*/

// editor si blocca con uno scale salvato - ved se possibile sgangiare la matrice da 12 caselle e agganciarla dopo il load
/*
la matrice da 12 si chiama scale1 si trova sotto tendina snd - riga 270
è collegata a un callback : componi - se clicco sul matrix box , con componi vado ad aggiornare le variabili Value e Max e i loro numberbox

sempre sotto tendina snd 
abbiamo  void set_scale_view() // caricare setup da numberbox al pianofortino
                               // e scegliere se visualizzre o no il pianofortino
*/



// adesso spunta il pianofortino delle scale anche in modalità pot-emulation-RAMP

// 1.79 fatto !! - non spunta immagne circuito memory position o led in modalità pot 

// correggere hint: touch lower limit (non esiste più lower limit - ora c'è led out)

// bug: nel page item non si carica la tendina type - risolto -  ho aggiunto "  midiTypeOpt.setValue(indexMidiType); " questo comando in void loadTableRow


// bug: nell'item general non viene visualizzata la corretta etichetta del selettore "led-efx" , che poi sarebbe keys
// la variabile viene salvata e caricata correttamente dal preset ( e presumibilmente viene anche inviata correttamente tramite sendall)
// ho notato anche che lo stesso bug si presenta sulle tendine dell'item PAGE - vuol dire che è qualcosa che è in relazione con il plugga_page
// che viene usato per linkare un selettore a due variabili. - risolto - Dkeys adesso è una ScrollableList e non una ListBox - bah... a quanto pare risolve..

// bug : impossible modificare "led2" nell item page - risolto, c'era il comando lock() attivato della sezione PAGE della void labeler


// modifico ui_4 riga 25 - la correzione mete a posto il range MIN negativo nel caso dello spinner
// in pratica non veniva caricato un valore negativo da preset. perchè il labeler ha bisogno di un valore certo del MODE di un modificatore, 
// per settare il range  -32+32


// non funzionano le HINT dell'ITEM GENERAL

// 1.78 - ho sistemato un pò la corrispondenza in feedback tra toggle groups tra editor e controller fisico

// bisogna aggiungere un discrimine per gli slot dedicati algi sprite, esempio : gli sprite sono memorizzati da 33 in poi, se specifico un "led" 
// inferiore a 33, vorrò dire che quel dato pulsante accenderà un led singolo , non una matrice intera di led

// studio su trasmissione dati sprite
// una riga va prima ricombinata e poi accorciata di un bit
// serve una procedura void di ricombinamento che mi restituisce un byte ricombinato
// serve una void di accorciamento


// 1.77 aggiunto un secondo oscillatore per una resa migliore del sound in basso e in alto

// 1.76 - la hint box mostrava testi anche per gli item nascosti - adesso corretto - z_thevent riga 12

// correggere : il sound quando va troppo in basso o troppo in alto è sgradevole - posibile usare altri oscillatori spostati di due ottave

// CORREGGERE! il messaggio di alert "page switch already activated" funziona anche sul general ... ma bisogna modificar il messaggio!!

// CORREGGERE! correzione da fare : anche se gli item hanno la shape a pulsante 
// (cioè senza l'indicatore del volume / value) mandano fuori velocity variabili a seconda di come vengono settati


//1.75 il feedback midi fa accendere solo l'item corrispondente , spegnendo tutti gli altri
// adesso l'item si colora di verse solo se corrispondono anche i type (note/cc)

// il feedback midi funziona senza distinguere tra note e cc, non va bene - risolto

// sistemare il feedback midi in modalità radio group

// aggiungere richiesta conferma per uscire dal programma editor - per ora ho disattivato l'esc

// 1.74// ho disabilitato il tasto esc




// 1.73 il send ALL aveva un problema. non mandava il rimo item


// 1.72
// problema load sensitivity value - il problema è il setrange(0,127) - cosa sucede? :
// si parte dal caricamento del file, parte "loadtablerow" che viene ripetuto per tutti gli item
// si arriva all'item 23 e qualdo, dentro loadtablerow , si arriva alla riga dMode.setValue(toggleMode); scatta CallbackListener doppioni  (comando onchange)
// il problema è in labeler - dentro cui alla sezone touch si arriva ad un setrange
// ho risolto (pare) spostando la riga che aggiorna dMode.setValue(toggleMode); prima di tutte le altre, che caricano i valori dal file csv... pare funzioni
// 

// 1384  di ui_ la scriverei meglio

// 1.71 problema 663 sezione "ui" - per ora risolto con una limitazione all'acesso a modifierslist

// le hints vengono triggerate in base al numero di togglemode cioè il contenuto di MODE, ma senza differenziare per pagina. da correggere

// sistemato problema rinominazione / sovrascrivi file

// ved se possibile fix save rinomina

// 1.69 aggiunto possibilità scelta led per page

// aggiunta possibilità selezionare qwerty keys per i pot

// rotazione jog pentagonali alla ricezione di 0 o 127

// il mouse release sui selettori, nella settings box , provoca l'inivio di qualche tipo di segnale fuori... capire perche

// le hint sulla jog wheel sono sbagliate, bisogna selezionare esattamente la memoryposition dell'encoder desiderato.

// il general send non veniva resettato caricando un nuovo preset - now fixed


// 1.65 - c'era un problema di reload della value sule selettore "led efx" in GENERAL mode - ora risolto - l'etichetta viene aggiornata correttamente dopo aver caricato il preset.


// si dovrebbe disabilitare il sound in fullscreen - anche no

// 1.64 - problema hotkeys - demo wemake - ora risolto


//sulle manopole bianche non c'è feedback segnale verde
// problema feedback in out 



// send riga 642 - if (  elementData.get(i).keyBoard == 0) {
  // condizione impropria

// BUG midimonitor - non visualizza bene il flusso di note on e off... le note on si vedono solo nell'ultima riga
// non funziona il midi monitor grafico in input dal controller


// toggle e toggle group non funziona bene nella seconda page 

// l'item 1 rimane grigio se cicco altri item all'avvio

// aggiungere hint per tutti i modificatori e sul general. - fatto 

// aggiungere hint sound - fatto
// problemi nella modalità draw - non spariscono i box della memoryposition. - fatto 

// button mode - pc e pb non devono mandare note off - ok
// il toggle deve lasciare il pulsante in grigio quando è acceso - deve valere per tutti i pulsanti
// ved per invertire il senso del mouse con la rotazione delle manopole


// problema feedback - quando agisco sulla value della knob dall'esterno, mi triggera comunque la void ctrl... ed esce midi - fatto
// feedback : se ricevo una nota da fuori, il note off mi azzera il volume - fatto

// diminuire il tempo di permanenza del suono quando si preme su un toggle. - fatto

// 
// feedback visivo movimento knobs aggiungere - fatto
// comunicazione via sysex
// sostituire la parola modifiers, con hotkeys - fatto

// 
