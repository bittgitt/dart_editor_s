 Table table;
 Table editor_settings;
 TableRow Draw_settings;
// *****************************************
// init TABLE to set data // 
//******************************************
void initTableOfElementData() {
  table = new Table();
  table.addColumn("Name");
  table.addColumn("MemoryPosition");
  table.addColumn("midiType");
  table.addColumn("midiType_2nd");
  
  table.addColumn("Note");
  table.addColumn("Note_2nd");
  table.addColumn("MaximumValue");
  table.addColumn("MaximumValue_2nd");
  
  table.addColumn("MinimumValue");
  table.addColumn("MinimumValue_2nd");
  table.addColumn("MidiChannel");
  table.addColumn("MidiChannel_2nd");
  
  table.addColumn("Toggle");
  table.addColumn("Toggle_2nd");
  table.addColumn("Keys");
  table.addColumn("Keys_2nd");
  
  
  table.addColumn("Modifiers");
  table.addColumn("Modifiers_2nd");
  table.addColumn("DMXAddress");
  table.addColumn("DMXAddress_2nd");
  
  table.addColumn("Page");
  table.addColumn("Hide");
  table.addColumn("posX");
  table.addColumn("posY");
  
  
  table.addColumn("shape");
  table.addColumn("dimensionX");
  table.addColumn("dimensionY");
  table.addColumn("labeler");
  
  table.addColumn("led");
  table.addColumn("led_2nd");
  table.addColumn("hint_message");  
  table.addColumn("User_byte_3");
  
  table.addColumn("User_byte_4");
  table.addColumn("User_byte_5");
  table.addColumn("User_byte_6");
  
   table.addColumn("User_byte_7");
  table.addColumn("User_byte_8");
  table.addColumn("User_byte_9");
  table.addColumn("User_byte_10");
  
   table.addColumn("User_byte_11");
    table.addColumn("User_byte_12");
    
    table.addColumn("User_byte_14");
  
}
// *****************************************
// SAVE AND LOAD SETTINGS INTO TABLE // 
//******************************************
void saveTableSettings(String s ) {
  String ss;
   initTableOfElementData();
  for (int i =0; i<60; i++) {
    TableRow r = table.getRow(i);
    r.setString("Name", elementData.get(i).label);
    r.setInt("MemoryPosition", elementData.get(i).memoryPosition);
   
 //  r.setInt("MemoryPosition", input_remap[((elementData.get(i).memoryPosition-1) -(((elementData.get(i).memoryPosition-1)/8)*8))]+ (((elementData.get(i).memoryPosition-1)/8)*8)  ); // remapper new memoryposition semplificata
   
    r.setInt("midiType", elementData.get(i).indexMidiType); // NOTE ,CC, PC, AT....
    r.setInt("midiType_2nd", elementData.get(i).indexMidiType_2nd);
    r.setInt("Note", elementData.get(i).note[0]);
    r.setInt("Note_2nd", elementData.get(i).note[1]);
    r.setInt("MaximumValue", elementData.get(i).setExcursionControllMax);
    r.setInt("MaximumValue_2nd", elementData.get(i).setExcursionControllMax_2nd);
    r.setInt("MinimumValue", elementData.get(i).setExcursionControllMin);
    r.setInt("MinimumValue_2nd", elementData.get(i).setExcursionControllMin_2nd);
    r.setInt("MidiChannel", elementData.get(i).midiChannel);
    r.setInt("MidiChannel_2nd", elementData.get(i).midiChannel_2nd);
    r.setInt("Toggle", elementData.get(i).toggleMode);     // POT, BUTTON, TOGGLE, PAGE ETC ETC...
    r.setInt("Toggle_2nd",elementData.get(i).toggleMode_2nd);
    r.setInt("Keys", elementData.get(i).keyBoard);
    r.setInt("Keys_2nd", elementData.get(i).keyBoard_2nd);
    r.setInt("Modifiers", elementData.get(i).modifiers);
    r.setInt("Modifiers_2nd", elementData.get(i).modifiers_2nd);
    r.setInt("DMXAddress", elementData.get(i).addressDMX);
    r.setInt("DMXAddress_2nd", elementData.get(i).addressDMX_2nd);
    
        r.setInt("Page", elementData.get(i).User_byte_1);
   
    r.setInt("Hide", elementData.get(i).hide);
    r.setInt("posX", elementData.get(i).posX);
    r.setInt("posY", elementData.get(i).posY);
    r.setInt("shape", elementData.get(i).shape);
    r.setInt("dimensionX", elementData.get(i).radiusW);
    r.setInt("dimensionY", elementData.get(i).radiusH);
       
        r.setInt("labeler", elementData.get(i).User_byte_2);
       
    r.setInt("led", elementData.get(i).led_);
    
    r.setString("hint_message", elementData.get(i).hint_message);
     
        r.setInt("User_byte_3", elementData.get(i).User_byte_3);   
        
        r.setInt("User_byte_4", elementData.get(i).User_byte_4);   
        r.setInt("User_byte_5", elementData.get(i).User_byte_5);   
        r.setInt("User_byte_6", elementData.get(i).User_byte_6);   
        
        r.setInt("User_byte_7", elementData.get(i).User_byte_7);   
        r.setInt("User_byte_8", elementData.get(i).User_byte_8);   
        r.setInt("User_byte_9", elementData.get(i).User_byte_9);  
          r.setInt("User_byte_10", elementData.get(i).User_byte_10);  // mask-immagine
          r.setInt("User_byte_11", elementData.get(i).User_byte_11);  // mask-immagine zoom
          r.setInt("User_byte_12", elementData.get(i).User_byte_12);  // mask-immagine layer
          
           r.setInt("User_byte_14", elementData.get(i).User_byte_14);  // ANCORA PER SPRITES MATRICI DI LED
           
           r.setInt("led_2nd", elementData.get(i).led_2nd);
    }
       
     //  if (i == 63) 
       {  
         TableRow r = table.getRow(64);   
        r.setInt("dimensionX", width);
       r.setInt("dimensionY", height);
                    }
                    if (s.indexOf(".csv") >0){
  ss= s.substring(0, s.indexOf(".csv"));  
  saveTable(table, ss+".csv");}
  else 
  saveTable(table, s+".csv");
}












void loadTableSettings(String s) {
  page_selected = false; // rimettiti sulla prima pagina

  sendFirstPage.setOn();
  sendSecondPage.setOff();
  table = loadTable(s, "header");
  TableRow roow;  roow = table.getRow(64); ex_dimensionX = roow.getInt("dimensionX"); ex_dimensionY = roow.getInt("dimensionY");
 // scale1.hide();
 // show_piano = 0;
 //scale1.hide();
  idElement = 1; // scelgo 59 , che sarebbe di solito il general setup , perchè vedo che così non si blocca quando carico uno scale.
                 //  se metto 1 mi ritrovo selezionto l'item della main wheel - indipendentemente dalla memoryposition l'item ha la sua numerazione nell editor...

  
  for (int i=0; i<60; i++) {
    // elementData.get(i).splugga();
   
  elementData.get(i).loadTableRow(table.getRow(i));
 // if (elementData.get(idElement).toggleMode > 18) idElement++;
  while (elementData.get(idElement).toggleMode > 18) {idElement++;}
   
   elementData.get(i).selected = false;
  
  //elementData.get(i).plug_page();
  //   elementData.get(i).labeler();
   

{
  Knob t = (Knob)cp5.get(Integer.toString(i+1)); 
 if (elementData.get(i).hide == 1) t.hide(); else t.show();
    t.setPosition(elementData.get(i).posX,elementData.get(i).posY); // posizione e grandezza della manopola sullo schermo
   
    t.setLabel(elementData.get(i).label);
    t.setWidth(elementData.get(i).radiusW);
    t.setHeight(elementData.get(i).radiusH);
   
  //  t.hide();
 }
   
    
    hint_message_send (i, i);
    elementData.get(i).label_hint.setText(elementData.get(i).hint_message); 
    mousewheel = elementData.get(i).shape;
    changeshape(i);
   
    delay(10);
  
  
  }
  
  square1 = loadImage("mask (1).png"); 
  square2 = loadImage("mask (2).png"); 
  square3 = loadImage("mask (3).png"); 
  square4 = loadImage("mask (4).png"); 
  square5 = loadImage("mask (5).png"); 
  
  circle1 = loadImage("mask (6).png"); 
  circle2 = loadImage("mask (7).png"); 
  circle3 = loadImage("mask (8).png"); 
  circle4 = loadImage("mask (9).png"); 
  circle5 = loadImage("mask (10).png"); 
  
  mask1 = loadImage("mask (11).png"); 
  mask2 = loadImage("mask (12).png"); 
  mask3 = loadImage("mask (13).png"); 
  mask4 = loadImage("mask (14).png"); 
  mask5 = loadImage("mask (15).png"); 
  
 general_send = 0;

      sendSecondPage.setOff();   
      sendFirstPage.setOn();
        
     page_selected = false;
       for (int i=0; i<60; i++) {
      
   elementData.get(i).plug_page();
     elementData.get(i).labeler();
    //  elementData.get(i).loadTableRow(table.getRow(i));
      }
      elementData.get(idElement).selected = true;
       initTableOfElementData();
       
     //  infoGraph = idElement;
     //  change = 2;
     
   //  println(elementData.get(idElement).dMode.getValue()+"cc");
  // elementData.get(idElement).dMode.setVisible(true);
  //  elementData.get(idElement).dMode.show();
     
     //  elementData.get(idElement).labeler();
  // println("ciao");
  //  println(elementData.get(59).keyBoard); 
    
  //  println(elementData.get(59).keyBoard_2nd); 
   // dKeys.plugTo( this, "keyBoard" ); dKeys.plugTo( this, "keyBoard_2nd" );
   
   
  // println(elementData.get(58).indexMidiType); 
   
}







void fileToSave(File selection) {
  if (selection == null) {
  //  println("Window was closed or the user hit cancel.");
  } else {
   // println("User selected " + selection.getAbsolutePath());
    saveTableSettings(selection.getAbsolutePath());
  }
}
void fileToLoad(File selection) {
  if (selection == null) {
  //  println("Window was closed or the user hit cancel.");
  } else {
   // println("User selected " + selection.getAbsolutePath());
    loadTableSettings(selection.getAbsolutePath());
 //  println(elementData.get(idElement).dMode.getValue()+"cc");
   
   elementData.get(idElement).setStateButton(true);   // in questo modo dopo aver caricato il prest faccio vedere tutti i selettori dell'item selezionato automanticamente in partenza
  // elementData.get(idElement).selected = true;
  // elementData.get(idElement).setStateButton(false); 
 
  }
}
// *****************************************
// This function returns all the files in a directory as an array of Strings  
//******************************************
String[] listFileNames(String dir) {
  File file = new File(dir);
  if (file.isDirectory()) {
    String names[] = file.list();
    return names;
  } else {
    // If it's not a directory
    return null;
  }
}
